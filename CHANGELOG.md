# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v1.5.12 - 2024-10-30(16:06:15 +0000)

### Other

- - [DynamiCDNS] DynamicDNS.Client fail when the ip_interface is not set on uci
- - [DynamiCDNS] DynamicDNS.Client fail when the ip_interface is not set on uci

## Release v1.5.11 - 2024-10-14(08:05:31 +0000)

### Other

- - [tr181-dynamicdns][DynamicDNS] Remove openwrt 19 backward compatibility.

## Release v1.5.10 - 2024-10-10(10:20:08 +0000)

### Other

- - DynDNS: client not attempting updates when WAN addres changes

## Release v1.5.9 - 2024-09-25(12:56:46 +0000)

### Other

- - TR-181: Device.DynamicDNS data model issues 19.03.2024

## Release v1.5.8 - 2024-09-24(14:31:25 +0000)

### Other

- - CLONE - [PRPL]DynDNS is not working using dslite

## Release v1.5.7 - 2024-09-10(07:15:58 +0000)

### Other

- [AppArmor] Create AppAmor profile for plugins

## Release v1.5.6 - 2024-07-23(07:52:51 +0000)

### Fixes

- Better shutdown script

## Release v1.5.5 - 2024-05-23(13:04:38 +0000)

### Other

- DynamicDNS non TR-181 parameters visible
- Device.DynamicDNS. need to be adapted to support migration

## Release v1.5.4 - 2024-04-18(15:10:57 +0000)

### Fixes

- [tr181-dynamicdns] Make it compatible with openwrt23.05 toolchain

## Release v1.5.3 - 2024-04-10(07:08:03 +0000)

### Changes

- Make amxb timeouts configurable

## Release v1.5.2 - 2024-03-19(12:59:14 +0000)

### Other

- Rework TR-181 interface stacks indexing number

## Release v1.5.1 - 2024-02-12(11:41:52 +0000)

### Fixes

-  [DynDNS] Misconfiguration in DynDNS Client causes Error Status

## Release v1.5.0 - 2024-02-05(16:21:17 +0000)

### New

- Add tr181-device proxy odl files to components

## Release v1.4.0 - 2024-01-17(09:33:23 +0000)

### New

- Add tr181-device proxy odl files to components

## Release v1.3.1 - 2023-11-30(12:36:36 +0000)

### Fixes

-  CLONE - Error setting DynamicDNS Server

## Release v1.3.0 - 2023-11-30(09:01:21 +0000)

### New

- [TR181-DynamicDNS][prpl] DynamicDNS parameters must be upgrade persistent

## Release v1.2.2 - 2023-10-13(14:13:29 +0000)

### Changes

-  All applications using sahtrace logs should use default log levels

## Release v1.2.1 - 2023-10-03(07:01:55 +0000)

### Other

- Remove runtime dependency on libopenssl-conf

## Release v1.2.0 - 2023-09-14(07:49:41 +0000)

### New

- [amxrt][no-root-user][capability drop] [tr181-dynamicdns] add capabilities for tr181-dynamicdns plugin

## Release v1.1.3 - 2023-08-24(08:05:23 +0000)

### Fixes

- [mxl-osp][tr181-dynamicdns]unable to update the WAN IP address

## Release v1.1.2 - 2023-06-23(10:53:39 +0000)

### Fixes

- [TR181-DynamicDNS] the LastUpdate parameter contains an incorrect value

## Release v1.1.1 - 2023-06-01(14:28:53 +0000)

### Other

- [PRPL] tr181-dynamicdns: /etc/ddns/services missing in OpenWrt 22.03 ddns package v2.8.2

## Release v1.1.0 - 2023-06-01(10:18:48 +0000)

### New

- add ACLs permissions for cwmp user

## Release v1.0.1 - 2023-04-17(12:50:28 +0000)

### Other

- [TR181-DynamicDNS] Implement DynamicDNS.Server{i}.Enable logic

## Release v1.0.0 - 2023-04-07(13:23:18 +0000)

### Breaking

- [TR181-dynamicDNS][TR-181]Device.DynamicDNS incorrect data types detected

## Release v0.1.10 - 2023-03-09(09:18:08 +0000)

### Other

- Add missing runtime dependency on rpcd
- [Config] enable configurable coredump generation

## Release v0.1.9 - 2023-02-16(10:28:57 +0000)

### Other

- Do not clean before building mod-dynamicdns-uci

## Release v0.1.8 - 2023-01-11(09:40:00 +0000)

### Fixes

- [dyndns] CDRouter test failing on M4.1

## Release v0.1.7 - 2023-01-04(09:46:15 +0000)

### Fixes

- TR181-DynamicDNS] Server instance indexes shift when ddns service list is updated breaking dyndns client references

## Release v0.1.6 - 2022-12-20(12:21:25 +0000)

### Fixes

- unable to use hyphen in hostname

## Release v0.1.5 - 2022-12-15(12:11:57 +0000)

### Fixes

- [DynDNS] Crash when removing a Client instance

## Release v0.1.4 - 2022-12-13(07:46:28 +0000)

### Fixes

- [DynDNS] Crash when entering wrong interface reference

## Release v0.1.3 - 2022-12-09(09:20:47 +0000)

### Fixes

- [Config] coredump generation should be configurable

## Release v0.1.2 - 2022-11-22(13:46:46 +0000)

### Other

- Opensource component

## Release v0.1.1 - 2022-11-16(07:58:51 +0000)

### Other

- [dynamicDNS][freedns.afraid.org] Add support for the freedns dynamicDNS client

## Release v0.1.0 - 2022-08-22(08:46:46 +0000)

### New

- [TR181-DynamicDNS] Development of a Device.DynamicDNS. amx plugin

