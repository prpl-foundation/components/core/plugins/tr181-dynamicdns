MACHINE = $(shell $(CC) -dumpmachine)

SRCDIR += $(realpath ../../src)
OBJDIR = $(realpath ../../output/$(MACHINE)/coverage)
INCDIR += $(realpath ../../include_priv)
# MOCK_SRCDIR
COMMON_SRCDIR += $(realpath ../common/)
# MOCK_INCDIR = $(realpath ../common/)
INCDIR += $(realpath ../common_includes/)

HEADERS = $(wildcard $(addsuffix /*.h,$(INCDIR)))
SOURCES = $(wildcard $(SRCDIR)/*.c) \
		  $(wildcard $(SRCDIR)/server/*.c) \
		  $(wildcard $(SRCDIR)/client/*.c)
SOURCES += $(wildcard $(COMMON_SRCDIR)/*.c)
SOURCES += $(wildcard $(MOCK_SRCDIR)/*.c)

CFLAGS += -Werror -Wall -Wextra -Wno-attributes\
          -std=gnu99 -g3 -Wmissing-declarations \
		  $(addprefix -I ,$(INCDIR)) -I$(OBJDIR)/.. \
		  -fkeep-inline-functions -fkeep-static-functions \
		  -Wno-format-nonliteral \
		  $(shell pkg-config --cflags cmocka) -pthread \
		  -D_GNU_SOURCE -DDYNAMIC_DNS_TEST

LDFLAGS += -fkeep-inline-functions -fkeep-static-functions \
		   $(shell pkg-config --libs cmocka) \
		   -lamxc -lamxp -lamxd -lamxo -lamxut -lamxm -lnetmodel \
		   -lsahtrace -ldl -lpthread -lcrypt -levent -lm
