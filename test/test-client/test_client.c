/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <string.h>

#include "test_common.h"
#include "test_client.h"
#include "ddns.h"
#include "ddns_priv.h"
#include "ddns_error_codes.h"
#include "amxp_mock.h"
#include "uci_mock.h"
#include "netmodel_mock.h"
#include "client/dm_client.h"
#include "client/client.h"
#include "server/dm_server.h"

#define TEST_SERVICE_NAME_DEFAULT "uci_afraid.org-basicauth"
#define TEST_SERVICE_DEFAULT "afraid.org-basicauth"

static amxc_var_t args;
static amxc_var_t ret;
static amxc_string_t server;
static amxc_string_t server_on_odl;
static int index_server = 0;

static void print_status(const char* message) {
    printf("- %s.\n", message);
    fflush(stdout);
}

static void clean_variables() {
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

int test_client_setup(void** state) {
    amxd_object_t* server_tmpl = NULL;
    amxd_object_t* server_obj = NULL;

    mock_init(state);

    server_tmpl = amxd_dm_findf(amxut_bus_dm(), DDNS ".Server.");
    assert_non_null(server_tmpl);
    server_obj = amxd_object_findf(server_tmpl, "[" SERVER_NAME " == '%s']", TEST_SERVICE_NAME_DEFAULT);
    assert_non_null(server_obj);
    index_server = amxd_object_get_index(server_obj);
    assert_int_not_equal(index_server, 0);
    amxc_string_init(&server, 0);
    amxc_string_init(&server_on_odl, 0);
    amxc_string_setf(&server, "Device.DynamicDNS.Server.%d.", index_server);
    amxc_string_setf(&server_on_odl, "Device.DynamicDNS.Server.%d", index_server);

    return 0;
}

int test_client_teardown(void** state) {
    mock_cleanup(state);
    clean_variables();
    amxc_string_clean(&server);
    amxc_string_clean(&server_on_odl);
    return 0;
}

void test_dm_events_with_uci(UNUSED void** state) {
    amxd_trans_t trans;
    amxc_var_t* value = NULL;
    amxc_var_t* section = NULL;
    const amxc_var_t* tmp_var = NULL;
    amxd_object_t* tmpl_client = NULL;
    amxd_object_t* obj_client = NULL;
    amxd_object_t* tmpl_hostname = NULL;
    amxd_object_t* obj_hostname = NULL;
    ddns_client_t* client = NULL;
    cstring_t str_var = NULL;
    cstring_t last_str_var = NULL;
    cstring_t last_str_2_var = NULL;
    cstring_t last_str_3_var = NULL;

    //Add Client instance
    print_status("Add new instance DynamicDNS.Client.1");
    assert_int_equal(amxd_trans_init(&trans), 0);
    amxd_trans_select_pathf(&trans, "DynamicDNS.Client.");
    amxd_trans_add_inst(&trans, 0, "test_1");
    amxd_trans_set_value(cstring_t, &trans, "Server", amxc_string_get(&server, 0));
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.2.");
    amxd_trans_set_value(cstring_t, &trans, "Username", "username");
    amxd_trans_set_value(cstring_t, &trans, "Password", "password");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxut_bus_handle_events();
    amxd_trans_clean(&trans);
    tmpl_client = amxd_dm_findf(ddns_get_dm(), "DynamicDNS.Client.");

    //Check DM
    assert_int_equal(uci_sections(), 0);
    assert_int_equal(processes_running(), 0);
    obj_client = amxd_object_findf(tmpl_client, ".1.");
    assert_non_null(obj_client);
    client = (ddns_client_t*) obj_client->priv;
    assert_non_null(client);
    tmp_var = amxd_object_get_param_value(obj_client, "Server");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), amxc_string_get(&server_on_odl, 0));
    tmp_var = amxd_object_get_param_value(obj_client, "Interface");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "Device.IP.Interface.2.");
    tmp_var = amxd_object_get_param_value(obj_client, "Username");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "username");
    tmp_var = amxd_object_get_param_value(obj_client, "Password");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "password");
    tmp_var = amxd_object_get_param_value(obj_client, "Enable");
    assert_non_null(tmp_var);
    assert_true(GET_BOOL(tmp_var, NULL));
    tmp_var = amxd_object_get_param_value(obj_client, "Status");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "Error_Misconfigured");
    tmp_var = amxd_object_get_param_value(obj_client, "LastError");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "MISCONFIGURATION_ERROR");
    assert_string_equal(amxc_string_get(&(client->linux_intf), 0), "eth0");

    //Add Client.1.Hostname instance
    print_status("Add new instance DynamicDNS.Client.1.Hostname.1");
    assert_int_equal(amxd_trans_init(&trans), 0);
    amxd_trans_select_pathf(&trans, "DynamicDNS.Client.1.Hostname.");
    amxd_trans_add_inst(&trans, 0, NULL);
    amxd_trans_set_value(cstring_t, &trans, "Name", "domain.be");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxut_bus_handle_events();
    amxd_trans_clean(&trans);

    //Check UCI Section
    assert_int_equal(uci_sections(), 1);
    assert_int_equal(processes_running(), 1);
    section = get_section("section_1_1");
    assert_string_equal(GETP_CHAR(section, "values.service_name"), TEST_SERVICE_DEFAULT);
    assert_string_equal(GETP_CHAR(section, "values.use_https"), "1");
    assert_string_equal(GETP_CHAR(section, "values.check_interval"), "10");
    assert_string_equal(GETP_CHAR(section, "values.check_unit"), "minutes");
    assert_string_equal(GETP_CHAR(section, "values.force_interval"), "20000");
    assert_string_equal(GETP_CHAR(section, "values.force_unit"), "days");
    assert_string_equal(GETP_CHAR(section, "values.retry_interval"), "60");
    assert_string_equal(GETP_CHAR(section, "values.retry_unit"), "seconds");
    assert_string_equal(GETP_CHAR(section, "values.retry_max_count"), "0");
    assert_string_equal(GETP_CHAR(section, "values.ip_source"), "interface");
    assert_string_equal(GETP_CHAR(section, "values.ip_interface"), "eth0");
    assert_string_equal(GETP_CHAR(section, "values.ip_script"), "/usr/lib/amx/scripts/ddns_scripts_echo_ip.sh");
    assert_string_equal(GETP_CHAR(section, "values.password"), "password");
    assert_string_equal(GETP_CHAR(section, "values.username"), "username");
    assert_string_equal(GETP_CHAR(section, "values.domain"), "domain.be");
    assert_string_equal(GETP_CHAR(section, "values.lookup_host"), "domain.be");
    assert_string_equal(GETP_CHAR(section, "values.enabled"), "1");
    amxc_var_delete(&section);
    //Check DM
    obj_client = amxd_object_findf(tmpl_client, ".1.");
    assert_non_null(obj_client);
    client = (ddns_client_t*) obj_client->priv;
    assert_non_null(client);
    tmp_var = amxd_object_get_param_value(obj_client, "Server");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), amxc_string_get(&server_on_odl, 0));
    tmp_var = amxd_object_get_param_value(obj_client, "Interface");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "Device.IP.Interface.2.");
    tmp_var = amxd_object_get_param_value(obj_client, "Username");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "username");
    tmp_var = amxd_object_get_param_value(obj_client, "Password");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "password");
    tmp_var = amxd_object_get_param_value(obj_client, "Enable");
    assert_non_null(tmp_var);
    assert_true(GET_BOOL(tmp_var, NULL));
    tmp_var = amxd_object_get_param_value(obj_client, "Status");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "Authenticating");
    tmp_var = amxd_object_get_param_value(obj_client, "LastError");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "MISCONFIGURATION_ERROR");
    tmpl_hostname = amxd_object_findf(obj_client, ".Hostname.");
    obj_hostname = amxd_object_findf(tmpl_hostname, ".1.");
    assert_non_null(obj_hostname);
    tmp_var = amxd_object_get_param_value(obj_hostname, "Name");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "domain.be");
    tmp_var = amxd_object_get_param_value(obj_hostname, "Enable");
    assert_non_null(tmp_var);
    assert_true(GET_BOOL(tmp_var, NULL));
    tmp_var = amxd_object_get_param_value(obj_hostname, "Status");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "Updating");
    tmp_var = amxd_object_get_param_value(obj_hostname, "LastUpdate");
    assert_non_null(tmp_var);
    str_var = amxc_var_dyncast(cstring_t, tmp_var);
    assert_string_equal(str_var, "0001-01-01T00:00:00Z");
    last_str_var = strdup(str_var);
    free(str_var);
    assert_string_equal(amxc_string_get(&(client->linux_intf), 0), "eth0");

    print_status("Waiting for check function");
    amxut_timer_go_to_future_ms(3001);
    amxut_bus_handle_events();

    //Check UCI Section
    assert_int_equal(uci_sections(), 1);
    assert_int_equal(processes_running(), 0);
    section = get_section("section_1_1");
    assert_string_equal(GETP_CHAR(section, "values.service_name"), TEST_SERVICE_DEFAULT);
    assert_string_equal(GETP_CHAR(section, "values.use_https"), "1");
    assert_string_equal(GETP_CHAR(section, "values.check_interval"), "10");
    assert_string_equal(GETP_CHAR(section, "values.check_unit"), "minutes");
    assert_string_equal(GETP_CHAR(section, "values.force_interval"), "20000");
    assert_string_equal(GETP_CHAR(section, "values.force_unit"), "days");
    assert_string_equal(GETP_CHAR(section, "values.retry_interval"), "60");
    assert_string_equal(GETP_CHAR(section, "values.retry_unit"), "seconds");
    assert_string_equal(GETP_CHAR(section, "values.retry_max_count"), "0");
    assert_string_equal(GETP_CHAR(section, "values.ip_source"), "interface");
    assert_string_equal(GETP_CHAR(section, "values.ip_interface"), "eth0");
    assert_string_equal(GETP_CHAR(section, "values.ip_script"), "/usr/lib/amx/scripts/ddns_scripts_echo_ip.sh");
    assert_string_equal(GETP_CHAR(section, "values.password"), "password");
    assert_string_equal(GETP_CHAR(section, "values.username"), "username");
    assert_string_equal(GETP_CHAR(section, "values.domain"), "domain.be");
    assert_string_equal(GETP_CHAR(section, "values.lookup_host"), "domain.be");
    assert_string_equal(GETP_CHAR(section, "values.enabled"), "1");
    amxc_var_delete(&section);
    //Check DM
    obj_client = amxd_object_findf(tmpl_client, ".1.");
    assert_non_null(obj_client);
    client = (ddns_client_t*) obj_client->priv;
    assert_non_null(client);
    tmp_var = amxd_object_get_param_value(obj_client, "Server");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), amxc_string_get(&server_on_odl, 0));
    tmp_var = amxd_object_get_param_value(obj_client, "Interface");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "Device.IP.Interface.2.");
    tmp_var = amxd_object_get_param_value(obj_client, "Username");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "username");
    tmp_var = amxd_object_get_param_value(obj_client, "Password");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "password");
    tmp_var = amxd_object_get_param_value(obj_client, "Enable");
    assert_non_null(tmp_var);
    assert_true(GET_BOOL(tmp_var, NULL));
    tmp_var = amxd_object_get_param_value(obj_client, "Status");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "Updated");
    tmp_var = amxd_object_get_param_value(obj_client, "LastError");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "NO_ERROR");
    tmpl_hostname = amxd_object_findf(obj_client, ".Hostname.");
    obj_hostname = amxd_object_findf(tmpl_hostname, ".1.");
    assert_non_null(obj_hostname);
    tmp_var = amxd_object_get_param_value(obj_hostname, "Name");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "domain.be");
    tmp_var = amxd_object_get_param_value(obj_hostname, "Enable");
    assert_non_null(tmp_var);
    assert_true(GET_BOOL(tmp_var, NULL));
    tmp_var = amxd_object_get_param_value(obj_hostname, "Status");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "Registered");
    tmp_var = amxd_object_get_param_value(obj_hostname, "LastUpdate");
    assert_non_null(tmp_var);
    str_var = amxc_var_dyncast(cstring_t, tmp_var);
    assert_string_not_equal(str_var, last_str_var);
    free(last_str_var);
    last_str_var = strdup(str_var);
    free(str_var);
    assert_string_equal(amxc_string_get(&(client->linux_intf), 0), "eth0");

    //Add Client.1.Hostname instance
    print_status("Add new instance DynamicDNS.Client.1.Hostname.2");
    assert_int_equal(amxd_trans_init(&trans), 0);
    amxd_trans_select_pathf(&trans, "DynamicDNS.Client.1.Hostname.");
    amxd_trans_add_inst(&trans, 0, NULL);
    amxd_trans_set_value(cstring_t, &trans, "Name", "domain.br");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxut_bus_handle_events();
    amxd_trans_clean(&trans);

    //Check UCI Section
    assert_int_equal(uci_sections(), 2);
    assert_int_equal(processes_running(), 1);
    section = get_section("section_1_2");
    assert_string_equal(GETP_CHAR(section, "values.service_name"), TEST_SERVICE_DEFAULT);
    assert_string_equal(GETP_CHAR(section, "values.use_https"), "1");
    assert_string_equal(GETP_CHAR(section, "values.check_interval"), "10");
    assert_string_equal(GETP_CHAR(section, "values.check_unit"), "minutes");
    assert_string_equal(GETP_CHAR(section, "values.force_interval"), "20000");
    assert_string_equal(GETP_CHAR(section, "values.force_unit"), "days");
    assert_string_equal(GETP_CHAR(section, "values.retry_interval"), "60");
    assert_string_equal(GETP_CHAR(section, "values.retry_unit"), "seconds");
    assert_string_equal(GETP_CHAR(section, "values.retry_max_count"), "0");
    assert_string_equal(GETP_CHAR(section, "values.ip_source"), "interface");
    assert_string_equal(GETP_CHAR(section, "values.ip_interface"), "eth0");
    assert_string_equal(GETP_CHAR(section, "values.ip_script"), "/usr/lib/amx/scripts/ddns_scripts_echo_ip.sh");
    assert_string_equal(GETP_CHAR(section, "values.password"), "password");
    assert_string_equal(GETP_CHAR(section, "values.username"), "username");
    assert_string_equal(GETP_CHAR(section, "values.domain"), "domain.br");
    assert_string_equal(GETP_CHAR(section, "values.lookup_host"), "domain.br");
    assert_string_equal(GETP_CHAR(section, "values.enabled"), "1");
    amxc_var_delete(&section);
    //Check DM
    obj_client = amxd_object_findf(tmpl_client, ".1.");
    assert_non_null(obj_client);
    client = (ddns_client_t*) obj_client->priv;
    assert_non_null(client);
    tmp_var = amxd_object_get_param_value(obj_client, "Server");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), amxc_string_get(&server_on_odl, 0));
    tmp_var = amxd_object_get_param_value(obj_client, "Interface");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "Device.IP.Interface.2.");
    tmp_var = amxd_object_get_param_value(obj_client, "Username");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "username");
    tmp_var = amxd_object_get_param_value(obj_client, "Password");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "password");
    tmp_var = amxd_object_get_param_value(obj_client, "Enable");
    assert_non_null(tmp_var);
    assert_true(GET_BOOL(tmp_var, NULL));
    tmp_var = amxd_object_get_param_value(obj_client, "Status");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "Authenticating");
    tmp_var = amxd_object_get_param_value(obj_client, "LastError");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "NO_ERROR");
    tmpl_hostname = amxd_object_findf(obj_client, ".Hostname.");
    obj_hostname = amxd_object_findf(tmpl_hostname, ".2.");
    assert_non_null(obj_hostname);
    tmp_var = amxd_object_get_param_value(obj_hostname, "Name");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "domain.br");
    tmp_var = amxd_object_get_param_value(obj_hostname, "Enable");
    assert_non_null(tmp_var);
    assert_true(GET_BOOL(tmp_var, NULL));
    tmp_var = amxd_object_get_param_value(obj_hostname, "Status");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "Updating");
    tmp_var = amxd_object_get_param_value(obj_hostname, "LastUpdate");
    assert_non_null(tmp_var);
    str_var = amxc_var_dyncast(cstring_t, tmp_var);
    assert_string_equal(str_var, "0001-01-01T00:00:00Z");
    last_str_2_var = strdup(str_var);
    free(str_var);
    assert_string_equal(amxc_string_get(&(client->linux_intf), 0), "eth0");

    print_status("Waiting for check function");
    amxut_timer_go_to_future_ms(3001);
    amxut_bus_handle_events();

    //Check UCI Section
    assert_int_equal(uci_sections(), 2);
    assert_int_equal(processes_running(), 0);
    section = get_section("section_1_2");
    assert_string_equal(GETP_CHAR(section, "values.service_name"), TEST_SERVICE_DEFAULT);
    assert_string_equal(GETP_CHAR(section, "values.use_https"), "1");
    assert_string_equal(GETP_CHAR(section, "values.check_interval"), "10");
    assert_string_equal(GETP_CHAR(section, "values.check_unit"), "minutes");
    assert_string_equal(GETP_CHAR(section, "values.force_interval"), "20000");
    assert_string_equal(GETP_CHAR(section, "values.force_unit"), "days");
    assert_string_equal(GETP_CHAR(section, "values.retry_interval"), "60");
    assert_string_equal(GETP_CHAR(section, "values.retry_unit"), "seconds");
    assert_string_equal(GETP_CHAR(section, "values.retry_max_count"), "0");
    assert_string_equal(GETP_CHAR(section, "values.ip_source"), "interface");
    assert_string_equal(GETP_CHAR(section, "values.ip_interface"), "eth0");
    assert_string_equal(GETP_CHAR(section, "values.ip_script"), "/usr/lib/amx/scripts/ddns_scripts_echo_ip.sh");
    assert_string_equal(GETP_CHAR(section, "values.password"), "password");
    assert_string_equal(GETP_CHAR(section, "values.username"), "username");
    assert_string_equal(GETP_CHAR(section, "values.domain"), "domain.br");
    assert_string_equal(GETP_CHAR(section, "values.lookup_host"), "domain.br");
    assert_string_equal(GETP_CHAR(section, "values.enabled"), "1");
    amxc_var_delete(&section);
    //Check DM
    obj_client = amxd_object_findf(tmpl_client, ".1.");
    assert_non_null(obj_client);
    client = (ddns_client_t*) obj_client->priv;
    assert_non_null(client);
    tmp_var = amxd_object_get_param_value(obj_client, "Server");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), amxc_string_get(&server_on_odl, 0));
    tmp_var = amxd_object_get_param_value(obj_client, "Interface");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "Device.IP.Interface.2.");
    tmp_var = amxd_object_get_param_value(obj_client, "Username");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "username");
    tmp_var = amxd_object_get_param_value(obj_client, "Password");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "password");
    tmp_var = amxd_object_get_param_value(obj_client, "Enable");
    assert_non_null(tmp_var);
    assert_true(GET_BOOL(tmp_var, NULL));
    tmp_var = amxd_object_get_param_value(obj_client, "Status");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "Updated");
    tmp_var = amxd_object_get_param_value(obj_client, "LastError");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "NO_ERROR");
    tmpl_hostname = amxd_object_findf(obj_client, ".Hostname.");
    obj_hostname = amxd_object_findf(tmpl_hostname, ".2.");
    assert_non_null(obj_hostname);
    tmp_var = amxd_object_get_param_value(obj_hostname, "Name");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "domain.br");
    tmp_var = amxd_object_get_param_value(obj_hostname, "Enable");
    assert_non_null(tmp_var);
    assert_true(GET_BOOL(tmp_var, NULL));
    tmp_var = amxd_object_get_param_value(obj_hostname, "Status");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "Registered");
    tmp_var = amxd_object_get_param_value(obj_hostname, "LastUpdate");
    assert_non_null(tmp_var);
    str_var = amxc_var_dyncast(cstring_t, tmp_var);
    assert_string_not_equal(str_var, last_str_2_var);
    free(last_str_2_var);
    last_str_2_var = strdup(str_var);
    free(str_var);
    assert_string_equal(amxc_string_get(&(client->linux_intf), 0), "eth0");

    //Change domain
    print_status("Change Domain of DynamicDNS.Client.1.Hostname.1");
    assert_int_equal(amxd_trans_init(&trans), 0);
    amxd_trans_select_pathf(&trans, "DynamicDNS.Client.1.Hostname.1.");
    amxc_var_new(&value);
    amxc_var_set(cstring_t, value, "newdomain.be");
    amxd_trans_set_param(&trans, "Name", value);
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxut_bus_handle_events();
    amxd_trans_clean(&trans);
    amxc_var_delete(&value);

    //Check UCI Section
    assert_int_equal(uci_sections(), 2);
    assert_int_equal(processes_running(), 1);
    section = get_section("section_1_1");
    assert_string_equal(GETP_CHAR(section, "values.service_name"), TEST_SERVICE_DEFAULT);
    assert_string_equal(GETP_CHAR(section, "values.use_https"), "1");
    assert_string_equal(GETP_CHAR(section, "values.check_interval"), "10");
    assert_string_equal(GETP_CHAR(section, "values.check_unit"), "minutes");
    assert_string_equal(GETP_CHAR(section, "values.force_interval"), "20000");
    assert_string_equal(GETP_CHAR(section, "values.force_unit"), "days");
    assert_string_equal(GETP_CHAR(section, "values.retry_interval"), "60");
    assert_string_equal(GETP_CHAR(section, "values.retry_unit"), "seconds");
    assert_string_equal(GETP_CHAR(section, "values.retry_max_count"), "0");
    assert_string_equal(GETP_CHAR(section, "values.ip_source"), "interface");
    assert_string_equal(GETP_CHAR(section, "values.ip_interface"), "eth0");
    assert_string_equal(GETP_CHAR(section, "values.ip_script"), "/usr/lib/amx/scripts/ddns_scripts_echo_ip.sh");
    assert_string_equal(GETP_CHAR(section, "values.password"), "password");
    assert_string_equal(GETP_CHAR(section, "values.username"), "username");
    assert_string_equal(GETP_CHAR(section, "values.domain"), "newdomain.be");
    assert_string_equal(GETP_CHAR(section, "values.lookup_host"), "newdomain.be");
    assert_string_equal(GETP_CHAR(section, "values.enabled"), "1");
    amxc_var_delete(&section);
    //Check DM
    obj_client = amxd_object_findf(tmpl_client, ".1.");
    assert_non_null(obj_client);
    client = (ddns_client_t*) obj_client->priv;
    assert_non_null(client);
    tmp_var = amxd_object_get_param_value(obj_client, "Server");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), amxc_string_get(&server_on_odl, 0));
    tmp_var = amxd_object_get_param_value(obj_client, "Interface");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "Device.IP.Interface.2.");
    tmp_var = amxd_object_get_param_value(obj_client, "Username");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "username");
    tmp_var = amxd_object_get_param_value(obj_client, "Password");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "password");
    tmp_var = amxd_object_get_param_value(obj_client, "Enable");
    assert_non_null(tmp_var);
    assert_true(GET_BOOL(tmp_var, NULL));
    tmp_var = amxd_object_get_param_value(obj_client, "Status");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "Authenticating");
    tmp_var = amxd_object_get_param_value(obj_client, "LastError");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "NO_ERROR");
    tmpl_hostname = amxd_object_findf(obj_client, ".Hostname.");
    obj_hostname = amxd_object_findf(tmpl_hostname, ".1.");
    assert_non_null(obj_hostname);
    tmp_var = amxd_object_get_param_value(obj_hostname, "Name");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "newdomain.be");
    tmp_var = amxd_object_get_param_value(obj_hostname, "Enable");
    assert_non_null(tmp_var);
    assert_true(GET_BOOL(tmp_var, NULL));
    tmp_var = amxd_object_get_param_value(obj_hostname, "Status");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "Updating");
    tmp_var = amxd_object_get_param_value(obj_hostname, "LastUpdate");
    assert_non_null(tmp_var);
    str_var = amxc_var_dyncast(cstring_t, tmp_var);
    assert_string_equal(str_var, last_str_var);
    free(last_str_var);
    last_str_var = strdup(str_var);
    free(str_var);
    assert_string_equal(amxc_string_get(&(client->linux_intf), 0), "eth0");

    print_status("Waiting for check function");
    amxut_timer_go_to_future_ms(3001);
    amxut_bus_handle_events();

    //Check UCI Section
    assert_int_equal(uci_sections(), 2);
    assert_int_equal(processes_running(), 0);
    section = get_section("section_1_1");
    assert_string_equal(GETP_CHAR(section, "values.service_name"), TEST_SERVICE_DEFAULT);
    assert_string_equal(GETP_CHAR(section, "values.use_https"), "1");
    assert_string_equal(GETP_CHAR(section, "values.check_interval"), "10");
    assert_string_equal(GETP_CHAR(section, "values.check_unit"), "minutes");
    assert_string_equal(GETP_CHAR(section, "values.force_interval"), "20000");
    assert_string_equal(GETP_CHAR(section, "values.force_unit"), "days");
    assert_string_equal(GETP_CHAR(section, "values.retry_interval"), "60");
    assert_string_equal(GETP_CHAR(section, "values.retry_unit"), "seconds");
    assert_string_equal(GETP_CHAR(section, "values.retry_max_count"), "0");
    assert_string_equal(GETP_CHAR(section, "values.ip_source"), "interface");
    assert_string_equal(GETP_CHAR(section, "values.ip_interface"), "eth0");
    assert_string_equal(GETP_CHAR(section, "values.ip_script"), "/usr/lib/amx/scripts/ddns_scripts_echo_ip.sh");
    assert_string_equal(GETP_CHAR(section, "values.password"), "password");
    assert_string_equal(GETP_CHAR(section, "values.username"), "username");
    assert_string_equal(GETP_CHAR(section, "values.domain"), "newdomain.be");
    assert_string_equal(GETP_CHAR(section, "values.lookup_host"), "newdomain.be");
    assert_string_equal(GETP_CHAR(section, "values.enabled"), "1");
    amxc_var_delete(&section);
    //Check DM
    obj_client = amxd_object_findf(tmpl_client, ".1.");
    assert_non_null(obj_client);
    client = (ddns_client_t*) obj_client->priv;
    assert_non_null(client);
    tmp_var = amxd_object_get_param_value(obj_client, "Server");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), amxc_string_get(&server_on_odl, 0));
    tmp_var = amxd_object_get_param_value(obj_client, "Interface");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "Device.IP.Interface.2.");
    tmp_var = amxd_object_get_param_value(obj_client, "Username");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "username");
    tmp_var = amxd_object_get_param_value(obj_client, "Password");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "password");
    tmp_var = amxd_object_get_param_value(obj_client, "Enable");
    assert_non_null(tmp_var);
    assert_true(GET_BOOL(tmp_var, NULL));
    tmp_var = amxd_object_get_param_value(obj_client, "Status");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "Updated");
    tmp_var = amxd_object_get_param_value(obj_client, "LastError");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "NO_ERROR");
    tmpl_hostname = amxd_object_findf(obj_client, ".Hostname.");
    obj_hostname = amxd_object_findf(tmpl_hostname, ".1.");
    assert_non_null(obj_hostname);
    tmp_var = amxd_object_get_param_value(obj_hostname, "Name");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "newdomain.be");
    tmp_var = amxd_object_get_param_value(obj_hostname, "Enable");
    assert_non_null(tmp_var);
    assert_true(GET_BOOL(tmp_var, NULL));
    tmp_var = amxd_object_get_param_value(obj_hostname, "Status");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "Registered");
    tmp_var = amxd_object_get_param_value(obj_hostname, "LastUpdate");
    assert_non_null(tmp_var);
    str_var = amxc_var_dyncast(cstring_t, tmp_var);
    assert_string_not_equal(str_var, last_str_var);
    free(last_str_var);
    last_str_var = strdup(str_var);
    free(str_var);
    assert_string_equal(amxc_string_get(&(client->linux_intf), 0), "eth0");

    //Change IP
    print_status("Change IP of DynamicDNS.Client.1.");
    assert_int_equal(amxd_trans_init(&trans), 0);
    amxd_trans_select_pathf(&trans, "DynamicDNS.Client.1.");
    amxc_var_new(&value);
    amxc_var_set(cstring_t, value, "127.0.0.1");
    amxd_trans_set_param(&trans, "IP", value);
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxut_bus_handle_events();
    amxd_trans_clean(&trans);
    amxc_var_delete(&value);

    //Check UCI Section
    assert_int_equal(uci_sections(), 2);
    assert_int_equal(processes_running(), 2);
    section = get_section("section_1_1");
    assert_string_equal(GETP_CHAR(section, "values.service_name"), TEST_SERVICE_DEFAULT);
    assert_string_equal(GETP_CHAR(section, "values.use_https"), "1");
    assert_string_equal(GETP_CHAR(section, "values.check_interval"), "10");
    assert_string_equal(GETP_CHAR(section, "values.check_unit"), "minutes");
    assert_string_equal(GETP_CHAR(section, "values.force_interval"), "20000");
    assert_string_equal(GETP_CHAR(section, "values.force_unit"), "days");
    assert_string_equal(GETP_CHAR(section, "values.retry_interval"), "60");
    assert_string_equal(GETP_CHAR(section, "values.retry_unit"), "seconds");
    assert_string_equal(GETP_CHAR(section, "values.retry_max_count"), "0");
    assert_string_equal(GETP_CHAR(section, "values.ip_source"), "script");
    assert_string_equal(GETP_CHAR(section, "values.ip_script"), "/usr/lib/amx/scripts/ddns_scripts_echo_ip.sh");
    assert_string_equal(GETP_CHAR(section, "values.ip_interface"), "eth0");
    assert_string_equal(GETP_CHAR(section, "values.password"), "password");
    assert_string_equal(GETP_CHAR(section, "values.username"), "username");
    assert_string_equal(GETP_CHAR(section, "values.domain"), "newdomain.be");
    assert_string_equal(GETP_CHAR(section, "values.lookup_host"), "newdomain.be");
    assert_string_equal(GETP_CHAR(section, "values.enabled"), "1");
    amxc_var_delete(&section);
    //Check DM
    obj_client = amxd_object_findf(tmpl_client, ".1.");
    assert_non_null(obj_client);
    client = (ddns_client_t*) obj_client->priv;
    assert_non_null(client);
    tmp_var = amxd_object_get_param_value(obj_client, "Server");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), amxc_string_get(&server_on_odl, 0));
    tmp_var = amxd_object_get_param_value(obj_client, "Interface");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "Device.IP.Interface.2.");
    tmp_var = amxd_object_get_param_value(obj_client, "Username");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "username");
    tmp_var = amxd_object_get_param_value(obj_client, "Password");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "password");
    tmp_var = amxd_object_get_param_value(obj_client, "Enable");
    assert_non_null(tmp_var);
    assert_true(GET_BOOL(tmp_var, NULL));
    tmp_var = amxd_object_get_param_value(obj_client, "Status");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "Authenticating");
    tmp_var = amxd_object_get_param_value(obj_client, "LastError");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "NO_ERROR");
    tmpl_hostname = amxd_object_findf(obj_client, ".Hostname.");
    obj_hostname = amxd_object_findf(tmpl_hostname, ".1.");
    assert_non_null(obj_hostname);
    tmp_var = amxd_object_get_param_value(obj_hostname, "Name");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "newdomain.be");
    tmp_var = amxd_object_get_param_value(obj_hostname, "Enable");
    assert_non_null(tmp_var);
    assert_true(GET_BOOL(tmp_var, NULL));
    tmp_var = amxd_object_get_param_value(obj_hostname, "Status");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "Updating");
    tmp_var = amxd_object_get_param_value(obj_hostname, "LastUpdate");
    assert_non_null(tmp_var);
    str_var = amxc_var_dyncast(cstring_t, tmp_var);
    assert_string_equal(str_var, last_str_var);
    free(last_str_var);
    last_str_var = strdup(str_var);
    free(str_var);
    assert_string_equal(amxc_string_get(&(client->linux_intf), 0), "eth0");

    print_status("Waiting for check function");
    amxut_timer_go_to_future_ms(3001);
    amxut_bus_handle_events();

    //Check UCI Section
    assert_int_equal(uci_sections(), 2);
    assert_int_equal(processes_running(), 0);
    section = get_section("section_1_1");
    assert_string_equal(GETP_CHAR(section, "values.service_name"), TEST_SERVICE_DEFAULT);
    assert_string_equal(GETP_CHAR(section, "values.use_https"), "1");
    assert_string_equal(GETP_CHAR(section, "values.check_interval"), "10");
    assert_string_equal(GETP_CHAR(section, "values.check_unit"), "minutes");
    assert_string_equal(GETP_CHAR(section, "values.force_interval"), "20000");
    assert_string_equal(GETP_CHAR(section, "values.force_unit"), "days");
    assert_string_equal(GETP_CHAR(section, "values.retry_interval"), "60");
    assert_string_equal(GETP_CHAR(section, "values.retry_unit"), "seconds");
    assert_string_equal(GETP_CHAR(section, "values.retry_max_count"), "0");
    assert_string_equal(GETP_CHAR(section, "values.ip_source"), "script");
    assert_string_equal(GETP_CHAR(section, "values.ip_script"), "/usr/lib/amx/scripts/ddns_scripts_echo_ip.sh");
    assert_string_equal(GETP_CHAR(section, "values.ip_interface"), "eth0");
    assert_string_equal(GETP_CHAR(section, "values.password"), "password");
    assert_string_equal(GETP_CHAR(section, "values.username"), "username");
    assert_string_equal(GETP_CHAR(section, "values.domain"), "newdomain.be");
    assert_string_equal(GETP_CHAR(section, "values.lookup_host"), "newdomain.be");
    assert_string_equal(GETP_CHAR(section, "values.enabled"), "1");
    amxc_var_delete(&section);
    //Check DM
    obj_client = amxd_object_findf(tmpl_client, ".1.");
    assert_non_null(obj_client);
    client = (ddns_client_t*) obj_client->priv;
    assert_non_null(client);
    tmp_var = amxd_object_get_param_value(obj_client, "Server");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), amxc_string_get(&server_on_odl, 0));
    tmp_var = amxd_object_get_param_value(obj_client, "Interface");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "Device.IP.Interface.2.");
    tmp_var = amxd_object_get_param_value(obj_client, "Username");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "username");
    tmp_var = amxd_object_get_param_value(obj_client, "Password");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "password");
    tmp_var = amxd_object_get_param_value(obj_client, "Enable");
    assert_non_null(tmp_var);
    assert_true(GET_BOOL(tmp_var, NULL));
    tmp_var = amxd_object_get_param_value(obj_client, "Status");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "Updated");
    tmp_var = amxd_object_get_param_value(obj_client, "LastError");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "NO_ERROR");
    tmpl_hostname = amxd_object_findf(obj_client, ".Hostname.");
    obj_hostname = amxd_object_findf(tmpl_hostname, ".1.");
    assert_non_null(obj_hostname);
    tmp_var = amxd_object_get_param_value(obj_hostname, "Name");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "newdomain.be");
    tmp_var = amxd_object_get_param_value(obj_hostname, "Enable");
    assert_non_null(tmp_var);
    assert_true(GET_BOOL(tmp_var, NULL));
    tmp_var = amxd_object_get_param_value(obj_hostname, "Status");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "Registered");
    tmp_var = amxd_object_get_param_value(obj_hostname, "LastUpdate");
    assert_non_null(tmp_var);
    str_var = amxc_var_dyncast(cstring_t, tmp_var);
    assert_string_not_equal(str_var, last_str_var);
    free(last_str_var);
    last_str_var = strdup(str_var);
    free(str_var);
    assert_string_equal(amxc_string_get(&(client->linux_intf), 0), "eth0");

    //Change Interface and remove IP
    print_status("Change Interface and remove IP of DynamicDNS.Client.1.");
    assert_int_equal(amxd_trans_init(&trans), 0);
    amxd_trans_select_pathf(&trans, "DynamicDNS.Client.1.");
    amxd_trans_set_cstring_t(&trans, "IP", "");
    amxd_trans_set_cstring_t(&trans, "Interface", "Device.IP.Interface.3.");
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxut_bus_handle_events();
    amxd_trans_clean(&trans);

    //Check UCI Section
    assert_int_equal(uci_sections(), 2);
    assert_int_equal(processes_running(), 2);
    section = get_section("section_1_1");
    assert_string_equal(GETP_CHAR(section, "values.service_name"), TEST_SERVICE_DEFAULT);
    assert_string_equal(GETP_CHAR(section, "values.use_https"), "1");
    assert_string_equal(GETP_CHAR(section, "values.check_interval"), "10");
    assert_string_equal(GETP_CHAR(section, "values.check_unit"), "minutes");
    assert_string_equal(GETP_CHAR(section, "values.force_interval"), "20000");
    assert_string_equal(GETP_CHAR(section, "values.force_unit"), "days");
    assert_string_equal(GETP_CHAR(section, "values.retry_interval"), "60");
    assert_string_equal(GETP_CHAR(section, "values.retry_unit"), "seconds");
    assert_string_equal(GETP_CHAR(section, "values.retry_max_count"), "0");
    assert_string_equal(GETP_CHAR(section, "values.ip_source"), "interface");
    assert_string_equal(GETP_CHAR(section, "values.ip_interface"), "br-lan");
    assert_string_equal(GETP_CHAR(section, "values.ip_script"), "/usr/lib/amx/scripts/ddns_scripts_echo_ip.sh");
    assert_string_equal(GETP_CHAR(section, "values.password"), "password");
    assert_string_equal(GETP_CHAR(section, "values.username"), "username");
    assert_string_equal(GETP_CHAR(section, "values.domain"), "newdomain.be");
    assert_string_equal(GETP_CHAR(section, "values.lookup_host"), "newdomain.be");
    assert_string_equal(GETP_CHAR(section, "values.enabled"), "1");
    amxc_var_delete(&section);
    //Check DM
    obj_client = amxd_object_findf(tmpl_client, ".1.");
    assert_non_null(obj_client);
    client = (ddns_client_t*) obj_client->priv;
    assert_non_null(client);
    tmp_var = amxd_object_get_param_value(obj_client, "Server");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), amxc_string_get(&server_on_odl, 0));
    tmp_var = amxd_object_get_param_value(obj_client, "Interface");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "Device.IP.Interface.3.");
    tmp_var = amxd_object_get_param_value(obj_client, "Username");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "username");
    tmp_var = amxd_object_get_param_value(obj_client, "Password");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "password");
    tmp_var = amxd_object_get_param_value(obj_client, "Enable");
    assert_non_null(tmp_var);
    assert_true(GET_BOOL(tmp_var, NULL));
    tmp_var = amxd_object_get_param_value(obj_client, "Status");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "Authenticating");
    tmp_var = amxd_object_get_param_value(obj_client, "LastError");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "NO_ERROR");
    tmpl_hostname = amxd_object_findf(obj_client, ".Hostname.");
    obj_hostname = amxd_object_findf(tmpl_hostname, ".1.");
    assert_non_null(obj_hostname);
    tmp_var = amxd_object_get_param_value(obj_hostname, "Name");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "newdomain.be");
    tmp_var = amxd_object_get_param_value(obj_hostname, "Enable");
    assert_non_null(tmp_var);
    assert_true(GET_BOOL(tmp_var, NULL));
    tmp_var = amxd_object_get_param_value(obj_hostname, "Status");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "Updating");
    tmp_var = amxd_object_get_param_value(obj_hostname, "LastUpdate");
    assert_non_null(tmp_var);
    str_var = amxc_var_dyncast(cstring_t, tmp_var);
    assert_string_equal(str_var, last_str_var);
    free(last_str_var);
    last_str_var = strdup(str_var);
    free(str_var);
    assert_string_equal(amxc_string_get(&(client->linux_intf), 0), "br-lan");

    print_status("Waiting for check function");
    amxut_timer_go_to_future_ms(3001);
    amxut_bus_handle_events();

    //Check UCI Section
    assert_int_equal(uci_sections(), 2);
    assert_int_equal(processes_running(), 0);
    section = get_section("section_1_1");
    assert_string_equal(GETP_CHAR(section, "values.service_name"), TEST_SERVICE_DEFAULT);
    assert_string_equal(GETP_CHAR(section, "values.use_https"), "1");
    assert_string_equal(GETP_CHAR(section, "values.check_interval"), "10");
    assert_string_equal(GETP_CHAR(section, "values.check_unit"), "minutes");
    assert_string_equal(GETP_CHAR(section, "values.force_interval"), "20000");
    assert_string_equal(GETP_CHAR(section, "values.force_unit"), "days");
    assert_string_equal(GETP_CHAR(section, "values.retry_interval"), "60");
    assert_string_equal(GETP_CHAR(section, "values.retry_unit"), "seconds");
    assert_string_equal(GETP_CHAR(section, "values.retry_max_count"), "0");
    assert_string_equal(GETP_CHAR(section, "values.ip_source"), "interface");
    assert_string_equal(GETP_CHAR(section, "values.ip_interface"), "br-lan");
    assert_string_equal(GETP_CHAR(section, "values.ip_script"), "/usr/lib/amx/scripts/ddns_scripts_echo_ip.sh");
    assert_string_equal(GETP_CHAR(section, "values.password"), "password");
    assert_string_equal(GETP_CHAR(section, "values.username"), "username");
    assert_string_equal(GETP_CHAR(section, "values.domain"), "newdomain.be");
    assert_string_equal(GETP_CHAR(section, "values.lookup_host"), "newdomain.be");
    assert_string_equal(GETP_CHAR(section, "values.enabled"), "1");
    amxc_var_delete(&section);
    //Check DM
    obj_client = amxd_object_findf(tmpl_client, ".1.");
    assert_non_null(obj_client);
    client = (ddns_client_t*) obj_client->priv;
    assert_non_null(client);
    tmp_var = amxd_object_get_param_value(obj_client, "Server");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), amxc_string_get(&server_on_odl, 0));
    tmp_var = amxd_object_get_param_value(obj_client, "Interface");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "Device.IP.Interface.3.");
    tmp_var = amxd_object_get_param_value(obj_client, "Username");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "username");
    tmp_var = amxd_object_get_param_value(obj_client, "Password");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "password");
    tmp_var = amxd_object_get_param_value(obj_client, "Enable");
    assert_non_null(tmp_var);
    assert_true(GET_BOOL(tmp_var, NULL));
    tmp_var = amxd_object_get_param_value(obj_client, "Status");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "Updated");
    tmp_var = amxd_object_get_param_value(obj_client, "LastError");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "NO_ERROR");
    tmpl_hostname = amxd_object_findf(obj_client, ".Hostname.");
    obj_hostname = amxd_object_findf(tmpl_hostname, ".1.");
    assert_non_null(obj_hostname);
    tmp_var = amxd_object_get_param_value(obj_hostname, "Name");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "newdomain.be");
    tmp_var = amxd_object_get_param_value(obj_hostname, "Enable");
    assert_non_null(tmp_var);
    assert_true(GET_BOOL(tmp_var, NULL));
    tmp_var = amxd_object_get_param_value(obj_hostname, "Status");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "Registered");
    tmp_var = amxd_object_get_param_value(obj_hostname, "LastUpdate");
    assert_non_null(tmp_var);
    str_var = amxc_var_dyncast(cstring_t, tmp_var);
    assert_string_not_equal(str_var, last_str_var);
    free(last_str_var);
    last_str_var = strdup(str_var);
    free(str_var);
    assert_string_equal(amxc_string_get(&(client->linux_intf), 0), "br-lan");

    //Change IPVersion to Ipv6
    print_status("Change IPVersion to IPv6");
    assert_int_equal(amxd_trans_init(&trans), 0);
    amxd_trans_select_pathf(&trans, "DynamicDNS.Client.1.");
    amxd_trans_set_uint32_t(&trans, "IPVersion", 6);
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxut_bus_handle_events();
    amxd_trans_clean(&trans);

    //Check UCI Section
    assert_int_equal(uci_sections(), 2);
    assert_int_equal(processes_running(), 2);
    section = get_section("section_1_1");
    assert_string_equal(GETP_CHAR(section, "values.service_name"), TEST_SERVICE_DEFAULT);
    assert_string_equal(GETP_CHAR(section, "values.use_https"), "1");
    assert_string_equal(GETP_CHAR(section, "values.check_interval"), "10");
    assert_string_equal(GETP_CHAR(section, "values.check_unit"), "minutes");
    assert_string_equal(GETP_CHAR(section, "values.force_interval"), "20000");
    assert_string_equal(GETP_CHAR(section, "values.force_unit"), "days");
    assert_string_equal(GETP_CHAR(section, "values.retry_interval"), "60");
    assert_string_equal(GETP_CHAR(section, "values.retry_unit"), "seconds");
    assert_string_equal(GETP_CHAR(section, "values.retry_max_count"), "0");
    assert_string_equal(GETP_CHAR(section, "values.ip_source"), "interface");
    assert_string_equal(GETP_CHAR(section, "values.ip_interface"), "br-lan");
    assert_string_equal(GETP_CHAR(section, "values.ip_script"), "/usr/lib/amx/scripts/ddns_scripts_echo_ip.sh");
    assert_string_equal(GETP_CHAR(section, "values.force_ipversion"), "1");
    assert_string_equal(GETP_CHAR(section, "values.use_ipv6"), "1");
    assert_string_equal(GETP_CHAR(section, "values.password"), "password");
    assert_string_equal(GETP_CHAR(section, "values.username"), "username");
    assert_string_equal(GETP_CHAR(section, "values.domain"), "newdomain.be");
    assert_string_equal(GETP_CHAR(section, "values.lookup_host"), "newdomain.be");
    assert_string_equal(GETP_CHAR(section, "values.enabled"), "1");
    amxc_var_delete(&section);
    //Check DM
    obj_client = amxd_object_findf(tmpl_client, ".1.");
    assert_non_null(obj_client);
    client = (ddns_client_t*) obj_client->priv;
    assert_non_null(client);
    tmp_var = amxd_object_get_param_value(obj_client, "Server");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), amxc_string_get(&server_on_odl, 0));
    tmp_var = amxd_object_get_param_value(obj_client, "Interface");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "Device.IP.Interface.3.");
    tmp_var = amxd_object_get_param_value(obj_client, "Username");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "username");
    tmp_var = amxd_object_get_param_value(obj_client, "Password");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "password");
    tmp_var = amxd_object_get_param_value(obj_client, "Enable");
    assert_non_null(tmp_var);
    assert_true(GET_BOOL(tmp_var, NULL));
    tmp_var = amxd_object_get_param_value(obj_client, "Status");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "Authenticating");
    tmp_var = amxd_object_get_param_value(obj_client, "LastError");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "NO_ERROR");
    tmpl_hostname = amxd_object_findf(obj_client, ".Hostname.");
    obj_hostname = amxd_object_findf(tmpl_hostname, ".1.");
    assert_non_null(obj_hostname);
    tmp_var = amxd_object_get_param_value(obj_hostname, "Name");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "newdomain.be");
    tmp_var = amxd_object_get_param_value(obj_hostname, "Enable");
    assert_non_null(tmp_var);
    assert_true(GET_BOOL(tmp_var, NULL));
    tmp_var = amxd_object_get_param_value(obj_hostname, "Status");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "Updating");
    tmp_var = amxd_object_get_param_value(obj_hostname, "LastUpdate");
    assert_non_null(tmp_var);
    str_var = amxc_var_dyncast(cstring_t, tmp_var);
    assert_string_equal(str_var, last_str_var);
    free(last_str_var);
    last_str_var = strdup(str_var);
    free(str_var);
    assert_string_equal(amxc_string_get(&(client->linux_intf), 0), "br-lan");

    print_status("Waiting for check function");
    amxut_timer_go_to_future_ms(3001);
    amxut_bus_handle_events();

    //Check UCI Section
    assert_int_equal(uci_sections(), 2);
    assert_int_equal(processes_running(), 0);
    section = get_section("section_1_1");
    assert_string_equal(GETP_CHAR(section, "values.service_name"), TEST_SERVICE_DEFAULT);
    assert_string_equal(GETP_CHAR(section, "values.use_https"), "1");
    assert_string_equal(GETP_CHAR(section, "values.check_interval"), "10");
    assert_string_equal(GETP_CHAR(section, "values.check_unit"), "minutes");
    assert_string_equal(GETP_CHAR(section, "values.force_interval"), "20000");
    assert_string_equal(GETP_CHAR(section, "values.force_unit"), "days");
    assert_string_equal(GETP_CHAR(section, "values.retry_interval"), "60");
    assert_string_equal(GETP_CHAR(section, "values.retry_unit"), "seconds");
    assert_string_equal(GETP_CHAR(section, "values.retry_max_count"), "0");
    assert_string_equal(GETP_CHAR(section, "values.ip_source"), "interface");
    assert_string_equal(GETP_CHAR(section, "values.ip_interface"), "br-lan");
    assert_string_equal(GETP_CHAR(section, "values.ip_script"), "/usr/lib/amx/scripts/ddns_scripts_echo_ip.sh");
    assert_string_equal(GETP_CHAR(section, "values.force_ipversion"), "1");
    assert_string_equal(GETP_CHAR(section, "values.use_ipv6"), "1");
    assert_string_equal(GETP_CHAR(section, "values.password"), "password");
    assert_string_equal(GETP_CHAR(section, "values.username"), "username");
    assert_string_equal(GETP_CHAR(section, "values.domain"), "newdomain.be");
    assert_string_equal(GETP_CHAR(section, "values.lookup_host"), "newdomain.be");
    assert_string_equal(GETP_CHAR(section, "values.enabled"), "1");
    amxc_var_delete(&section);
    //Check DM
    obj_client = amxd_object_findf(tmpl_client, ".1.");
    assert_non_null(obj_client);
    client = (ddns_client_t*) obj_client->priv;
    assert_non_null(client);
    tmp_var = amxd_object_get_param_value(obj_client, "Server");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), amxc_string_get(&server_on_odl, 0));
    tmp_var = amxd_object_get_param_value(obj_client, "Interface");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "Device.IP.Interface.3.");
    tmp_var = amxd_object_get_param_value(obj_client, "Username");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "username");
    tmp_var = amxd_object_get_param_value(obj_client, "Password");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "password");
    tmp_var = amxd_object_get_param_value(obj_client, "Enable");
    assert_non_null(tmp_var);
    assert_true(GET_BOOL(tmp_var, NULL));
    tmp_var = amxd_object_get_param_value(obj_client, "Status");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "Updated");
    tmp_var = amxd_object_get_param_value(obj_client, "LastError");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "NO_ERROR");
    tmpl_hostname = amxd_object_findf(obj_client, ".Hostname.");
    obj_hostname = amxd_object_findf(tmpl_hostname, ".1.");
    assert_non_null(obj_hostname);
    tmp_var = amxd_object_get_param_value(obj_hostname, "Name");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "newdomain.be");
    tmp_var = amxd_object_get_param_value(obj_hostname, "Enable");
    assert_non_null(tmp_var);
    assert_true(GET_BOOL(tmp_var, NULL));
    tmp_var = amxd_object_get_param_value(obj_hostname, "Status");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "Registered");
    tmp_var = amxd_object_get_param_value(obj_hostname, "LastUpdate");
    assert_non_null(tmp_var);
    str_var = amxc_var_dyncast(cstring_t, tmp_var);
    assert_string_not_equal(str_var, last_str_var);
    free(last_str_var);
    last_str_var = strdup(str_var);
    free(str_var);
    assert_string_equal(amxc_string_get(&(client->linux_intf), 0), "br-lan");

    //Change IPVersion to IPv4
    print_status("Change IPVersion to IPv4");
    assert_int_equal(amxd_trans_init(&trans), 0);
    amxd_trans_select_pathf(&trans, "DynamicDNS.Client.1.");
    amxd_trans_set_uint32_t(&trans, "IPVersion", 4);
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxut_bus_handle_events();
    amxd_trans_clean(&trans);

    //Check UCI Section
    assert_int_equal(uci_sections(), 2);
    assert_int_equal(processes_running(), 2);
    section = get_section("section_1_1");
    assert_string_equal(GETP_CHAR(section, "values.service_name"), TEST_SERVICE_DEFAULT);
    assert_string_equal(GETP_CHAR(section, "values.use_https"), "1");
    assert_string_equal(GETP_CHAR(section, "values.check_interval"), "10");
    assert_string_equal(GETP_CHAR(section, "values.check_unit"), "minutes");
    assert_string_equal(GETP_CHAR(section, "values.force_interval"), "20000");
    assert_string_equal(GETP_CHAR(section, "values.force_unit"), "days");
    assert_string_equal(GETP_CHAR(section, "values.retry_interval"), "60");
    assert_string_equal(GETP_CHAR(section, "values.retry_unit"), "seconds");
    assert_string_equal(GETP_CHAR(section, "values.retry_max_count"), "0");
    assert_string_equal(GETP_CHAR(section, "values.ip_source"), "interface");
    assert_string_equal(GETP_CHAR(section, "values.ip_interface"), "br-lan");
    assert_string_equal(GETP_CHAR(section, "values.ip_script"), "/usr/lib/amx/scripts/ddns_scripts_echo_ip.sh");
    assert_string_equal(GETP_CHAR(section, "values.force_ipversion"), "1");
    assert_string_equal(GETP_CHAR(section, "values.use_ipv6"), "0");
    assert_string_equal(GETP_CHAR(section, "values.password"), "password");
    assert_string_equal(GETP_CHAR(section, "values.username"), "username");
    assert_string_equal(GETP_CHAR(section, "values.domain"), "newdomain.be");
    assert_string_equal(GETP_CHAR(section, "values.lookup_host"), "newdomain.be");
    assert_string_equal(GETP_CHAR(section, "values.enabled"), "1");
    amxc_var_delete(&section);
    //Check DM
    obj_client = amxd_object_findf(tmpl_client, ".1.");
    assert_non_null(obj_client);
    client = (ddns_client_t*) obj_client->priv;
    assert_non_null(client);
    tmp_var = amxd_object_get_param_value(obj_client, "Server");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), amxc_string_get(&server_on_odl, 0));
    tmp_var = amxd_object_get_param_value(obj_client, "Interface");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "Device.IP.Interface.3.");
    tmp_var = amxd_object_get_param_value(obj_client, "Username");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "username");
    tmp_var = amxd_object_get_param_value(obj_client, "Password");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "password");
    tmp_var = amxd_object_get_param_value(obj_client, "Enable");
    assert_non_null(tmp_var);
    assert_true(GET_BOOL(tmp_var, NULL));
    tmp_var = amxd_object_get_param_value(obj_client, "Status");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "Authenticating");
    tmp_var = amxd_object_get_param_value(obj_client, "LastError");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "NO_ERROR");
    tmpl_hostname = amxd_object_findf(obj_client, ".Hostname.");
    obj_hostname = amxd_object_findf(tmpl_hostname, ".1.");
    assert_non_null(obj_hostname);
    tmp_var = amxd_object_get_param_value(obj_hostname, "Name");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "newdomain.be");
    tmp_var = amxd_object_get_param_value(obj_hostname, "Enable");
    assert_non_null(tmp_var);
    assert_true(GET_BOOL(tmp_var, NULL));
    tmp_var = amxd_object_get_param_value(obj_hostname, "Status");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "Updating");
    tmp_var = amxd_object_get_param_value(obj_hostname, "LastUpdate");
    assert_non_null(tmp_var);
    str_var = amxc_var_dyncast(cstring_t, tmp_var);
    assert_string_equal(str_var, last_str_var);
    free(last_str_var);
    last_str_var = strdup(str_var);
    free(str_var);
    assert_string_equal(amxc_string_get(&(client->linux_intf), 0), "br-lan");

    print_status("Waiting for check function");
    amxut_timer_go_to_future_ms(3001);
    amxut_bus_handle_events();

    //Check UCI Section
    assert_int_equal(uci_sections(), 2);
    assert_int_equal(processes_running(), 0);
    section = get_section("section_1_1");
    assert_string_equal(GETP_CHAR(section, "values.service_name"), TEST_SERVICE_DEFAULT);
    assert_string_equal(GETP_CHAR(section, "values.use_https"), "1");
    assert_string_equal(GETP_CHAR(section, "values.check_interval"), "10");
    assert_string_equal(GETP_CHAR(section, "values.check_unit"), "minutes");
    assert_string_equal(GETP_CHAR(section, "values.force_interval"), "20000");
    assert_string_equal(GETP_CHAR(section, "values.force_unit"), "days");
    assert_string_equal(GETP_CHAR(section, "values.retry_interval"), "60");
    assert_string_equal(GETP_CHAR(section, "values.retry_unit"), "seconds");
    assert_string_equal(GETP_CHAR(section, "values.retry_max_count"), "0");
    assert_string_equal(GETP_CHAR(section, "values.ip_source"), "interface");
    assert_string_equal(GETP_CHAR(section, "values.ip_interface"), "br-lan");
    assert_string_equal(GETP_CHAR(section, "values.ip_script"), "/usr/lib/amx/scripts/ddns_scripts_echo_ip.sh");
    assert_string_equal(GETP_CHAR(section, "values.force_ipversion"), "1");
    assert_string_equal(GETP_CHAR(section, "values.use_ipv6"), "0");
    assert_string_equal(GETP_CHAR(section, "values.password"), "password");
    assert_string_equal(GETP_CHAR(section, "values.username"), "username");
    assert_string_equal(GETP_CHAR(section, "values.domain"), "newdomain.be");
    assert_string_equal(GETP_CHAR(section, "values.lookup_host"), "newdomain.be");
    assert_string_equal(GETP_CHAR(section, "values.enabled"), "1");
    amxc_var_delete(&section);
    //Check DM
    obj_client = amxd_object_findf(tmpl_client, ".1.");
    assert_non_null(obj_client);
    client = (ddns_client_t*) obj_client->priv;
    assert_non_null(client);
    tmp_var = amxd_object_get_param_value(obj_client, "Server");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), amxc_string_get(&server_on_odl, 0));
    tmp_var = amxd_object_get_param_value(obj_client, "Interface");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "Device.IP.Interface.3.");
    tmp_var = amxd_object_get_param_value(obj_client, "Username");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "username");
    tmp_var = amxd_object_get_param_value(obj_client, "Password");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "password");
    tmp_var = amxd_object_get_param_value(obj_client, "Enable");
    assert_non_null(tmp_var);
    assert_true(GET_BOOL(tmp_var, NULL));
    tmp_var = amxd_object_get_param_value(obj_client, "Status");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "Updated");
    tmp_var = amxd_object_get_param_value(obj_client, "LastError");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "NO_ERROR");
    tmpl_hostname = amxd_object_findf(obj_client, ".Hostname.");
    obj_hostname = amxd_object_findf(tmpl_hostname, ".1.");
    assert_non_null(obj_hostname);
    tmp_var = amxd_object_get_param_value(obj_hostname, "Name");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "newdomain.be");
    tmp_var = amxd_object_get_param_value(obj_hostname, "Enable");
    assert_non_null(tmp_var);
    assert_true(GET_BOOL(tmp_var, NULL));
    tmp_var = amxd_object_get_param_value(obj_hostname, "Status");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "Registered");
    tmp_var = amxd_object_get_param_value(obj_hostname, "LastUpdate");
    assert_non_null(tmp_var);
    str_var = amxc_var_dyncast(cstring_t, tmp_var);
    assert_string_not_equal(str_var, last_str_var);
    free(last_str_var);
    last_str_var = strdup(str_var);
    free(str_var);
    assert_string_equal(amxc_string_get(&(client->linux_intf), 0), "br-lan");

    //Change DNSServer to 8.8.8.8
    print_status("Change DNSServer to 8.8.8.8");
    assert_int_equal(amxd_trans_init(&trans), 0);
    amxd_trans_select_pathf(&trans, "DynamicDNS.Client.1.");
    amxd_trans_set_cstring_t(&trans, "DNSServer", "8.8.8.8");
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxut_bus_handle_events();
    amxd_trans_clean(&trans);

    //Check UCI Section
    assert_int_equal(uci_sections(), 2);
    assert_int_equal(processes_running(), 2);
    section = get_section("section_1_1");
    assert_string_equal(GETP_CHAR(section, "values.service_name"), TEST_SERVICE_DEFAULT);
    assert_string_equal(GETP_CHAR(section, "values.use_https"), "1");
    assert_string_equal(GETP_CHAR(section, "values.check_interval"), "10");
    assert_string_equal(GETP_CHAR(section, "values.check_unit"), "minutes");
    assert_string_equal(GETP_CHAR(section, "values.force_interval"), "20000");
    assert_string_equal(GETP_CHAR(section, "values.force_unit"), "days");
    assert_string_equal(GETP_CHAR(section, "values.retry_interval"), "60");
    assert_string_equal(GETP_CHAR(section, "values.retry_unit"), "seconds");
    assert_string_equal(GETP_CHAR(section, "values.retry_max_count"), "0");
    assert_string_equal(GETP_CHAR(section, "values.ip_source"), "interface");
    assert_string_equal(GETP_CHAR(section, "values.ip_interface"), "br-lan");
    assert_string_equal(GETP_CHAR(section, "values.ip_script"), "/usr/lib/amx/scripts/ddns_scripts_echo_ip.sh");
    assert_string_equal(GETP_CHAR(section, "values.force_ipversion"), "1");
    assert_string_equal(GETP_CHAR(section, "values.use_ipv6"), "0");
    assert_string_equal(GETP_CHAR(section, "values.dns_server"), "8.8.8.8");
    assert_string_equal(GETP_CHAR(section, "values.password"), "password");
    assert_string_equal(GETP_CHAR(section, "values.username"), "username");
    assert_string_equal(GETP_CHAR(section, "values.domain"), "newdomain.be");
    assert_string_equal(GETP_CHAR(section, "values.lookup_host"), "newdomain.be");
    assert_string_equal(GETP_CHAR(section, "values.enabled"), "1");
    amxc_var_delete(&section);
    //Check DM
    obj_client = amxd_object_findf(tmpl_client, ".1.");
    assert_non_null(obj_client);
    client = (ddns_client_t*) obj_client->priv;
    assert_non_null(client);
    tmp_var = amxd_object_get_param_value(obj_client, "Server");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), amxc_string_get(&server_on_odl, 0));
    tmp_var = amxd_object_get_param_value(obj_client, "Interface");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "Device.IP.Interface.3.");
    tmp_var = amxd_object_get_param_value(obj_client, "Username");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "username");
    tmp_var = amxd_object_get_param_value(obj_client, "Password");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "password");
    tmp_var = amxd_object_get_param_value(obj_client, "Enable");
    assert_non_null(tmp_var);
    assert_true(GET_BOOL(tmp_var, NULL));
    tmp_var = amxd_object_get_param_value(obj_client, "Status");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "Authenticating");
    tmp_var = amxd_object_get_param_value(obj_client, "LastError");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "NO_ERROR");
    tmpl_hostname = amxd_object_findf(obj_client, ".Hostname.");
    obj_hostname = amxd_object_findf(tmpl_hostname, ".1.");
    assert_non_null(obj_hostname);
    tmp_var = amxd_object_get_param_value(obj_hostname, "Name");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "newdomain.be");
    tmp_var = amxd_object_get_param_value(obj_hostname, "Enable");
    assert_non_null(tmp_var);
    assert_true(GET_BOOL(tmp_var, NULL));
    tmp_var = amxd_object_get_param_value(obj_hostname, "Status");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "Updating");
    tmp_var = amxd_object_get_param_value(obj_hostname, "LastUpdate");
    assert_non_null(tmp_var);
    str_var = amxc_var_dyncast(cstring_t, tmp_var);
    assert_string_equal(str_var, last_str_var);
    free(last_str_var);
    last_str_var = strdup(str_var);
    free(str_var);
    assert_string_equal(amxc_string_get(&(client->linux_intf), 0), "br-lan");

    print_status("Waiting for check function");
    amxut_timer_go_to_future_ms(3001);
    amxut_bus_handle_events();

    //Check UCI Section
    assert_int_equal(uci_sections(), 2);
    assert_int_equal(processes_running(), 0);
    section = get_section("section_1_1");
    assert_string_equal(GETP_CHAR(section, "values.service_name"), TEST_SERVICE_DEFAULT);
    assert_string_equal(GETP_CHAR(section, "values.use_https"), "1");
    assert_string_equal(GETP_CHAR(section, "values.check_interval"), "10");
    assert_string_equal(GETP_CHAR(section, "values.check_unit"), "minutes");
    assert_string_equal(GETP_CHAR(section, "values.force_interval"), "20000");
    assert_string_equal(GETP_CHAR(section, "values.force_unit"), "days");
    assert_string_equal(GETP_CHAR(section, "values.retry_interval"), "60");
    assert_string_equal(GETP_CHAR(section, "values.retry_unit"), "seconds");
    assert_string_equal(GETP_CHAR(section, "values.retry_max_count"), "0");
    assert_string_equal(GETP_CHAR(section, "values.ip_source"), "interface");
    assert_string_equal(GETP_CHAR(section, "values.ip_interface"), "br-lan");
    assert_string_equal(GETP_CHAR(section, "values.ip_script"), "/usr/lib/amx/scripts/ddns_scripts_echo_ip.sh");
    assert_string_equal(GETP_CHAR(section, "values.force_ipversion"), "1");
    assert_string_equal(GETP_CHAR(section, "values.use_ipv6"), "0");
    assert_string_equal(GETP_CHAR(section, "values.dns_server"), "8.8.8.8");
    assert_string_equal(GETP_CHAR(section, "values.password"), "password");
    assert_string_equal(GETP_CHAR(section, "values.username"), "username");
    assert_string_equal(GETP_CHAR(section, "values.domain"), "newdomain.be");
    assert_string_equal(GETP_CHAR(section, "values.lookup_host"), "newdomain.be");
    assert_string_equal(GETP_CHAR(section, "values.enabled"), "1");
    amxc_var_delete(&section);
    //Check DM
    obj_client = amxd_object_findf(tmpl_client, ".1.");
    assert_non_null(obj_client);
    client = (ddns_client_t*) obj_client->priv;
    assert_non_null(client);
    tmp_var = amxd_object_get_param_value(obj_client, "Server");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), amxc_string_get(&server_on_odl, 0));
    tmp_var = amxd_object_get_param_value(obj_client, "Interface");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "Device.IP.Interface.3.");
    tmp_var = amxd_object_get_param_value(obj_client, "Username");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "username");
    tmp_var = amxd_object_get_param_value(obj_client, "Password");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "password");
    tmp_var = amxd_object_get_param_value(obj_client, "Enable");
    assert_non_null(tmp_var);
    assert_true(GET_BOOL(tmp_var, NULL));
    tmp_var = amxd_object_get_param_value(obj_client, "Status");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "Updated");
    tmp_var = amxd_object_get_param_value(obj_client, "LastError");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "NO_ERROR");
    tmpl_hostname = amxd_object_findf(obj_client, ".Hostname.");
    obj_hostname = amxd_object_findf(tmpl_hostname, ".1.");
    assert_non_null(obj_hostname);
    tmp_var = amxd_object_get_param_value(obj_hostname, "Name");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "newdomain.be");
    tmp_var = amxd_object_get_param_value(obj_hostname, "Enable");
    assert_non_null(tmp_var);
    assert_true(GET_BOOL(tmp_var, NULL));
    tmp_var = amxd_object_get_param_value(obj_hostname, "Status");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "Registered");
    tmp_var = amxd_object_get_param_value(obj_hostname, "LastUpdate");
    assert_non_null(tmp_var);
    str_var = amxc_var_dyncast(cstring_t, tmp_var);
    assert_string_not_equal(str_var, last_str_var);
    free(last_str_var);
    last_str_var = strdup(str_var);
    free(str_var);
    assert_string_equal(amxc_string_get(&(client->linux_intf), 0), "br-lan");

    //Change DNSServer to empty string
    print_status("Change DNSServer to empty string");
    assert_int_equal(amxd_trans_init(&trans), 0);
    amxd_trans_select_pathf(&trans, "DynamicDNS.Client.1.");
    amxd_trans_set_cstring_t(&trans, "DNSServer", "");
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxut_bus_handle_events();
    amxd_trans_clean(&trans);

    //Check UCI Section
    assert_int_equal(uci_sections(), 2);
    assert_int_equal(processes_running(), 2);
    section = get_section("section_1_1");
    assert_string_equal(GETP_CHAR(section, "values.service_name"), TEST_SERVICE_DEFAULT);
    assert_string_equal(GETP_CHAR(section, "values.use_https"), "1");
    assert_string_equal(GETP_CHAR(section, "values.check_interval"), "10");
    assert_string_equal(GETP_CHAR(section, "values.check_unit"), "minutes");
    assert_string_equal(GETP_CHAR(section, "values.force_interval"), "20000");
    assert_string_equal(GETP_CHAR(section, "values.force_unit"), "days");
    assert_string_equal(GETP_CHAR(section, "values.retry_interval"), "60");
    assert_string_equal(GETP_CHAR(section, "values.retry_unit"), "seconds");
    assert_string_equal(GETP_CHAR(section, "values.retry_max_count"), "0");
    assert_string_equal(GETP_CHAR(section, "values.ip_source"), "interface");
    assert_string_equal(GETP_CHAR(section, "values.ip_interface"), "br-lan");
    assert_string_equal(GETP_CHAR(section, "values.ip_script"), "/usr/lib/amx/scripts/ddns_scripts_echo_ip.sh");
    assert_string_equal(GETP_CHAR(section, "values.force_ipversion"), "1");
    assert_string_equal(GETP_CHAR(section, "values.use_ipv6"), "0");
    assert_string_equal(GETP_CHAR(section, "values.dns_server"), "");
    assert_string_equal(GETP_CHAR(section, "values.password"), "password");
    assert_string_equal(GETP_CHAR(section, "values.username"), "username");
    assert_string_equal(GETP_CHAR(section, "values.domain"), "newdomain.be");
    assert_string_equal(GETP_CHAR(section, "values.lookup_host"), "newdomain.be");
    assert_string_equal(GETP_CHAR(section, "values.enabled"), "1");
    amxc_var_delete(&section);
    //Check DM
    obj_client = amxd_object_findf(tmpl_client, ".1.");
    assert_non_null(obj_client);
    client = (ddns_client_t*) obj_client->priv;
    assert_non_null(client);
    tmp_var = amxd_object_get_param_value(obj_client, "Server");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), amxc_string_get(&server_on_odl, 0));
    tmp_var = amxd_object_get_param_value(obj_client, "Interface");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "Device.IP.Interface.3.");
    tmp_var = amxd_object_get_param_value(obj_client, "Username");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "username");
    tmp_var = amxd_object_get_param_value(obj_client, "Password");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "password");
    tmp_var = amxd_object_get_param_value(obj_client, "Enable");
    assert_non_null(tmp_var);
    assert_true(GET_BOOL(tmp_var, NULL));
    tmp_var = amxd_object_get_param_value(obj_client, "Status");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "Authenticating");
    tmp_var = amxd_object_get_param_value(obj_client, "LastError");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "NO_ERROR");
    tmpl_hostname = amxd_object_findf(obj_client, ".Hostname.");
    obj_hostname = amxd_object_findf(tmpl_hostname, ".1.");
    assert_non_null(obj_hostname);
    tmp_var = amxd_object_get_param_value(obj_hostname, "Name");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "newdomain.be");
    tmp_var = amxd_object_get_param_value(obj_hostname, "Enable");
    assert_non_null(tmp_var);
    assert_true(GET_BOOL(tmp_var, NULL));
    tmp_var = amxd_object_get_param_value(obj_hostname, "Status");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "Updating");
    tmp_var = amxd_object_get_param_value(obj_hostname, "LastUpdate");
    assert_non_null(tmp_var);
    str_var = amxc_var_dyncast(cstring_t, tmp_var);
    assert_string_equal(str_var, last_str_var);
    free(last_str_var);
    last_str_var = strdup(str_var);
    free(str_var);
    assert_string_equal(amxc_string_get(&(client->linux_intf), 0), "br-lan");

    print_status("Waiting for check function");
    amxut_timer_go_to_future_ms(3001);
    amxut_bus_handle_events();

    //Check UCI Section
    assert_int_equal(uci_sections(), 2);
    assert_int_equal(processes_running(), 0);
    section = get_section("section_1_1");
    assert_string_equal(GETP_CHAR(section, "values.service_name"), TEST_SERVICE_DEFAULT);
    assert_string_equal(GETP_CHAR(section, "values.use_https"), "1");
    assert_string_equal(GETP_CHAR(section, "values.check_interval"), "10");
    assert_string_equal(GETP_CHAR(section, "values.check_unit"), "minutes");
    assert_string_equal(GETP_CHAR(section, "values.force_interval"), "20000");
    assert_string_equal(GETP_CHAR(section, "values.force_unit"), "days");
    assert_string_equal(GETP_CHAR(section, "values.retry_interval"), "60");
    assert_string_equal(GETP_CHAR(section, "values.retry_unit"), "seconds");
    assert_string_equal(GETP_CHAR(section, "values.retry_max_count"), "0");
    assert_string_equal(GETP_CHAR(section, "values.ip_source"), "interface");
    assert_string_equal(GETP_CHAR(section, "values.ip_interface"), "br-lan");
    assert_string_equal(GETP_CHAR(section, "values.ip_script"), "/usr/lib/amx/scripts/ddns_scripts_echo_ip.sh");
    assert_string_equal(GETP_CHAR(section, "values.force_ipversion"), "1");
    assert_string_equal(GETP_CHAR(section, "values.use_ipv6"), "0");
    assert_string_equal(GETP_CHAR(section, "values.dns_server"), "");
    assert_string_equal(GETP_CHAR(section, "values.password"), "password");
    assert_string_equal(GETP_CHAR(section, "values.username"), "username");
    assert_string_equal(GETP_CHAR(section, "values.domain"), "newdomain.be");
    assert_string_equal(GETP_CHAR(section, "values.lookup_host"), "newdomain.be");
    assert_string_equal(GETP_CHAR(section, "values.enabled"), "1");
    amxc_var_delete(&section);
    //Check DM
    obj_client = amxd_object_findf(tmpl_client, ".1.");
    assert_non_null(obj_client);
    client = (ddns_client_t*) obj_client->priv;
    assert_non_null(client);
    tmp_var = amxd_object_get_param_value(obj_client, "Server");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), amxc_string_get(&server_on_odl, 0));
    tmp_var = amxd_object_get_param_value(obj_client, "Interface");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "Device.IP.Interface.3.");
    tmp_var = amxd_object_get_param_value(obj_client, "Username");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "username");
    tmp_var = amxd_object_get_param_value(obj_client, "Password");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "password");
    tmp_var = amxd_object_get_param_value(obj_client, "Enable");
    assert_non_null(tmp_var);
    assert_true(GET_BOOL(tmp_var, NULL));
    tmp_var = amxd_object_get_param_value(obj_client, "Status");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "Updated");
    tmp_var = amxd_object_get_param_value(obj_client, "LastError");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "NO_ERROR");
    tmpl_hostname = amxd_object_findf(obj_client, ".Hostname.");
    obj_hostname = amxd_object_findf(tmpl_hostname, ".1.");
    assert_non_null(obj_hostname);
    tmp_var = amxd_object_get_param_value(obj_hostname, "Name");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "newdomain.be");
    tmp_var = amxd_object_get_param_value(obj_hostname, "Enable");
    assert_non_null(tmp_var);
    assert_true(GET_BOOL(tmp_var, NULL));
    tmp_var = amxd_object_get_param_value(obj_hostname, "Status");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "Registered");
    tmp_var = amxd_object_get_param_value(obj_hostname, "LastUpdate");
    assert_non_null(tmp_var);
    str_var = amxc_var_dyncast(cstring_t, tmp_var);
    assert_string_not_equal(str_var, last_str_var);
    free(last_str_var);
    last_str_var = strdup(str_var);
    free(str_var);
    assert_string_equal(amxc_string_get(&(client->linux_intf), 0), "br-lan");

    //Change enable
    print_status("Change Enable of DynamicDNS.Client.1.Hostname.1");
    assert_int_equal(amxd_trans_init(&trans), 0);
    amxd_trans_select_pathf(&trans, "DynamicDNS.Client.1.Hostname.1.");
    amxc_var_new(&value);
    amxc_var_set(bool, value, false);
    amxd_trans_set_param(&trans, "Enable", value);
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxut_bus_handle_events();
    amxd_trans_clean(&trans);
    amxc_var_delete(&value);

    //Check UCI Section
    assert_int_equal(uci_sections(), 2);
    assert_int_equal(processes_running(), 0);
    section = get_section("section_1_1");
    assert_string_equal(GETP_CHAR(section, "values.service_name"), TEST_SERVICE_DEFAULT);
    assert_string_equal(GETP_CHAR(section, "values.use_https"), "1");
    assert_string_equal(GETP_CHAR(section, "values.check_interval"), "10");
    assert_string_equal(GETP_CHAR(section, "values.check_unit"), "minutes");
    assert_string_equal(GETP_CHAR(section, "values.force_interval"), "20000");
    assert_string_equal(GETP_CHAR(section, "values.force_unit"), "days");
    assert_string_equal(GETP_CHAR(section, "values.retry_interval"), "60");
    assert_string_equal(GETP_CHAR(section, "values.retry_unit"), "seconds");
    assert_string_equal(GETP_CHAR(section, "values.retry_max_count"), "0");
    assert_string_equal(GETP_CHAR(section, "values.ip_source"), "interface");
    assert_string_equal(GETP_CHAR(section, "values.ip_interface"), "br-lan");
    assert_string_equal(GETP_CHAR(section, "values.ip_script"), "/usr/lib/amx/scripts/ddns_scripts_echo_ip.sh");
    assert_string_equal(GETP_CHAR(section, "values.password"), "password");
    assert_string_equal(GETP_CHAR(section, "values.username"), "username");
    assert_string_equal(GETP_CHAR(section, "values.domain"), "newdomain.be");
    assert_string_equal(GETP_CHAR(section, "values.lookup_host"), "newdomain.be");
    assert_string_equal(GETP_CHAR(section, "values.enabled"), "0");
    amxc_var_delete(&section);
    //Check DM
    obj_client = amxd_object_findf(tmpl_client, ".1.");
    assert_non_null(obj_client);
    client = (ddns_client_t*) obj_client->priv;
    assert_non_null(client);
    tmp_var = amxd_object_get_param_value(obj_client, "Server");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), amxc_string_get(&server_on_odl, 0));
    tmp_var = amxd_object_get_param_value(obj_client, "Interface");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "Device.IP.Interface.3.");
    tmp_var = amxd_object_get_param_value(obj_client, "Username");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "username");
    tmp_var = amxd_object_get_param_value(obj_client, "Password");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "password");
    tmp_var = amxd_object_get_param_value(obj_client, "Enable");
    assert_non_null(tmp_var);
    assert_true(GET_BOOL(tmp_var, NULL));
    tmp_var = amxd_object_get_param_value(obj_client, "Status");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "Updated");
    tmp_var = amxd_object_get_param_value(obj_client, "LastError");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "NO_ERROR");
    tmpl_hostname = amxd_object_findf(obj_client, ".Hostname.");
    obj_hostname = amxd_object_findf(tmpl_hostname, ".1.");
    assert_non_null(obj_hostname);
    tmp_var = amxd_object_get_param_value(obj_hostname, "Name");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "newdomain.be");
    tmp_var = amxd_object_get_param_value(obj_hostname, "Enable");
    assert_non_null(tmp_var);
    assert_false(GET_BOOL(tmp_var, NULL));
    tmp_var = amxd_object_get_param_value(obj_hostname, "Status");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "Disabled");
    tmp_var = amxd_object_get_param_value(obj_hostname, "LastUpdate");
    assert_non_null(tmp_var);
    str_var = amxc_var_dyncast(cstring_t, tmp_var);
    assert_string_equal(str_var, last_str_var);
    free(last_str_var);
    last_str_var = strdup(str_var);
    free(str_var);
    assert_string_equal(amxc_string_get(&(client->linux_intf), 0), "br-lan");

    print_status("Waiting for check function");
    amxut_timer_go_to_future_ms(3001);
    amxut_bus_handle_events();

    //Check UCI Section
    assert_int_equal(uci_sections(), 2);
    assert_int_equal(processes_running(), 0);
    section = get_section("section_1_1");
    assert_string_equal(GETP_CHAR(section, "values.service_name"), TEST_SERVICE_DEFAULT);
    assert_string_equal(GETP_CHAR(section, "values.use_https"), "1");
    assert_string_equal(GETP_CHAR(section, "values.check_interval"), "10");
    assert_string_equal(GETP_CHAR(section, "values.check_unit"), "minutes");
    assert_string_equal(GETP_CHAR(section, "values.force_interval"), "20000");
    assert_string_equal(GETP_CHAR(section, "values.force_unit"), "days");
    assert_string_equal(GETP_CHAR(section, "values.retry_interval"), "60");
    assert_string_equal(GETP_CHAR(section, "values.retry_unit"), "seconds");
    assert_string_equal(GETP_CHAR(section, "values.retry_max_count"), "0");
    assert_string_equal(GETP_CHAR(section, "values.ip_source"), "interface");
    assert_string_equal(GETP_CHAR(section, "values.ip_interface"), "br-lan");
    assert_string_equal(GETP_CHAR(section, "values.ip_script"), "/usr/lib/amx/scripts/ddns_scripts_echo_ip.sh");
    assert_string_equal(GETP_CHAR(section, "values.password"), "password");
    assert_string_equal(GETP_CHAR(section, "values.username"), "username");
    assert_string_equal(GETP_CHAR(section, "values.domain"), "newdomain.be");
    assert_string_equal(GETP_CHAR(section, "values.lookup_host"), "newdomain.be");
    assert_string_equal(GETP_CHAR(section, "values.enabled"), "0");
    amxc_var_delete(&section);
    //Check DM
    obj_client = amxd_object_findf(tmpl_client, ".1.");
    assert_non_null(obj_client);
    client = (ddns_client_t*) obj_client->priv;
    assert_non_null(client);
    tmp_var = amxd_object_get_param_value(obj_client, "Server");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), amxc_string_get(&server_on_odl, 0));
    tmp_var = amxd_object_get_param_value(obj_client, "Interface");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "Device.IP.Interface.3.");
    tmp_var = amxd_object_get_param_value(obj_client, "Username");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "username");
    tmp_var = amxd_object_get_param_value(obj_client, "Password");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "password");
    tmp_var = amxd_object_get_param_value(obj_client, "Enable");
    assert_non_null(tmp_var);
    assert_true(GET_BOOL(tmp_var, NULL));
    tmp_var = amxd_object_get_param_value(obj_client, "Status");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "Updated");
    tmp_var = amxd_object_get_param_value(obj_client, "LastError");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "NO_ERROR");
    tmpl_hostname = amxd_object_findf(obj_client, ".Hostname.");
    obj_hostname = amxd_object_findf(tmpl_hostname, ".1.");
    assert_non_null(obj_hostname);
    tmp_var = amxd_object_get_param_value(obj_hostname, "Name");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "newdomain.be");
    tmp_var = amxd_object_get_param_value(obj_hostname, "Enable");
    assert_non_null(tmp_var);
    assert_false(GET_BOOL(tmp_var, NULL));
    tmp_var = amxd_object_get_param_value(obj_hostname, "Status");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "Disabled");
    tmp_var = amxd_object_get_param_value(obj_hostname, "LastUpdate");
    assert_non_null(tmp_var);
    str_var = amxc_var_dyncast(cstring_t, tmp_var);
    assert_string_equal(str_var, last_str_var);
    free(last_str_var);
    last_str_var = strdup(str_var);
    free(str_var);
    assert_string_equal(amxc_string_get(&(client->linux_intf), 0), "br-lan");

    //Change enable
    print_status("Change Enable of DynamicDNS.Client.1");
    assert_int_equal(amxd_trans_init(&trans), 0);
    amxd_trans_select_pathf(&trans, "DynamicDNS.Client.1.");
    amxc_var_new(&value);
    amxc_var_set(bool, value, false);
    amxd_trans_set_param(&trans, "Enable", value);
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxut_bus_handle_events();
    amxd_trans_clean(&trans);
    amxc_var_delete(&value);

    //Check UCI Section
    assert_int_equal(uci_sections(), 2);
    assert_int_equal(processes_running(), 0);
    section = get_section("section_1_2");
    assert_string_equal(GETP_CHAR(section, "values.service_name"), TEST_SERVICE_DEFAULT);
    assert_string_equal(GETP_CHAR(section, "values.use_https"), "1");
    assert_string_equal(GETP_CHAR(section, "values.check_interval"), "10");
    assert_string_equal(GETP_CHAR(section, "values.check_unit"), "minutes");
    assert_string_equal(GETP_CHAR(section, "values.force_interval"), "20000");
    assert_string_equal(GETP_CHAR(section, "values.force_unit"), "days");
    assert_string_equal(GETP_CHAR(section, "values.retry_interval"), "60");
    assert_string_equal(GETP_CHAR(section, "values.retry_unit"), "seconds");
    assert_string_equal(GETP_CHAR(section, "values.retry_max_count"), "0");
    assert_string_equal(GETP_CHAR(section, "values.ip_source"), "interface");
    assert_string_equal(GETP_CHAR(section, "values.ip_interface"), "br-lan");
    assert_string_equal(GETP_CHAR(section, "values.ip_script"), "/usr/lib/amx/scripts/ddns_scripts_echo_ip.sh");
    assert_string_equal(GETP_CHAR(section, "values.password"), "password");
    assert_string_equal(GETP_CHAR(section, "values.username"), "username");
    assert_string_equal(GETP_CHAR(section, "values.domain"), "domain.br");
    assert_string_equal(GETP_CHAR(section, "values.lookup_host"), "domain.br");
    assert_string_equal(GETP_CHAR(section, "values.enabled"), "0");
    amxc_var_delete(&section);
    //Check DM
    obj_client = amxd_object_findf(tmpl_client, ".1.");
    assert_non_null(obj_client);
    client = (ddns_client_t*) obj_client->priv;
    assert_non_null(client);
    tmp_var = amxd_object_get_param_value(obj_client, "Server");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), amxc_string_get(&server_on_odl, 0));
    tmp_var = amxd_object_get_param_value(obj_client, "Interface");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "Device.IP.Interface.3.");
    tmp_var = amxd_object_get_param_value(obj_client, "Username");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "username");
    tmp_var = amxd_object_get_param_value(obj_client, "Password");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "password");
    tmp_var = amxd_object_get_param_value(obj_client, "Enable");
    assert_non_null(tmp_var);
    assert_false(GET_BOOL(tmp_var, NULL));
    tmp_var = amxd_object_get_param_value(obj_client, "Status");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "Disabled");
    tmp_var = amxd_object_get_param_value(obj_client, "LastError");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "NO_ERROR");
    tmpl_hostname = amxd_object_findf(obj_client, ".Hostname.");
    obj_hostname = amxd_object_findf(tmpl_hostname, ".1.");
    assert_non_null(obj_hostname);
    tmp_var = amxd_object_get_param_value(obj_hostname, "Name");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "newdomain.be");
    tmp_var = amxd_object_get_param_value(obj_hostname, "Enable");
    assert_non_null(tmp_var);
    assert_false(GET_BOOL(tmp_var, NULL));
    tmp_var = amxd_object_get_param_value(obj_hostname, "Status");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "Disabled");
    tmp_var = amxd_object_get_param_value(obj_hostname, "LastUpdate");
    assert_non_null(tmp_var);
    str_var = amxc_var_dyncast(cstring_t, tmp_var);
    assert_string_equal(str_var, last_str_var);
    free(last_str_var);
    last_str_var = strdup(str_var);
    free(str_var);
    obj_hostname = amxd_object_findf(tmpl_hostname, ".2.");
    assert_non_null(obj_hostname);
    tmp_var = amxd_object_get_param_value(obj_hostname, "Name");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "domain.br");
    tmp_var = amxd_object_get_param_value(obj_hostname, "Enable");
    assert_non_null(tmp_var);
    assert_true(GET_BOOL(tmp_var, NULL));
    tmp_var = amxd_object_get_param_value(obj_hostname, "Status");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "Disabled");
    tmp_var = amxd_object_get_param_value(obj_hostname, "LastUpdate");
    assert_non_null(tmp_var);
    str_var = amxc_var_dyncast(cstring_t, tmp_var);
    assert_string_equal(str_var, last_str_2_var);
    free(last_str_2_var);
    last_str_2_var = strdup(str_var);
    free(str_var);
    assert_string_equal(amxc_string_get(&(client->linux_intf), 0), "br-lan");

    print_status("Waiting for check function");
    amxut_timer_go_to_future_ms(3001);
    amxut_bus_handle_events();

    //Check UCI Section
    assert_int_equal(uci_sections(), 2);
    assert_int_equal(processes_running(), 0);
    section = get_section("section_1_2");
    assert_string_equal(GETP_CHAR(section, "values.service_name"), TEST_SERVICE_DEFAULT);
    assert_string_equal(GETP_CHAR(section, "values.use_https"), "1");
    assert_string_equal(GETP_CHAR(section, "values.check_interval"), "10");
    assert_string_equal(GETP_CHAR(section, "values.check_unit"), "minutes");
    assert_string_equal(GETP_CHAR(section, "values.force_interval"), "20000");
    assert_string_equal(GETP_CHAR(section, "values.force_unit"), "days");
    assert_string_equal(GETP_CHAR(section, "values.retry_interval"), "60");
    assert_string_equal(GETP_CHAR(section, "values.retry_unit"), "seconds");
    assert_string_equal(GETP_CHAR(section, "values.retry_max_count"), "0");
    assert_string_equal(GETP_CHAR(section, "values.ip_source"), "interface");
    assert_string_equal(GETP_CHAR(section, "values.ip_interface"), "br-lan");
    assert_string_equal(GETP_CHAR(section, "values.ip_script"), "/usr/lib/amx/scripts/ddns_scripts_echo_ip.sh");
    assert_string_equal(GETP_CHAR(section, "values.password"), "password");
    assert_string_equal(GETP_CHAR(section, "values.username"), "username");
    assert_string_equal(GETP_CHAR(section, "values.domain"), "domain.br");
    assert_string_equal(GETP_CHAR(section, "values.lookup_host"), "domain.br");
    assert_string_equal(GETP_CHAR(section, "values.enabled"), "0");
    amxc_var_delete(&section);
    //Check DM
    obj_client = amxd_object_findf(tmpl_client, ".1.");
    assert_non_null(obj_client);
    client = (ddns_client_t*) obj_client->priv;
    assert_non_null(client);
    tmp_var = amxd_object_get_param_value(obj_client, "Server");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), amxc_string_get(&server_on_odl, 0));
    tmp_var = amxd_object_get_param_value(obj_client, "Interface");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "Device.IP.Interface.3.");
    tmp_var = amxd_object_get_param_value(obj_client, "Username");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "username");
    tmp_var = amxd_object_get_param_value(obj_client, "Password");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "password");
    tmp_var = amxd_object_get_param_value(obj_client, "Enable");
    assert_non_null(tmp_var);
    assert_false(GET_BOOL(tmp_var, NULL));
    tmp_var = amxd_object_get_param_value(obj_client, "Status");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "Disabled");
    tmp_var = amxd_object_get_param_value(obj_client, "LastError");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "NO_ERROR");
    tmpl_hostname = amxd_object_findf(obj_client, ".Hostname.");
    obj_hostname = amxd_object_findf(tmpl_hostname, ".1.");
    assert_non_null(obj_hostname);
    tmp_var = amxd_object_get_param_value(obj_hostname, "Name");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "newdomain.be");
    tmp_var = amxd_object_get_param_value(obj_hostname, "Enable");
    assert_non_null(tmp_var);
    assert_false(GET_BOOL(tmp_var, NULL));
    tmp_var = amxd_object_get_param_value(obj_hostname, "Status");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "Disabled");
    tmp_var = amxd_object_get_param_value(obj_hostname, "LastUpdate");
    assert_non_null(tmp_var);
    str_var = amxc_var_dyncast(cstring_t, tmp_var);
    assert_string_equal(str_var, last_str_var);
    free(last_str_var);
    last_str_var = strdup(str_var);
    free(str_var);
    obj_hostname = amxd_object_findf(tmpl_hostname, ".2.");
    assert_non_null(obj_hostname);
    tmp_var = amxd_object_get_param_value(obj_hostname, "Name");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "domain.br");
    tmp_var = amxd_object_get_param_value(obj_hostname, "Enable");
    assert_non_null(tmp_var);
    assert_true(GET_BOOL(tmp_var, NULL));
    tmp_var = amxd_object_get_param_value(obj_hostname, "Status");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "Disabled");
    tmp_var = amxd_object_get_param_value(obj_hostname, "LastUpdate");
    assert_non_null(tmp_var);
    str_var = amxc_var_dyncast(cstring_t, tmp_var);
    assert_string_equal(str_var, last_str_2_var);
    free(last_str_2_var);
    last_str_2_var = strdup(str_var);
    free(str_var);
    assert_string_equal(amxc_string_get(&(client->linux_intf), 0), "br-lan");

    //Remove Client Hostname
    print_status("Remove DynamicDNS.Client.1.Hostname.1");
    assert_int_equal(amxd_trans_init(&trans), 0);
    amxd_trans_select_pathf(&trans, "DynamicDNS.Client.1.Hostname.");
    amxd_trans_del_inst(&trans, 1, NULL);
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxut_bus_handle_events();
    amxd_trans_clean(&trans);
    amxc_var_delete(&value);
    assert_int_equal(uci_sections(), 1);
    assert_int_equal(processes_running(), 0);

    //Add Client.1.Hostname instance
    print_status("Add new instance DynamicDNS.Client.1.Hostname.3");
    assert_int_equal(amxd_trans_init(&trans), 0);
    amxd_trans_select_pathf(&trans, "DynamicDNS.Client.1.Hostname.");
    amxd_trans_add_inst(&trans, 0, NULL);
    amxd_trans_set_value(cstring_t, &trans, "Name", "domain.com");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxut_bus_handle_events();
    amxd_trans_clean(&trans);

    //Check UCI Section
    assert_int_equal(uci_sections(), 2);
    assert_int_equal(processes_running(), 0);
    section = get_section("section_1_3");
    assert_string_equal(GETP_CHAR(section, "values.service_name"), TEST_SERVICE_DEFAULT);
    assert_string_equal(GETP_CHAR(section, "values.use_https"), "1");
    assert_string_equal(GETP_CHAR(section, "values.check_interval"), "10");
    assert_string_equal(GETP_CHAR(section, "values.check_unit"), "minutes");
    assert_string_equal(GETP_CHAR(section, "values.force_interval"), "20000");
    assert_string_equal(GETP_CHAR(section, "values.force_unit"), "days");
    assert_string_equal(GETP_CHAR(section, "values.retry_interval"), "60");
    assert_string_equal(GETP_CHAR(section, "values.retry_unit"), "seconds");
    assert_string_equal(GETP_CHAR(section, "values.retry_max_count"), "0");
    assert_string_equal(GETP_CHAR(section, "values.ip_source"), "interface");
    assert_string_equal(GETP_CHAR(section, "values.ip_interface"), "br-lan");
    assert_string_equal(GETP_CHAR(section, "values.ip_script"), "/usr/lib/amx/scripts/ddns_scripts_echo_ip.sh");
    assert_string_equal(GETP_CHAR(section, "values.password"), "password");
    assert_string_equal(GETP_CHAR(section, "values.username"), "username");
    assert_string_equal(GETP_CHAR(section, "values.domain"), "domain.com");
    assert_string_equal(GETP_CHAR(section, "values.lookup_host"), "domain.com");
    assert_string_equal(GETP_CHAR(section, "values.enabled"), "0");
    amxc_var_delete(&section);
    //Check DM
    obj_client = amxd_object_findf(tmpl_client, ".1.");
    assert_non_null(obj_client);
    client = (ddns_client_t*) obj_client->priv;
    assert_non_null(client);
    tmp_var = amxd_object_get_param_value(obj_client, "Server");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), amxc_string_get(&server_on_odl, 0));
    tmp_var = amxd_object_get_param_value(obj_client, "Interface");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "Device.IP.Interface.3.");
    tmp_var = amxd_object_get_param_value(obj_client, "Username");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "username");
    tmp_var = amxd_object_get_param_value(obj_client, "Password");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "password");
    tmp_var = amxd_object_get_param_value(obj_client, "Enable");
    assert_non_null(tmp_var);
    assert_false(GET_BOOL(tmp_var, NULL));
    tmp_var = amxd_object_get_param_value(obj_client, "Status");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "Disabled");
    tmp_var = amxd_object_get_param_value(obj_client, "LastError");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "NO_ERROR");
    tmpl_hostname = amxd_object_findf(obj_client, ".Hostname.");
    obj_hostname = amxd_object_findf(tmpl_hostname, ".3.");
    assert_non_null(obj_hostname);
    tmp_var = amxd_object_get_param_value(obj_hostname, "Name");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "domain.com");
    tmp_var = amxd_object_get_param_value(obj_hostname, "Enable");
    assert_non_null(tmp_var);
    assert_true(GET_BOOL(tmp_var, NULL));
    tmp_var = amxd_object_get_param_value(obj_hostname, "Status");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "Disabled");
    tmp_var = amxd_object_get_param_value(obj_hostname, "LastUpdate");
    assert_non_null(tmp_var);
    str_var = amxc_var_dyncast(cstring_t, tmp_var);
    assert_string_equal(str_var, "0001-01-01T00:00:00Z");
    last_str_3_var = strdup(str_var);
    free(str_var);
    assert_string_equal(amxc_string_get(&(client->linux_intf), 0), "br-lan");

    print_status("Waiting for check function");
    amxut_timer_go_to_future_ms(3001);
    amxut_bus_handle_events();

    //Check UCI Section
    assert_int_equal(uci_sections(), 2);
    assert_int_equal(processes_running(), 0);
    section = get_section("section_1_3");
    assert_string_equal(GETP_CHAR(section, "values.service_name"), TEST_SERVICE_DEFAULT);
    assert_string_equal(GETP_CHAR(section, "values.use_https"), "1");
    assert_string_equal(GETP_CHAR(section, "values.check_interval"), "10");
    assert_string_equal(GETP_CHAR(section, "values.check_unit"), "minutes");
    assert_string_equal(GETP_CHAR(section, "values.force_interval"), "20000");
    assert_string_equal(GETP_CHAR(section, "values.force_unit"), "days");
    assert_string_equal(GETP_CHAR(section, "values.retry_interval"), "60");
    assert_string_equal(GETP_CHAR(section, "values.retry_unit"), "seconds");
    assert_string_equal(GETP_CHAR(section, "values.retry_max_count"), "0");
    assert_string_equal(GETP_CHAR(section, "values.ip_source"), "interface");
    assert_string_equal(GETP_CHAR(section, "values.ip_interface"), "br-lan");
    assert_string_equal(GETP_CHAR(section, "values.ip_script"), "/usr/lib/amx/scripts/ddns_scripts_echo_ip.sh");
    assert_string_equal(GETP_CHAR(section, "values.password"), "password");
    assert_string_equal(GETP_CHAR(section, "values.username"), "username");
    assert_string_equal(GETP_CHAR(section, "values.domain"), "domain.com");
    assert_string_equal(GETP_CHAR(section, "values.lookup_host"), "domain.com");
    assert_string_equal(GETP_CHAR(section, "values.enabled"), "0");
    amxc_var_delete(&section);
    //Check DM
    obj_client = amxd_object_findf(tmpl_client, ".1.");
    assert_non_null(obj_client);
    client = (ddns_client_t*) obj_client->priv;
    assert_non_null(client);
    tmp_var = amxd_object_get_param_value(obj_client, "Server");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), amxc_string_get(&server_on_odl, 0));
    tmp_var = amxd_object_get_param_value(obj_client, "Interface");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "Device.IP.Interface.3.");
    tmp_var = amxd_object_get_param_value(obj_client, "Username");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "username");
    tmp_var = amxd_object_get_param_value(obj_client, "Password");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "password");
    tmp_var = amxd_object_get_param_value(obj_client, "Enable");
    assert_non_null(tmp_var);
    assert_false(GET_BOOL(tmp_var, NULL));
    tmp_var = amxd_object_get_param_value(obj_client, "Status");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "Disabled");
    tmp_var = amxd_object_get_param_value(obj_client, "LastError");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "NO_ERROR");
    tmpl_hostname = amxd_object_findf(obj_client, ".Hostname.");
    obj_hostname = amxd_object_findf(tmpl_hostname, ".3.");
    assert_non_null(obj_hostname);
    tmp_var = amxd_object_get_param_value(obj_hostname, "Name");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "domain.com");
    tmp_var = amxd_object_get_param_value(obj_hostname, "Enable");
    assert_non_null(tmp_var);
    assert_true(GET_BOOL(tmp_var, NULL));
    tmp_var = amxd_object_get_param_value(obj_hostname, "Status");
    assert_non_null(tmp_var);
    assert_string_equal(GET_CHAR(tmp_var, NULL), "Disabled");
    tmp_var = amxd_object_get_param_value(obj_hostname, "LastUpdate");
    assert_non_null(tmp_var);
    str_var = amxc_var_dyncast(cstring_t, tmp_var);
    assert_string_equal(str_var, last_str_3_var);
    free(last_str_3_var);
    last_str_3_var = strdup(str_var);
    free(str_var);
    assert_string_equal(amxc_string_get(&(client->linux_intf), 0), "br-lan");

    //Remove Client
    print_status("Remove DynamicDNS.Client.1");
    assert_int_equal(amxd_trans_init(&trans), 0);
    amxd_trans_select_pathf(&trans, "DynamicDNS.Client.");
    amxd_trans_del_inst(&trans, 1, NULL);
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxut_bus_handle_events();
    amxd_trans_clean(&trans);
    amxc_var_delete(&value);

    //Check DM
    assert_int_equal(uci_sections(), 0);
    assert_int_equal(processes_running(), 0);
    obj_client = amxd_object_findf(tmpl_client, ".1.");
    assert_null(obj_client);

    amxc_var_delete(&value);
    amxd_trans_clean(&trans);
    free(last_str_var);
    free(last_str_2_var);
    free(last_str_3_var);
}