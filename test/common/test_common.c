/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <string.h>
#include <regex.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <math.h>
#include <event2/event.h>
#include <pthread.h>

#include "test_common.h"

#include "ddns.h"
#include "ddns_priv.h"
#include "ddns_error_codes.h"
#include "amxp_mock.h"
#include "uci_mock.h"
#include "netmodel_mock.h"
#include "client/client.h"
#include "client/dm_client.h"
#include "server/dm_server.h"

static const char* odl_validator = "../../odl/tr181-dynamicdns_validators.odl";
static const char* odl_defs = "../../odl/tr181-dynamicdns_definition.odl";
static const char* mock_odl_defs = "../mock_odl/mock.odl";
static const char* mock_uci_odl = "../mock_odl/mock_uci.odl";
static const char* mock_netmodel_odl = "../mock_odl/mock_netmodel.odl";
static const char* mock_ip_odl = "../mock_odl/mock_ip.odl";

void dump_object(amxd_object_t* object, int index) {
    amxd_param_t* param = NULL;
    amxd_object_t* tmp_obj = NULL;
    amxc_var_t* tmp_var = NULL;
    cstring_t obj_name = NULL;
    cstring_t param_name = NULL;
    cstring_t param_value = NULL;
    index++;

    if(object == NULL) {
        printf("The object is NULL.\n");
    } else {
        amxc_llist_for_each(it, (&object->parameters)) {
            param = amxc_llist_it_get_data(it, amxd_param_t, it);
            param_name = (cstring_t) amxd_param_get_name(param);
            tmp_var = (amxc_var_t*) amxd_object_get_param_value(object, param_name);
            param_value = amxc_var_dyncast(cstring_t, tmp_var);
            printf("param_name[%d]: %s = %s\n", index, param_name, param_value);
            free(param_value);
            param = NULL;
        }
        amxc_llist_for_each(it, (&object->objects)) {
            tmp_obj = amxc_llist_it_get_data(it, amxd_object_t, it);
            obj_name = (cstring_t) amxd_object_get_name(tmp_obj, AMXD_OBJECT_INDEXED);
            printf("obj_name[%d]: %s\n", index, obj_name);
            dump_object(tmp_obj, index);
            tmp_obj = NULL;
        }
        amxc_llist_for_each(it, (&object->instances)) {
            tmp_obj = amxc_llist_it_get_data(it, amxd_object_t, it);
            obj_name = (cstring_t) amxd_object_get_name(tmp_obj, AMXD_OBJECT_INDEXED);
            printf("instance_name[%d]: %s\n", index, obj_name);
            dump_object(tmp_obj, index);
            tmp_obj = NULL;
        }
    }
    fflush(stdout);
}

/**
   @brief
   Extract a substring with a regular expression match.

   @param[in] str Source string containing the information to be searched.
   @param[in] regex Regular expression.
   @param[out] extracted_str Substring matched.

   @return
   0 if there is a match with the regex. Not 0 if there isn't a match.
 */
int extract_regex_from_string(amxc_string_t* str,
                              const cstring_t regex,
                              amxc_string_t** extracted_str) {
    regex_t finder;
    size_t match_size;
    cstring_t substr;
    int ret = -1;
    int cflags = REG_EXTENDED;
    regmatch_t pmatch[2] = {{-1, -1}, {-1, -1}};

    ret = regcomp(&finder, regex, cflags);
    if(ret != 0) {
        goto exit;
    }

    ret = regexec(&finder, str->buffer, 1, pmatch, 0);
    if(ret != 0) {
        goto exit;
    }

    match_size = pmatch[0].rm_eo - pmatch[0].rm_so;
    substr = (cstring_t) calloc(1, match_size + 1);
    for(int index = 0; index < (int) match_size; index++) {
        substr[index] = str->buffer[index + pmatch[0].rm_so];
    }
    substr[match_size] = '\0';

    amxc_string_set(*extracted_str, substr);
    free(substr);
exit:
    regfree(&finder);
    return ret;
}

/**
   @brief
   Fetch the instance number from the URI.

   @param[in] uri URI.

   @return
   0 when it fails.
 */
uint32_t parse_instance_index_from_uri(cstring_t uri) {
    uint32_t instance_index = 0;
    amxc_string_t* string_uri = NULL;
    amxc_string_t* extracted_string = NULL;
    amxc_string_t* partial_string = NULL;
    int rv;

    amxc_string_new(&string_uri, 1);
    amxc_string_new(&partial_string, 1);
    amxc_string_new(&extracted_string, 1);
    amxc_string_set(string_uri, uri);

    rv = extract_regex_from_string(string_uri, "\\.[0-9]+$", &partial_string);
    when_false((rv == 0), exit);
    rv = extract_regex_from_string(partial_string, "[0-9]+", &extracted_string);
    when_false((rv == 0), exit);

    instance_index = atoi(extracted_string->buffer);

exit:
    amxc_string_delete(&string_uri);
    amxc_string_delete(&extracted_string);
    amxc_string_delete(&partial_string);
    return instance_index;
}

/**
   @brief
   Read the information from the file and save to a string.

   @param[in] fname file name.
   @param[out] string string containing the information of the file appended.
 */
void read_from_file(const char* fname, amxc_string_t** string) {
    FILE* p_file;
    UNUSED size_t bytes_read;
    cstring_t buffer = 0;
    long length;

    p_file = fopen(fname, "rb");

    if(p_file != NULL) {
        fseek(p_file, 0, SEEK_END);
        length = ftell(p_file);
        fseek(p_file, 0, SEEK_SET);
        buffer = malloc(length);
        bytes_read = fread(buffer, 1, length, p_file);
        fclose(p_file);
    }

    if(buffer != NULL) {
        amxc_string_delete(string);
        amxc_string_new(string, 1);
        amxc_string_append(*string, buffer, length);
        free(buffer);
    }
}

/**
   @brief
   Write string to a file.

   @param[in] fname file name.
   @param[in] string string containing the information to be written.
 */
void write_to_file(const char* fname, amxc_string_t* string) {
    FILE* p_file;

    if(access(fname, F_OK) == 0) {
        remove(fname);
    }

    p_file = fopen(fname, "w");
    fputs(string->buffer, p_file);
    fclose(p_file);
}

void delete_file(const cstring_t filename) {
    if(access(filename, F_OK) == 0) {
        remove(filename);
    }
}

static void test_init_dummy_fn_resolvers(amxo_parser_t* p_parser) {
    // NetModel
    amxo_resolver_ftab_add(p_parser, "getResult", AMXO_FUNC(getResult));
    amxo_resolver_ftab_add(p_parser, "openQuery", AMXO_FUNC(openQuery));
    amxo_resolver_ftab_add(p_parser, "closeQuery", AMXO_FUNC(closeQuery));
    amxo_resolver_ftab_add(p_parser, "resolvePath", AMXO_FUNC(resolvePath));

    // uci
    amxo_resolver_ftab_add(p_parser, "get", AMXO_FUNC(mock_uci_get));
    amxo_resolver_ftab_add(p_parser, "add", AMXO_FUNC(mock_uci_add));
    amxo_resolver_ftab_add(p_parser, "set", AMXO_FUNC(mock_uci_set));
    amxo_resolver_ftab_add(p_parser, "delete", AMXO_FUNC(mock_uci_delete));
    amxo_resolver_ftab_add(p_parser, "rename", AMXO_FUNC(mock_uci_rename));
    amxo_resolver_ftab_add(p_parser, "commit", AMXO_FUNC(mock_uci_commit));
    amxo_resolver_ftab_add(p_parser, "changes", AMXO_FUNC(mock_uci_changes));

    // actions
    amxo_resolver_ftab_add(p_parser, "client_instance_cleanup", AMXO_FUNC(_client_instance_cleanup));
    amxo_resolver_ftab_add(p_parser, "client_hostname_instance_cleanup", AMXO_FUNC(_client_hostname_instance_cleanup));

    // events
    amxo_resolver_ftab_add(p_parser, "client_added", AMXO_FUNC(_client_added));
    amxo_resolver_ftab_add(p_parser, "client_changed", AMXO_FUNC(_client_changed));
    amxo_resolver_ftab_add(p_parser, "client_hostname_added", AMXO_FUNC(_client_hostname_added));
    amxo_resolver_ftab_add(p_parser, "client_hostname_changed", AMXO_FUNC(_client_hostname_changed));
    amxo_resolver_ftab_add(p_parser, "propagate_server_change", AMXO_FUNC(_propagate_server_change));
}

void mock_init(void** state) {
    amxd_dm_t* dm = NULL;
    amxo_parser_t* parser = NULL;
    amxd_object_t* root = NULL;
    const amxc_var_t* tmp_var = NULL;
    amxd_object_t* ddns = NULL;
    amxd_object_t* server_tmpl = NULL;
    amxd_object_t* server_obj = NULL;

    amxut_bus_setup(state);
    parser = amxut_bus_parser();
    dm = amxut_bus_dm();
    root = amxd_dm_get_root(dm);

    test_init_dummy_fn_resolvers(parser);

    assert_int_equal(amxo_parser_parse_file(parser, mock_uci_odl, root), 0);
    assert_int_equal(amxo_parser_parse_file(parser, mock_netmodel_odl, root), 0);
    assert_int_equal(amxo_parser_parse_file(parser, mock_ip_odl, root), 0);
    assert_int_equal(amxo_parser_parse_file(parser, mock_odl_defs, root), 0);
    assert_int_equal(amxo_resolver_import_open(parser, "/usr/lib/amx/modules/mod-dmext.so", "mod-dmext.so", 0), 0);
    assert_int_equal(amxo_parser_parse_file(parser, odl_validator, root), 0);
    assert_int_equal(amxo_parser_parse_file(parser, odl_defs, root), 0);

    // empty signal handler for 'amxp_slot_connect'
    amxp_sigmngr_add_signal(NULL, "connection-deleted");

    assert_int_equal(_dynamic_dns_main(AMXO_START, dm, parser), 0);
    amxut_bus_handle_events();

    ddns = amxd_dm_findf(dm, DDNS);
    assert_non_null(ddns);
    tmp_var = amxd_object_get_param_value(ddns, "ServerNumberOfEntries");
    assert_non_null(tmp_var);
    assert_int_equal(GET_UINT32(tmp_var, NULL), 4);
    server_tmpl = amxd_object_findf(ddns, ".Server.");
    assert_non_null(server_tmpl);
    server_obj = amxd_object_findf(server_tmpl, ".1.");
    assert_non_null(server_obj);
    server_obj = amxd_object_findf(server_tmpl, ".2.");
    assert_non_null(server_obj);
    server_obj = amxd_object_findf(server_tmpl, ".3.");
    assert_non_null(server_obj);
    server_obj = amxd_object_findf(server_tmpl, ".4.");
    assert_non_null(server_obj);
    amxut_bus_handle_events();
}

void mock_cleanup(void** state) {
    amxp_sigmngr_emit_signal(&amxut_bus_dm()->sigmngr, "app:stop", NULL);
    amxut_bus_handle_events();
    assert_int_equal(_dynamic_dns_main(AMXO_STOP, amxut_bus_dm(), amxut_bus_parser()), 0);
    amxut_bus_handle_events();

    uci_cleanup();
    amxp_mock_cleanup();
    amxut_bus_teardown(state);
    amxm_close_all();
    amxo_resolver_import_close_all();
}
