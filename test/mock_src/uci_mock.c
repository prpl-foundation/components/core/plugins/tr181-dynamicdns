/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <string.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>
#include <amxd/amxd_path.h>

#include <amxo/amxo.h>
#include <amxb/amxb_be_intf.h>
#include <amxb/amxb_register.h>
#include <amxb/amxb.h>

#include <amxm/amxm.h>

#include "uci_mock.h"
#include "ddns.h"

static amxc_var_t* uci = NULL;

amxd_status_t mock_uci_get(UNUSED amxd_object_t* object,
                           UNUSED amxd_function_t* func,
                           amxc_var_t* args,
                           amxc_var_t* ret) {
    amxc_var_t* values = NULL;
    amxc_var_t* section_var = NULL;
    amxc_var_t* ret_values = NULL;
    const char* config = GET_CHAR(args, "config");
    const char* type = GET_CHAR(args, "type");
    const char* section = GET_CHAR(args, "section");
    amxd_status_t status = amxd_status_unknown_error;

    assert_string_equal(config, "ddns");
    assert_string_equal(type, "service");

    amxc_var_set_type(ret, AMXC_VAR_ID_HTABLE);

    when_null(uci, exit);
    values = GET_ARG(uci, "values");
    when_null(values, exit);

    if(!str_empty(section)) {
        section_var = GET_ARG(values, section);
        if(section_var != NULL) {
            ret_values = amxc_var_add_key(amxc_htable_t, ret, "values", NULL);
            amxc_var_copy(ret_values, section_var);
        }
    } else {
        ret_values = amxc_var_add_key(amxc_htable_t, ret, "values", NULL);
        amxc_var_copy(ret_values, values);
    }

    status = amxd_status_ok;
exit:
    return status;
}

amxd_status_t mock_uci_add(UNUSED amxd_object_t* object,
                           UNUSED amxd_function_t* func,
                           amxc_var_t* args,
                           UNUSED amxc_var_t* ret) {
    amxc_var_t* values = NULL;
    amxc_var_t* section_var = NULL;
    const char* config = GET_CHAR(args, "config");
    const char* type = GET_CHAR(args, "type");
    const char* name = GET_CHAR(args, "name");
    amxc_var_t* arg_values = GET_ARG(args, "values");
    amxd_status_t status = amxd_status_unknown_error;

    assert_string_equal(config, "ddns");
    assert_string_equal(type, "service");
    assert_string_not_equal(name, "");
    assert_non_null(arg_values);

    if(uci == NULL) {
        amxc_var_new(&uci);
        amxc_var_set_type(uci, AMXC_VAR_ID_HTABLE);
    }

    values = GET_ARG(uci, "values");
    if(values == NULL) {
        values = amxc_var_add_key(amxc_htable_t, uci, "values", NULL);
    }

    section_var = GET_ARG(values, name);
    when_not_null(section_var, exit);

    section_var = amxc_var_add_key(amxc_htable_t, values, name, NULL);
    amxc_var_copy(section_var, arg_values);

    status = amxd_status_ok;
exit:
    return status;
}

amxd_status_t mock_uci_set(UNUSED amxd_object_t* object,
                           UNUSED amxd_function_t* func,
                           amxc_var_t* args,
                           UNUSED amxc_var_t* ret) {
    amxc_var_t* values = NULL;
    amxc_var_t* section_var = NULL;
    const char* config = GET_CHAR(args, "config");
    const char* type = GET_CHAR(args, "type");
    const char* section = GET_CHAR(args, "section");
    amxc_var_t* arg_values = GET_ARG(args, "values");
    const amxc_htable_t* ht_values = amxc_var_constcast(amxc_htable_t, arg_values);
    amxd_status_t status = amxd_status_unknown_error;

    if(strcmp(section, "global") == 0) {
        status = amxd_status_ok;
        goto exit;
    }

    assert_string_equal(config, "ddns");
    assert_string_equal(type, "service");
    assert_string_not_equal(section, "");
    assert_non_null(arg_values);
    assert_non_null(ht_values);

    when_null(uci, exit);
    values = GET_ARG(uci, "values");
    when_null(values, exit);
    section_var = GET_ARG(values, section);
    when_null(section_var, exit);

    amxc_htable_for_each(it, ht_values) {
        const char* key = amxc_htable_it_get_key(it);
        const amxc_var_t* value = amxc_htable_it_get_data(it, amxc_var_t, hit);
        amxc_var_t* tmp = GET_ARG(section_var, key);
        if(tmp != NULL) {
            amxc_var_delete(&tmp);
        }
        amxc_var_add_key(cstring_t, section_var, key, GET_CHAR(value, NULL));
    }

    status = amxd_status_ok;
exit:
    return status;
}

amxd_status_t mock_uci_delete(UNUSED amxd_object_t* object,
                              UNUSED amxd_function_t* func,
                              amxc_var_t* args,
                              UNUSED amxc_var_t* ret) {
    amxc_var_t* values = NULL;
    amxc_var_t* section_var = NULL;
    amxc_var_t* option_var = NULL;
    const char* config = GET_CHAR(args, "config");
    const char* type = GET_CHAR(args, "type");
    const char* section = GET_CHAR(args, "section");
    const char* option = GET_CHAR(args, "option");
    amxd_status_t status = amxd_status_unknown_error;

    assert_string_equal(config, "ddns");
    assert_string_equal(type, "service");
    assert_string_not_equal(section, "");

    when_null(uci, exit);
    values = GET_ARG(uci, "values");
    when_null(values, exit);
    section_var = GET_ARG(values, section);
    when_null(section_var, exit);

    if(!str_empty(option)) {
        option_var = GET_ARG(section_var, option);
        when_null(option_var, exit);
        amxc_var_delete(&option_var);
    } else {
        amxc_var_delete(&section_var);
    }

    status = amxd_status_ok;

exit:
    return status;
}

amxd_status_t mock_uci_rename(UNUSED amxd_object_t* object,
                              UNUSED amxd_function_t* func,
                              amxc_var_t* args,
                              UNUSED amxc_var_t* ret) {
    amxc_var_t* values = NULL;
    amxc_var_t* section_var = NULL;
    amxc_var_t* new_section_var = NULL;
    const char* config = GET_CHAR(args, "config");
    const char* section = GET_CHAR(args, "section");
    const char* name = GET_CHAR(args, "name");
    const amxc_htable_t* ht_values = NULL;
    amxd_status_t status = amxd_status_unknown_error;

    assert_string_equal(config, "ddns");
    assert_string_not_equal(section, "");
    assert_string_not_equal(name, "");

    when_null(uci, exit);
    values = GET_ARG(uci, "values");
    when_null(values, exit);
    section_var = GET_ARG(values, section);
    when_null(section_var, exit);

    ht_values = amxc_var_constcast(amxc_htable_t, section_var);
    when_null(ht_values, exit);
    new_section_var = amxc_var_add_key(amxc_htable_t, values, name, NULL);
    amxc_htable_for_each(it, ht_values) {
        const char* key = amxc_htable_it_get_key(it);
        const amxc_var_t* value = amxc_htable_it_get_data(it, amxc_var_t, hit);
        amxc_var_add_key(cstring_t, new_section_var, key, GET_CHAR(value, NULL));
    }
    amxc_var_delete(&section_var);

    status = amxd_status_ok;

exit:
    return status;
}

amxd_status_t mock_uci_commit(UNUSED amxd_object_t* object,
                              UNUSED amxd_function_t* func,
                              UNUSED amxc_var_t* args,
                              UNUSED amxc_var_t* ret) {
    return amxd_status_ok;
}

amxd_status_t mock_uci_changes(UNUSED amxd_object_t* object,
                               UNUSED amxd_function_t* func,
                               UNUSED amxc_var_t* args,
                               UNUSED amxc_var_t* ret) {
    return amxd_status_ok;
}

void uci_cleanup(void) {
    amxc_var_delete(&uci);
}

int uci_sections(void) {
    int size = 0;
    const amxc_htable_t* ht_values = NULL;

    amxc_var_t* values = GET_ARG(uci, "values");
    when_null(values, exit);
    ht_values = amxc_var_constcast(amxc_htable_t, values);
    when_null(ht_values, exit);

    size = (int) amxc_htable_size(ht_values);
exit:
    return size;
}

amxc_var_t* get_section(const char* section) {
    amxc_var_t uci_args;
    amxc_var_t* uci_ret = NULL;
    amxc_var_t* tmp = NULL;
    amxc_var_t* ret = NULL;
    amxc_var_t* values = NULL;

    amxc_var_init(&uci_args);
    amxc_var_set_type(&uci_args, AMXC_VAR_ID_HTABLE);
    amxc_var_new(&uci_ret);
    amxc_var_new(&ret);

    amxc_var_add_key(cstring_t, &uci_args, "config", "ddns");
    amxc_var_add_key(cstring_t, &uci_args, "type", "service");
    amxc_var_add_key(cstring_t, &uci_args, "section", section);

    mock_uci_get(NULL, NULL, &uci_args, uci_ret);

    if(amxc_var_type_of(uci_ret) == AMXC_VAR_ID_LIST) {
        tmp = GETP_ARG(uci_ret, "0");
        amxc_var_move(ret, tmp);
    } else {
        amxc_var_move(ret, uci_ret);
    }

    amxc_var_clean(&uci_args);
    amxc_var_delete(&uci_ret);

    values = GET_ARG(ret, "values");
    if(values == NULL) {
        amxc_var_delete(&ret);
        ret = NULL;
    }

    return ret;
}

void uci_dump(void) {
    amxc_var_dump(uci, STDOUT_FILENO);
    fflush(stdout);
}
