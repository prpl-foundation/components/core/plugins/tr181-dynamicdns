/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <string.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>
#include <amxd/amxd_path.h>

#include <amxo/amxo.h>
#include <amxb/amxb_be_intf.h>
#include <amxb/amxb_register.h>
#include <amxb/amxb.h>

#include <amxm/amxm.h>

#include "netmodel_mock.h"
#include "ddns.h"

typedef struct {
    amxc_htable_it_t it;
    char* subscriber;
    char* method;
    char* path;
    char* flag;
    bool being_queried;
} dummy_query_t;

typedef struct {
    amxc_htable_it_t it;
    amxc_htable_t* query_table;
} dummy_queries_t;

static amxc_htable_t* query_table_per_intf = NULL;

void dump_query_table(void) {
    amxc_htable_for_each(it_query_table_per_intf, query_table_per_intf) {
        const char* query_key = NULL;
        dummy_queries_t* queries = NULL;

        query_key = amxc_htable_it_get_key(it_query_table_per_intf);
        queries = amxc_htable_it_get_data(it_query_table_per_intf, dummy_queries_t, it);
        printf("%s = {\n", query_key); fflush(stdout);
        if(queries == NULL) {
            printf("},\n"); fflush(stdout);
            continue;
        }
        if(queries->query_table == NULL) {
            printf("},\n"); fflush(stdout);
            continue;
        }

        amxc_htable_for_each(it, queries->query_table) {
            dummy_query_t* query = NULL;
            const char* key = NULL;

            key = amxc_htable_it_get_key(it);
            query = amxc_htable_it_get_data(it, dummy_query_t, it);
            printf("\t%s = {\n", key); fflush(stdout);
            if(query == NULL) {
                printf("\t},\n"); fflush(stdout);
                continue;
            }
            printf("\t\tsubscriber = %s\n", query->subscriber); fflush(stdout);
            printf("\t\tmethod = %s\n", query->method); fflush(stdout);
            printf("\t\tpath = %s\n", query->path); fflush(stdout);
            printf("\t\tflag = %s\n", query->flag); fflush(stdout);
            printf("\t\tbeing_queried = %d\n", query->being_queried); fflush(stdout);
            printf("\t},\n"); fflush(stdout);
        }
        printf("},\n"); fflush(stdout);
    }
}

static dummy_query_t* get_active_query(const char* intf) {
    dummy_queries_t* queries = NULL;
    dummy_query_t* query = NULL;

    queries = amxc_htable_it_get_data(amxc_htable_get(query_table_per_intf, intf), dummy_queries_t, it);
    assert_non_null(queries);

    amxc_htable_for_each(it, queries->query_table) {
        query = amxc_htable_it_get_data(it, dummy_query_t, it);
        assert_non_null(query);

        if(query->being_queried) {
            return query;
        }
    }
    return NULL;
}

amxd_status_t getResult(amxd_object_t* object,
                        UNUSED amxd_function_t* func,
                        UNUSED amxc_var_t* args,
                        amxc_var_t* ret) {
    amxd_path_t dm_path;
    dummy_query_t* q = NULL;
    char* path = NULL;
    char* end_of_path = NULL;
    const char* intf = NULL;

    path = amxd_object_get_path(object, AMXD_OBJECT_NAMED);
    end_of_path = strstr(path, ".Query");
    if(end_of_path != NULL) {
        *end_of_path = '\0';
    }

    amxd_path_init(&dm_path, path);
    intf = amxd_path_get_param(&dm_path);
    assert_non_null(intf);

    q = get_active_query(intf);
    assert_non_null(q);

    if(strcmp(q->method, "getFirstParameter") == 0) {
        const char* result = "lo";
        if(strcmp(intf, "ip-lan") == 0) {
            result = "br-lan";
        } else if(strcmp(intf, "ip-wan") == 0) {
            result = "eth0";
        } else if(strcmp(intf, "ip-guest") == 0) {
            result = "br-guest";
        } else if(strcmp(intf, "ip-lcm") == 0) {
            result = "br-lcm";
        }
        amxc_var_set(cstring_t, ret, result);
    } else if(strcmp(q->method, "isUp") == 0) {
        amxc_var_set(int32_t, ret, 1);
    } else if(strcmp(q->method, "getAddrs") == 0) {
        amxc_var_t* var = NULL;
        bool for_resolver = strcmp(intf, "resolver") == 0;
        bool is_ipv4_query = true;
        bool is_ipv6_query = true;
        amxc_var_set_type(ret, AMXC_VAR_ID_LIST);
        if(strcmp(q->flag, "ipv4") == 0) {
            is_ipv6_query = false;
        } else if(strcmp(q->flag, "ipv6") == 0) {
            is_ipv4_query = false;
        }
        if((strcmp(intf, "ip-wan") == 0) || for_resolver) {
            if(is_ipv4_query) {
                var = amxc_var_add_new(ret);
                amxc_var_set_type(var, AMXC_VAR_ID_HTABLE);
                amxc_var_add_key(cstring_t, var, "Address", "192.168.99.221");
                amxc_var_add_key(cstring_t, var, "Family", "ipv4");
                amxc_var_add_key(cstring_t, var, "Flags", "");
                amxc_var_add_key(cstring_t, var, "NetDevName", "eth0");
                amxc_var_add_key(cstring_t, var, "NetModelIntf", "ip-wan");
                amxc_var_add_key(cstring_t, var, "Peer", "");
                amxc_var_add_key(cstring_t, var, "PrefixLen", "24");
                amxc_var_add_key(cstring_t, var, "Scope", "");
            }

            if(is_ipv6_query) {
                var = amxc_var_add_new(ret);
                amxc_var_set_type(var, AMXC_VAR_ID_HTABLE);
                amxc_var_add_key(cstring_t, var, "Address", "fc::1:10:18ff:fe01:2102");
                amxc_var_add_key(cstring_t, var, "Family", "ipv6");
                amxc_var_add_key(cstring_t, var, "Flags", "");
                amxc_var_add_key(cstring_t, var, "NetDevName", "eth0");
                amxc_var_add_key(cstring_t, var, "NetModelIntf", "ip-wan");
                amxc_var_add_key(cstring_t, var, "Peer", "");
                amxc_var_add_key(cstring_t, var, "PrefixLen", "64");
                amxc_var_add_key(cstring_t, var, "Scope", "");
            }
        }
        if((strcmp(intf, "ip-lan") == 0) || for_resolver) {
            if(is_ipv4_query) {
                var = amxc_var_add_new(ret);
                amxc_var_set_type(var, AMXC_VAR_ID_HTABLE);
                amxc_var_add_key(cstring_t, var, "Address", "192.168.1.1");
                amxc_var_add_key(cstring_t, var, "Family", "ipv4");
                amxc_var_add_key(cstring_t, var, "Flags", "");
                amxc_var_add_key(cstring_t, var, "NetDevName", "br-lan");
                amxc_var_add_key(cstring_t, var, "NetModelIntf", "ip-lan");
                amxc_var_add_key(cstring_t, var, "Peer", "");
                amxc_var_add_key(cstring_t, var, "PrefixLen", "24");
                amxc_var_add_key(cstring_t, var, "Scope", "");

                var = amxc_var_add_new(ret);
                amxc_var_set_type(var, AMXC_VAR_ID_HTABLE);
                amxc_var_add_key(cstring_t, var, "Address", "192.168.3.1");
                amxc_var_add_key(cstring_t, var, "Family", "ipv4");
                amxc_var_add_key(cstring_t, var, "Flags", "");
                amxc_var_add_key(cstring_t, var, "NetDevName", "br-lan");
                amxc_var_add_key(cstring_t, var, "NetModelIntf", "ip-lan");
                amxc_var_add_key(cstring_t, var, "Peer", "");
                amxc_var_add_key(cstring_t, var, "PrefixLen", "24");
                amxc_var_add_key(cstring_t, var, "Scope", "");
            }

            if(is_ipv6_query) {
                var = amxc_var_add_new(ret);
                amxc_var_set_type(var, AMXC_VAR_ID_HTABLE);
                amxc_var_add_key(cstring_t, var, "Address", "fc::1:10:18ff:fe01:2101");
                amxc_var_add_key(cstring_t, var, "Family", "ipv6");
                amxc_var_add_key(cstring_t, var, "Flags", "");
                amxc_var_add_key(cstring_t, var, "NetDevName", "br-lan");
                amxc_var_add_key(cstring_t, var, "NetModelIntf", "ip-lan");
                amxc_var_add_key(cstring_t, var, "Peer", "");
                amxc_var_add_key(cstring_t, var, "PrefixLen", "64");
                amxc_var_add_key(cstring_t, var, "Scope", "");
            }
        }
        if((strcmp(intf, "ip-guest") == 0) || for_resolver) {
            if(is_ipv4_query) {
                var = amxc_var_add_new(ret);
                amxc_var_set_type(var, AMXC_VAR_ID_HTABLE);
                amxc_var_add_key(cstring_t, var, "Address", "192.168.2.1");
                amxc_var_add_key(cstring_t, var, "Family", "ipv4");
                amxc_var_add_key(cstring_t, var, "Flags", "");
                amxc_var_add_key(cstring_t, var, "NetDevName", "br-guest");
                amxc_var_add_key(cstring_t, var, "NetModelIntf", "ip-guest");
                amxc_var_add_key(cstring_t, var, "Peer", "");
                amxc_var_add_key(cstring_t, var, "PrefixLen", "24");
                amxc_var_add_key(cstring_t, var, "Scope", "");
            }
        }
        if((strcmp(intf, "ip-lcm") == 0) || for_resolver) {
            if(is_ipv4_query) {
                var = amxc_var_add_new(ret);
                amxc_var_set_type(var, AMXC_VAR_ID_HTABLE);
                amxc_var_add_key(cstring_t, var, "Address", "192.168.20.1");
                amxc_var_add_key(cstring_t, var, "Family", "ipv4");
                amxc_var_add_key(cstring_t, var, "Flags", "");
                amxc_var_add_key(cstring_t, var, "NetDevName", "br-lcm");
                amxc_var_add_key(cstring_t, var, "NetModelIntf", "ip-lcm");
                amxc_var_add_key(cstring_t, var, "Peer", "");
                amxc_var_add_key(cstring_t, var, "PrefixLen", "24");
                amxc_var_add_key(cstring_t, var, "Scope", "");
            }
        }
        if((strcmp(intf, "lo") == 0) || for_resolver) {
            if(is_ipv4_query) {
                var = amxc_var_add_new(ret);
                amxc_var_set_type(var, AMXC_VAR_ID_HTABLE);
                amxc_var_add_key(cstring_t, var, "Address", "127.0.0.1");
                amxc_var_add_key(cstring_t, var, "Family", "ipv4");
                amxc_var_add_key(cstring_t, var, "Flags", "");
                amxc_var_add_key(cstring_t, var, "NetDevName", "lo");
                amxc_var_add_key(cstring_t, var, "NetModelIntf", "ip-loopback");
                amxc_var_add_key(cstring_t, var, "Peer", "");
                amxc_var_add_key(cstring_t, var, "PrefixLen", "8");
                amxc_var_add_key(cstring_t, var, "Scope", "");
            }

            if(is_ipv6_query) {
                var = amxc_var_add_new(ret);
                amxc_var_set_type(var, AMXC_VAR_ID_HTABLE);
                amxc_var_add_key(cstring_t, var, "Address", "::1");
                amxc_var_add_key(cstring_t, var, "Family", "ipv6");
                amxc_var_add_key(cstring_t, var, "Flags", "");
                amxc_var_add_key(cstring_t, var, "NetDevName", "lo");
                amxc_var_add_key(cstring_t, var, "NetModelIntf", "ip-loopback");
                amxc_var_add_key(cstring_t, var, "Peer", "");
                amxc_var_add_key(cstring_t, var, "PrefixLen", "128");
                amxc_var_add_key(cstring_t, var, "Scope", "");
            }
        }
    }

    free(path);
    amxd_path_clean(&dm_path);
    q->being_queried = false;

    return amxd_status_ok;
}

amxd_status_t openQuery(UNUSED amxd_object_t* object,
                        UNUSED amxd_function_t* func,
                        amxc_var_t* args,
                        amxc_var_t* ret) {
    amxd_path_t dm_path;
    char* path = NULL;
    const char* intf = NULL;
    dummy_query_t* q = NULL;
    dummy_queries_t* queries = NULL;
    amxc_htable_it_t* it_queries = NULL;

    if(query_table_per_intf == NULL) {
        amxc_htable_new(&query_table_per_intf, 5);
    }

    path = amxd_object_get_path(object, AMXD_OBJECT_NAMED);
    amxd_path_init(&dm_path, path);
    intf = amxd_path_get_param(&dm_path);
    assert_non_null(intf);

    it_queries = amxc_htable_get(query_table_per_intf, intf);
    if(it_queries == NULL) {
        queries = calloc(1, sizeof(dummy_queries_t));
        queries->query_table = NULL;
        amxc_htable_insert(query_table_per_intf, intf, &queries->it);
    } else {
        queries = amxc_htable_it_get_data(it_queries, dummy_queries_t, it);
    }
    assert_non_null(queries);

    if(queries->query_table == NULL) {
        amxc_htable_new(&queries->query_table, 1);
    }

    q = amxc_htable_it_get_data(amxc_htable_get(queries->query_table, GETP_CHAR(args, "class")), dummy_query_t, it);
    if(q == NULL) {
        q = calloc(1, sizeof(*q));
        q->subscriber = strdup(GETP_CHAR(args, "subscriber"));
        q->method = strdup(GETP_CHAR(args, "class"));
        if(!str_empty(GETP_CHAR(args, "flag"))) {
            q->flag = strdup(GETP_CHAR(args, "flag"));
        } else {
            q->flag = strdup("");
        }
        q->being_queried = true;
        amxc_htable_insert(queries->query_table, q->method, &q->it);
    } else {
        q->being_queried = true;
    }

    amxc_var_set(uint32_t, ret, 1);
    free(path);
    amxd_path_clean(&dm_path);

    return amxd_status_ok;
}

amxd_status_t closeQuery(amxd_object_t* object,
                         UNUSED amxd_function_t* func,
                         UNUSED amxc_var_t* args,
                         UNUSED amxc_var_t* ret) {
    amxd_path_t dm_path;
    char* path = NULL;
    const char* intf = NULL;
    dummy_queries_t* queries = NULL;
    dummy_query_t* q = NULL;

    path = amxd_object_get_path(object, AMXD_OBJECT_NAMED);
    amxd_path_init(&dm_path, path);
    intf = amxd_path_get_param(&dm_path);
    assert_non_null(intf);

    queries = amxc_htable_it_get_data(amxc_htable_get(query_table_per_intf, intf), dummy_queries_t, it);
    // assert_non_null(queries);
    if(queries != NULL) {
        q = amxc_htable_it_get_data(amxc_htable_get(queries->query_table, GETP_CHAR(args, "class")), dummy_query_t, it);
        amxc_htable_it_clean(&q->it, NULL);
        free(q->subscriber);
        free(q->path);
        free(q->method);
        free(q->flag);
        free(q);

        if(amxc_htable_is_empty(queries->query_table)) {
            amxc_htable_it_clean(&queries->it, NULL);
            amxc_htable_delete(&queries->query_table, NULL);
            free(queries);
        }
    }

    if(amxc_htable_is_empty(query_table_per_intf)) {
        amxc_htable_delete(&query_table_per_intf, NULL);
    }

    free(path);
    amxd_path_clean(&dm_path);

    return amxd_status_ok;
}

amxd_status_t resolvePath(UNUSED amxd_object_t* object,
                          UNUSED amxd_function_t* func,
                          amxc_var_t* args,
                          amxc_var_t* ret) {
    const char* path = GETP_CHAR(args, "path");
    const char* interface = path;

    if(strstr(path, "IP.Interface.3") != NULL) {             //lan
        interface = "ip-lan";
    } else if(strstr(path, "IP.Interface.2") != NULL) {      //wan
        interface = "ip-wan";
    } else if(strstr(path, "IP.Interface.4") != NULL) {      //guest
        interface = "ip-guest";
    } else if(strstr(path, "IP.Interface.20") != NULL) {     //lcm
        interface = "ip-lcm";
    } else if(strstr(path, "IP.Interface.1") != NULL) {      //lo
        interface = "lo";
    } else if(strstr(path, "Logical.Interface.1") != NULL) { //wan
        interface = "ip-wan";
    }

    amxc_var_set_type(ret, AMXC_VAR_ID_LIST);
    amxc_var_add(cstring_t, ret, interface);

    return amxd_status_ok;
}

amxc_var_t* netmodel_getFirstParameter(UNUSED const char* intf,
                                       UNUSED const char* name,
                                       UNUSED const char* flag,
                                       UNUSED const char* traverse) {
    amxc_var_t* result = NULL;
    const char* interface = NULL;

    when_str_empty(intf, exit);

    if(strstr(intf, "IP.Interface.3") != NULL) {             //lan
        interface = "br-lan";
    } else if(strstr(intf, "IP.Interface.2") != NULL) {      //wan
        interface = "eth0";
    } else if(strstr(intf, "IP.Interface.4") != NULL) {      //guest
        interface = "br-guest";
    } else if(strstr(intf, "IP.Interface.20") != NULL) {     //lcm
        interface = "br-lcm";
    } else if(strstr(intf, "IP.Interface.1") != NULL) {      //lo
        interface = "lo";
    } else if(strstr(intf, "Logical.Interface.1") != NULL) { //wan
        interface = "eth0";
    }
    when_str_empty(interface, exit);

    amxc_var_new(&result);
    amxc_var_set(cstring_t, result, interface);

exit:
    return result;
}