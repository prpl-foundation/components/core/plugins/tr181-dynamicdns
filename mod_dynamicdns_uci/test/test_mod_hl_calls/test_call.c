/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <string.h>

#include <amxc/amxc.h>

#include <amxm/amxm.h>

#include "mock.h"
#include "amxp_mock.h"
#include "uci_mock.h"
#include "ddns.h"
#include "ddns_error_codes.h"
#include "mod_ddns_common.h"
#include "mod_ddns_uci_commit.h"
#include "mod_ddns_uci_utils.h"
#include "mod_ddns_uci_scripts.h"

#include "test_call.h"

#define TESTDATA_1_DOMAIN "mydomain.be"
#define TESTDATA_1_DOMAIN_ "mydomainbe"
#define TESTDATA_1_SERVER "afraid.org-basicauth"
#define TESTDATA_1_ADDRESS "http://[USERNAME]:[PASSWORD]@freedns.afraid.org/nic/update?hostname=[DOMAIN]&myip=[IP]"
#define TESTDATA_1_INTERFACE "Device.IP.Interface.2."
#define TESTDATA_1_IP        "10.0.0.1"
#define TESTDATA_1_USER   "user"
#define TESTDATA_1_PASSWD "password"
#define TESTDATA_1_INDEX  1
#define TESTDATA_1_HOST_INDEX 1

#define TESTDATA_2_DOMAIN "mydomain.nl"
#define TESTDATA_2_DOMAIN_ "mydomainnl"
#define TESTDATA_2_SERVER "afraid.org-v2-basic"
#define TESTDATA_2_ADDRESS "http://[USERNAME]:[PASSWORD]@dyndns.com/nic/update?hostname=[DOMAIN]&myip=[IP]"
#define TESTDATA_2_INTERFACE "Device.IP.Interface.3."
#define TESTDATA_2_IP        "192.168.0.1"
#define TESTDATA_2_USER   "user_2"
#define TESTDATA_2_PASSWD "password2"
#define TESTDATA_2_INDEX  2
#define TESTDATA_2_HOST_INDEX 2

#define TESTDATA_3_DOMAIN "mydomain.de"
#define TESTDATA_3_DOMAIN_ "mydomainde"
#define TESTDATA_3_SERVER "afraid.org-v2-token"
#define TESTDATA_3_ADDRESS "http://[USERNAME]:[PASSWORD]@easydns.com/nic/update?hostname=[DOMAIN]&myip=[IP]"
#define TESTDATA_3_INTERFACE "Device.IP.Interface.3."
#define TESTDATA_3_IP        "192.168.0.2"
#define TESTDATA_3_USER   "user_3"
#define TESTDATA_3_PASSWD "password3"
#define TESTDATA_3_INDEX  3
#define TESTDATA_3_HOST_INDEX 3

amxc_var_t* args = NULL;
amxc_var_t* ret = NULL;

static void init_soc_vars(void) {
    if(args != NULL) {
        amxc_var_delete(&args);
    }
    if(ret != NULL) {
        amxc_var_delete(&ret);
    }
    amxc_var_new(&args);
    amxc_var_set_type(args, AMXC_VAR_ID_HTABLE);
    amxc_var_new(&ret);
    amxc_var_set_type(ret, AMXC_VAR_ID_HTABLE);
}

static void clean_soc_vars(void) {
    if(args != NULL) {
        amxc_var_delete(&args);
        args = NULL;
    }
    if(ret != NULL) {
        amxc_var_delete(&ret);
        ret = NULL;
    }
}

static void fill_server_info(amxc_var_t* mod_args, const char* service, const char* address) {
    assert_non_null(mod_args);

    if(service != NULL) {
        amxc_var_add_key(cstring_t, mod_args, SERVER_SERVICE, service);
    }
    if(address != NULL) {
        amxc_var_add_key(cstring_t, mod_args, SERVER_ADDRESS, address);
    }
    amxc_var_add_key(cstring_t, mod_args, SERVER_PROTOCOL, "HTTPS");
    amxc_var_add_key(uint32_t, mod_args, SERVER_CHECK_INTERVAL, 600);
    amxc_var_add_key(uint32_t, mod_args, SERVER_RETRY_INTERVAL, 60);
    amxc_var_add_key(uint32_t, mod_args, SERVER_MAX_RETRIES, 0);
}

static ddns_status_t add_on_module(uint32_t client_index, uint32_t host_index, const char* domain, const char* service, const char* address,
                                   const char* interface, const char* ip, const char* username, const char* password, bool enable) {
    init_soc_vars();
    fill_server_info(args, service, address);

    if(client_index > 0) {
        amxc_var_add_key(uint32_t, args, "ClientInstance", client_index);
    }
    if(host_index > 0) {
        amxc_var_add_key(uint32_t, args, "HostnameInstance", host_index);
    }
    if(domain != NULL) {
        amxc_var_add_key(cstring_t, args, "Domain", domain);
    }
    if(interface != NULL) {
        if(ip != NULL) {
            amxc_var_add_key(cstring_t, args, "IP", ip);
            amxc_var_add_key(cstring_t, args, "ip_source", "script");
        } else {
            amxc_var_add_key(cstring_t, args, "Interface", interface);
            amxc_var_add_key(cstring_t, args, "ip_source", "interface");
        }
    } else {
        if(ip != NULL) {
            amxc_var_add_key(cstring_t, args, "IP", ip);
            amxc_var_add_key(cstring_t, args, "ip_source", "script");
        } else {
            amxc_var_add_key(cstring_t, args, "ip_source", "interface");
        }
    }

    if(username != NULL) {
        amxc_var_add_key(cstring_t, args, "Username", username);
    }
    if(password != NULL) {
        amxc_var_add_key(cstring_t, args, "Password", password);
    }
    amxc_var_add_key(bool, args, "Enable", enable);

    return amxm_execute_function("target_module", MOD_DDNS_CTRL, "add-ddns", args, ret);
}

static ddns_status_t edit_on_module(uint32_t client_index, uint32_t host_index, const char* new_domain, const char* service,
                                    const char* address, const char* interface, const char* ip, const char* username, const char* password, bool enable) {
    init_soc_vars();
    fill_server_info(args, service, address);

    if(client_index > 0) {
        amxc_var_add_key(uint32_t, args, "ClientInstance", client_index);
    }
    if(host_index > 0) {
        amxc_var_add_key(uint32_t, args, "HostnameInstance", host_index);
    }
    if(new_domain != NULL) {
        amxc_var_add_key(cstring_t, args, "Domain", new_domain);
    }
    if(ip != NULL) {
        amxc_var_add_key(cstring_t, args, "IP", ip);
        amxc_var_add_key(cstring_t, args, "ip_source", "script");
    } else {
        amxc_var_add_key(cstring_t, args, "ip_source", "interface");
        if(interface != NULL) {
            amxc_var_add_key(cstring_t, args, "Interface", interface);
        }
    }
    if(username != NULL) {
        amxc_var_add_key(cstring_t, args, "Username", username);
    }
    if(password != NULL) {
        amxc_var_add_key(cstring_t, args, "Password", password);
    }
    amxc_var_add_key(bool, args, "Enable", enable);

    return amxm_execute_function("target_module", MOD_DDNS_CTRL, "edit-ddns", args, ret);
}

static ddns_status_t delete_on_module(uint32_t client_index, uint32_t host_index) {
    init_soc_vars();

    if(client_index > 0) {
        amxc_var_add_key(uint32_t, args, "ClientInstance", client_index);
    }
    if(host_index > 0) {
        amxc_var_add_key(uint32_t, args, "HostnameInstance", host_index);
    }

    return amxm_execute_function("target_module", MOD_DDNS_CTRL, "delete-ddns", args, ret);
}

static ddns_error_t check_on_module(uint32_t client_index, uint32_t host_index) {
    init_soc_vars();

    if(client_index > 0) {
        amxc_var_add_key(uint32_t, args, "ClientInstance", client_index);
    }
    if(host_index > 0) {
        amxc_var_add_key(uint32_t, args, "HostnameInstance", host_index);
    }

    return amxm_execute_function("target_module", MOD_DDNS_CTRL, "check-ddns", args, ret);
}

int test_calls_setup(void** state) {
    init_soc_vars();
    mock_init(state);
    return 0;
}

int test_calls_teardown(void** state) {
    clean_soc_vars();
    mock_cleanup(state);
    return 0;
}

void test_module_can_load(UNUSED void** state) {
    amxm_shared_object_t* so = NULL;
    assert_int_equal(amxm_so_open(&so, "target_module", "../modules_under_test/target_module.so"), 0);
}

void test_add_no_client_instance(UNUSED void** state) {
    ddns_status_t rv = -1;

    rv = add_on_module(0, TESTDATA_1_HOST_INDEX, TESTDATA_1_DOMAIN, TESTDATA_1_SERVER, TESTDATA_1_ADDRESS, TESTDATA_1_INTERFACE, NULL, TESTDATA_1_USER, TESTDATA_1_PASSWD, false);
    assert_int_equal(rv, ddns_status_error_config);

    assert_int_equal(uci_sections(), 0);
    assert_int_equal(processes_running(), 0);
}

void test_add_invalid_server(UNUSED void** state) {
    ddns_status_t rv = -1;

    rv = add_on_module(TESTDATA_1_INDEX, TESTDATA_1_HOST_INDEX, TESTDATA_1_DOMAIN, NULL, TESTDATA_1_ADDRESS, TESTDATA_1_INTERFACE, NULL, TESTDATA_1_USER, TESTDATA_1_PASSWD, false);
    assert_int_equal(rv, ddns_status_error_config);

    rv = add_on_module(TESTDATA_1_INDEX, TESTDATA_1_HOST_INDEX, TESTDATA_1_DOMAIN, "", TESTDATA_1_ADDRESS, TESTDATA_1_INTERFACE, NULL, TESTDATA_1_USER, TESTDATA_1_PASSWD, false);
    assert_int_equal(rv, ddns_status_error_config);

    assert_int_equal(uci_sections(), 0);
    assert_int_equal(processes_running(), 0);
}

void test_add_no_user(UNUSED void** state) {
    ddns_status_t rv = -1;

    rv = add_on_module(TESTDATA_1_INDEX, TESTDATA_1_HOST_INDEX, TESTDATA_1_DOMAIN, TESTDATA_1_SERVER, TESTDATA_1_ADDRESS, TESTDATA_1_INTERFACE, NULL, NULL, TESTDATA_1_PASSWD, false);
    assert_int_equal(rv, ddns_status_error_config);

    rv = add_on_module(TESTDATA_1_INDEX, TESTDATA_1_HOST_INDEX, TESTDATA_1_DOMAIN, TESTDATA_1_SERVER, TESTDATA_1_ADDRESS, TESTDATA_1_INTERFACE, NULL, "", TESTDATA_1_PASSWD, false);
    assert_int_equal(rv, ddns_status_error_config);

    assert_int_equal(uci_sections(), 0);
    assert_int_equal(processes_running(), 0);
}

void test_add_no_password(UNUSED void** state) {
    ddns_status_t rv = -1;

    rv = add_on_module(TESTDATA_1_INDEX, TESTDATA_1_HOST_INDEX, TESTDATA_1_DOMAIN, TESTDATA_1_SERVER, TESTDATA_1_ADDRESS, TESTDATA_1_INTERFACE, NULL, TESTDATA_1_USER, NULL, false);
    assert_int_equal(rv, ddns_status_error_config);

    rv = add_on_module(TESTDATA_1_INDEX, TESTDATA_1_HOST_INDEX, TESTDATA_1_DOMAIN, TESTDATA_1_SERVER, TESTDATA_1_ADDRESS, TESTDATA_1_INTERFACE, NULL, TESTDATA_1_USER, "", false);
    assert_int_equal(rv, ddns_status_error_config);

    assert_int_equal(uci_sections(), 0);
    assert_int_equal(processes_running(), 0);
}

void test_module_operations(UNUSED void** state) {
    ddns_status_t rv = -1;
    amxc_var_t* ddns_section = NULL;
    amxc_var_t* values = NULL;
    amxc_string_t section_name;

    amxc_string_init(&section_name, 0);

    //Add without a hostname instance
    rv = add_on_module(TESTDATA_1_INDEX, 0, TESTDATA_1_DOMAIN, TESTDATA_1_SERVER, TESTDATA_1_ADDRESS, TESTDATA_1_INTERFACE, NULL, TESTDATA_1_USER, TESTDATA_1_PASSWD, true);
    assert_int_equal(rv, ddns_status_error_config);
    assert_int_equal(uci_sections(), 0);
    assert_int_equal(processes_running(), 0);

    //Add without a domain
    rv = add_on_module(TESTDATA_1_INDEX, TESTDATA_1_HOST_INDEX, NULL, TESTDATA_1_SERVER, TESTDATA_1_ADDRESS, TESTDATA_1_INTERFACE, NULL, TESTDATA_1_USER, TESTDATA_1_PASSWD, true);
    assert_int_equal(rv, ddns_status_error_config);
    assert_int_equal(uci_sections(), 0);
    assert_int_equal(processes_running(), 0);

    //Add again to change the domain
    rv = add_on_module(TESTDATA_1_INDEX, TESTDATA_1_HOST_INDEX, TESTDATA_1_DOMAIN, TESTDATA_1_SERVER, TESTDATA_1_ADDRESS, TESTDATA_1_INTERFACE, NULL, TESTDATA_1_USER, TESTDATA_1_PASSWD, true);
    assert_int_equal(rv, ddns_status_updated);
    amxc_string_setf(&section_name, "section_%d_%d", TESTDATA_1_INDEX, TESTDATA_1_HOST_INDEX);

    ddns_section = uci_get_ddns_section(amxc_string_get(&section_name, 0), NULL);
    assert_non_null(ddns_section);
    values = GET_ARG(ddns_section, "values");
    assert_non_null(values);
    assert_string_equal(GET_CHAR(values, "domain"), TESTDATA_1_DOMAIN);
    assert_string_equal(GET_CHAR(values, "lookup_host"), TESTDATA_1_DOMAIN);
    assert_string_equal(GET_CHAR(values, "username"), TESTDATA_1_USER);
    assert_string_equal(GET_CHAR(values, "password"), TESTDATA_1_PASSWD);
    assert_string_equal(GET_CHAR(values, "ip_interface"), TESTDATA_1_INTERFACE);
    assert_string_equal(GET_CHAR(values, "ip_source"), "interface");
    assert_string_equal(GET_CHAR(values, "service_name"), TESTDATA_1_SERVER);
    assert_null(GET_CHAR(values, "update_url"));
    assert_string_equal(GET_CHAR(values, "enabled"), "1");
    assert_int_equal(GET_UINT32(values, "check_interval"), 10);
    assert_int_equal(GET_UINT32(values, "force_interval"), 20000);
    assert_int_equal(GET_UINT32(values, "retry_interval"), 60);
    assert_int_equal(GET_UINT32(values, "retry_max_count"), 0);
    amxc_var_delete(&ddns_section);

    assert_int_equal(uci_sections(), 1);
    assert_int_equal(processes_running(), 1);

    //Add new
    rv = add_on_module(TESTDATA_3_INDEX, TESTDATA_3_HOST_INDEX, TESTDATA_3_DOMAIN, TESTDATA_3_SERVER, TESTDATA_3_ADDRESS, TESTDATA_3_INTERFACE, NULL, TESTDATA_3_USER, TESTDATA_3_PASSWD, false);
    assert_int_equal(rv, ddns_status_disabled);
    amxc_string_setf(&section_name, "section_%d_%d", TESTDATA_3_INDEX, TESTDATA_3_HOST_INDEX);

    ddns_section = uci_get_ddns_section(amxc_string_get(&section_name, 0), NULL);
    assert_non_null(ddns_section);
    values = GET_ARG(ddns_section, "values");
    assert_non_null(values);
    assert_string_equal(GET_CHAR(values, "domain"), TESTDATA_3_DOMAIN);
    assert_string_equal(GET_CHAR(values, "lookup_host"), TESTDATA_3_DOMAIN);
    assert_string_equal(GET_CHAR(values, "username"), TESTDATA_3_USER);
    assert_string_equal(GET_CHAR(values, "password"), TESTDATA_3_PASSWD);
    assert_string_equal(GET_CHAR(values, "ip_interface"), TESTDATA_3_INTERFACE);
    assert_string_equal(GET_CHAR(values, "ip_source"), "interface");
    assert_string_equal(GET_CHAR(values, "service_name"), TESTDATA_3_SERVER);
    assert_null(GET_CHAR(values, "update_url"));
    assert_string_equal(GET_CHAR(values, "enabled"), "0");
    assert_int_equal(GET_UINT32(values, "check_interval"), 10);
    assert_int_equal(GET_UINT32(values, "force_interval"), 20000);
    assert_int_equal(GET_UINT32(values, "retry_interval"), 60);
    assert_int_equal(GET_UINT32(values, "retry_max_count"), 0);
    amxc_var_delete(&ddns_section);

    assert_int_equal(uci_sections(), 2);
    assert_int_equal(processes_running(), 1);

    //Edit
    rv = edit_on_module(TESTDATA_1_INDEX, TESTDATA_1_HOST_INDEX, TESTDATA_2_DOMAIN, TESTDATA_2_SERVER, TESTDATA_2_ADDRESS, TESTDATA_2_INTERFACE, NULL, TESTDATA_2_USER, TESTDATA_2_PASSWD, false);
    assert_int_equal(rv, ddns_status_disabled);
    amxc_string_setf(&section_name, "section_%d_%d", TESTDATA_1_INDEX, TESTDATA_1_HOST_INDEX);

    ddns_section = uci_get_ddns_section(amxc_string_get(&section_name, 0), NULL);
    assert_non_null(ddns_section);
    values = GET_ARG(ddns_section, "values");
    assert_non_null(values);
    assert_string_equal(GET_CHAR(values, "domain"), TESTDATA_2_DOMAIN);
    assert_string_equal(GET_CHAR(values, "lookup_host"), TESTDATA_2_DOMAIN);
    assert_string_equal(GET_CHAR(values, "username"), TESTDATA_2_USER);
    assert_string_equal(GET_CHAR(values, "password"), TESTDATA_2_PASSWD);
    assert_string_equal(GET_CHAR(values, "ip_interface"), TESTDATA_2_INTERFACE);
    assert_string_equal(GET_CHAR(values, "ip_source"), "interface");
    assert_string_equal(GET_CHAR(values, "service_name"), TESTDATA_2_SERVER);
    assert_null(GET_CHAR(values, "update_url"));
    assert_string_equal(GET_CHAR(values, "enabled"), "0");
    assert_int_equal(GET_UINT32(values, "check_interval"), 10);
    assert_int_equal(GET_UINT32(values, "force_interval"), 20000);
    assert_int_equal(GET_UINT32(values, "retry_interval"), 60);
    assert_int_equal(GET_UINT32(values, "retry_max_count"), 0);
    amxc_var_delete(&ddns_section);

    assert_int_equal(uci_sections(), 2);
    assert_int_equal(processes_running(), 0);

    rv = (int) check_on_module(TESTDATA_1_INDEX, TESTDATA_1_HOST_INDEX);
    assert_int_equal(rv, ddns_error_no);

    rv = (int) check_on_module(TESTDATA_3_INDEX, TESTDATA_3_HOST_INDEX);
    assert_int_equal(rv, ddns_error_no);

    //Edit using IP
    rv = edit_on_module(TESTDATA_1_INDEX, TESTDATA_1_HOST_INDEX, TESTDATA_2_DOMAIN, TESTDATA_2_SERVER, TESTDATA_2_ADDRESS, TESTDATA_2_INTERFACE, TESTDATA_2_IP, TESTDATA_2_USER, TESTDATA_2_PASSWD, false);
    assert_int_equal(rv, ddns_status_disabled);
    amxc_string_setf(&section_name, "section_%d_%d", TESTDATA_1_INDEX, TESTDATA_1_HOST_INDEX);

    ddns_section = uci_get_ddns_section(amxc_string_get(&section_name, 0), NULL);
    assert_non_null(ddns_section);
    values = GET_ARG(ddns_section, "values");
    assert_non_null(values);
    assert_string_equal(GET_CHAR(values, "domain"), TESTDATA_2_DOMAIN);
    assert_string_equal(GET_CHAR(values, "lookup_host"), TESTDATA_2_DOMAIN);
    assert_string_equal(GET_CHAR(values, "username"), TESTDATA_2_USER);
    assert_string_equal(GET_CHAR(values, "password"), TESTDATA_2_PASSWD);
    assert_string_equal(GET_CHAR(values, "ip_interface"), TESTDATA_2_INTERFACE);
    assert_string_equal(GET_CHAR(values, "ip_script"), "/usr/lib/amx/scripts/ddns_scripts_echo_ip.sh");
    assert_string_equal(GET_CHAR(values, "ip_source"), "script");
    assert_string_equal(GET_CHAR(values, "service_name"), TESTDATA_2_SERVER);
    assert_null(GET_CHAR(values, "update_url"));
    assert_string_equal(GET_CHAR(values, "enabled"), "0");
    assert_int_equal(GET_UINT32(values, "check_interval"), 10);
    assert_int_equal(GET_UINT32(values, "force_interval"), 20000);
    assert_int_equal(GET_UINT32(values, "retry_interval"), 60);
    assert_int_equal(GET_UINT32(values, "retry_max_count"), 0);
    amxc_var_delete(&ddns_section);

    assert_int_equal(uci_sections(), 2);
    assert_int_equal(processes_running(), 0);

    rv = (int) check_on_module(TESTDATA_1_INDEX, TESTDATA_1_HOST_INDEX);
    assert_int_equal(rv, ddns_error_no);

    rv = (int) check_on_module(TESTDATA_3_INDEX, TESTDATA_3_HOST_INDEX);
    assert_int_equal(rv, ddns_error_no);

    //Delete
    rv = delete_on_module(TESTDATA_1_INDEX, TESTDATA_1_HOST_INDEX);
    assert_int_equal(rv, 0);

    assert_int_equal(uci_sections(), 1);
    assert_int_equal(processes_running(), 0);

    rv = delete_on_module(TESTDATA_3_INDEX, TESTDATA_3_HOST_INDEX);
    assert_int_equal(rv, 0);

    assert_int_equal(uci_sections(), 0);
    assert_int_equal(processes_running(), 0);

    amxc_string_clean(&section_name);
}

void test_edit_misconfiguration(UNUSED void** state) {
    ddns_status_t rv = -1;
    amxc_var_t* ddns_section = NULL;
    amxc_var_t* values = NULL;
    amxc_string_t section_name;

    amxc_string_init(&section_name, 0);

    rv = add_on_module(TESTDATA_1_INDEX, TESTDATA_1_HOST_INDEX, TESTDATA_1_DOMAIN, TESTDATA_1_SERVER, TESTDATA_1_ADDRESS, TESTDATA_1_INTERFACE, NULL, TESTDATA_1_USER, TESTDATA_1_PASSWD, true);
    assert_int_equal(rv, ddns_status_updated);
    amxc_string_setf(&section_name, "section_%d_%d", TESTDATA_1_INDEX, TESTDATA_1_HOST_INDEX);

    ddns_section = uci_get_ddns_section(amxc_string_get(&section_name, 0), NULL);
    assert_non_null(ddns_section);
    values = GET_ARG(ddns_section, "values");
    assert_non_null(values);
    assert_string_equal(GET_CHAR(values, "domain"), TESTDATA_1_DOMAIN);
    assert_string_equal(GET_CHAR(values, "lookup_host"), TESTDATA_1_DOMAIN);
    assert_string_equal(GET_CHAR(values, "username"), TESTDATA_1_USER);
    assert_string_equal(GET_CHAR(values, "password"), TESTDATA_1_PASSWD);
    assert_string_equal(GET_CHAR(values, "ip_interface"), TESTDATA_1_INTERFACE);
    assert_string_equal(GET_CHAR(values, "ip_source"), "interface");
    assert_string_equal(GET_CHAR(values, "service_name"), TESTDATA_1_SERVER);
    assert_null(GET_CHAR(values, "update_url"));
    assert_string_equal(GET_CHAR(values, "enabled"), "1");
    assert_int_equal(GET_UINT32(values, "check_interval"), 10);
    assert_int_equal(GET_UINT32(values, "force_interval"), 20000);
    assert_int_equal(GET_UINT32(values, "retry_interval"), 60);
    assert_int_equal(GET_UINT32(values, "retry_max_count"), 0);
    amxc_var_delete(&ddns_section);

    assert_int_equal(uci_sections(), 1);
    assert_int_equal(processes_running(), 1);

    //Edit without client instance
    rv = edit_on_module(0, TESTDATA_1_HOST_INDEX, TESTDATA_2_DOMAIN, TESTDATA_2_SERVER, TESTDATA_2_ADDRESS, TESTDATA_2_INTERFACE, NULL, TESTDATA_2_USER, TESTDATA_2_PASSWD, false);
    assert_int_equal(rv, ddns_status_error_config);

    assert_int_equal(uci_sections(), 1);
    assert_int_equal(processes_running(), 1);

    //Edit without hostname instance
    rv = edit_on_module(TESTDATA_1_INDEX, 0, TESTDATA_2_DOMAIN, TESTDATA_2_SERVER, TESTDATA_2_ADDRESS, TESTDATA_2_INTERFACE, NULL, TESTDATA_2_USER, TESTDATA_2_PASSWD, false);
    assert_int_equal(rv, ddns_status_error_config);

    assert_int_equal(uci_sections(), 1);
    assert_int_equal(processes_running(), 1);

    //Delete
    rv = delete_on_module(TESTDATA_1_INDEX, TESTDATA_1_HOST_INDEX);
    assert_int_equal(rv, 0);

    assert_int_equal(uci_sections(), 0);
    assert_int_equal(processes_running(), 0);

    amxc_string_clean(&section_name);
}

void test_delete_misconfiguration(UNUSED void** state) {
    ddns_status_t rv = -1;
    amxc_var_t* ddns_section = NULL;
    amxc_var_t* values = NULL;
    amxc_string_t section_name;

    amxc_string_init(&section_name, 0);

    rv = add_on_module(TESTDATA_1_INDEX, TESTDATA_1_HOST_INDEX, TESTDATA_1_DOMAIN, TESTDATA_1_SERVER, TESTDATA_1_ADDRESS, TESTDATA_1_INTERFACE, NULL, TESTDATA_1_USER, TESTDATA_1_PASSWD, true);
    assert_int_equal(rv, ddns_status_updated);
    amxc_string_setf(&section_name, "section_%d_%d", TESTDATA_1_INDEX, TESTDATA_1_HOST_INDEX);

    ddns_section = uci_get_ddns_section(amxc_string_get(&section_name, 0), NULL);
    assert_non_null(ddns_section);
    values = GET_ARG(ddns_section, "values");
    assert_non_null(values);
    assert_string_equal(GET_CHAR(values, "domain"), TESTDATA_1_DOMAIN);
    assert_string_equal(GET_CHAR(values, "lookup_host"), TESTDATA_1_DOMAIN);
    assert_string_equal(GET_CHAR(values, "username"), TESTDATA_1_USER);
    assert_string_equal(GET_CHAR(values, "password"), TESTDATA_1_PASSWD);
    assert_string_equal(GET_CHAR(values, "ip_interface"), TESTDATA_1_INTERFACE);
    assert_string_equal(GET_CHAR(values, "ip_source"), "interface");
    assert_string_equal(GET_CHAR(values, "service_name"), TESTDATA_1_SERVER);
    assert_null(GET_CHAR(values, "update_url"));
    assert_string_equal(GET_CHAR(values, "enabled"), "1");
    assert_int_equal(GET_UINT32(values, "check_interval"), 10);
    assert_int_equal(GET_UINT32(values, "force_interval"), 20000);
    assert_int_equal(GET_UINT32(values, "retry_interval"), 60);
    assert_int_equal(GET_UINT32(values, "retry_max_count"), 0);
    amxc_var_delete(&ddns_section);

    assert_int_equal(uci_sections(), 1);
    assert_int_equal(processes_running(), 1);

    //Delete without client instance
    rv = delete_on_module(0, TESTDATA_1_HOST_INDEX);
    assert_int_not_equal(rv, 0);

    assert_int_equal(uci_sections(), 1);
    assert_int_equal(processes_running(), 1);

    //Delete without hostname instance
    rv = delete_on_module(TESTDATA_1_INDEX, 0);
    assert_int_not_equal(rv, 0);

    assert_int_equal(uci_sections(), 1);
    assert_int_equal(processes_running(), 1);


    //Delete with different instance
    rv = delete_on_module(TESTDATA_2_INDEX, TESTDATA_1_HOST_INDEX);
    assert_int_not_equal(rv, 0);

    assert_int_equal(uci_sections(), 1);
    assert_int_equal(processes_running(), 1);

    //Delete with different host instance
    rv = delete_on_module(TESTDATA_1_INDEX, TESTDATA_2_HOST_INDEX);
    assert_int_not_equal(rv, 0);

    assert_int_equal(uci_sections(), 1);
    assert_int_equal(processes_running(), 1);

    //Delete
    rv = delete_on_module(TESTDATA_1_INDEX, TESTDATA_1_HOST_INDEX);
    assert_int_equal(rv, 0);

    assert_int_equal(uci_sections(), 0);
    assert_int_equal(processes_running(), 0);

    amxc_string_clean(&section_name);
}

void test_custom_service(UNUSED void** state) {
    ddns_status_t rv = -1;
    amxc_var_t* ddns_section = NULL;
    amxc_var_t* values = NULL;
    amxc_string_t section_name;

    amxc_string_init(&section_name, 0);

    //Add custom without address
    rv = add_on_module(TESTDATA_1_INDEX, TESTDATA_1_HOST_INDEX, TESTDATA_1_DOMAIN, "custom", NULL, TESTDATA_1_INTERFACE, NULL, TESTDATA_1_USER, TESTDATA_1_PASSWD, true);
    assert_int_equal(rv, ddns_status_error_config);
    assert_int_equal(uci_sections(), 0);
    assert_int_equal(processes_running(), 0);

    rv = add_on_module(TESTDATA_1_INDEX, TESTDATA_1_HOST_INDEX, TESTDATA_1_DOMAIN, "custom", "", TESTDATA_1_INTERFACE, NULL, TESTDATA_1_USER, TESTDATA_1_PASSWD, true);
    assert_int_equal(rv, ddns_status_error_config);
    assert_int_equal(uci_sections(), 0);
    assert_int_equal(processes_running(), 0);

    //Add
    rv = add_on_module(TESTDATA_1_INDEX, TESTDATA_1_HOST_INDEX, TESTDATA_1_DOMAIN, TESTDATA_1_SERVER, TESTDATA_1_ADDRESS, TESTDATA_1_INTERFACE, NULL, TESTDATA_1_USER, TESTDATA_1_PASSWD, true);
    assert_int_equal(rv, ddns_status_updated);
    amxc_string_setf(&section_name, "section_%d_%d", TESTDATA_1_INDEX, TESTDATA_1_HOST_INDEX);

    ddns_section = uci_get_ddns_section(amxc_string_get(&section_name, 0), NULL);
    assert_non_null(ddns_section);
    values = GET_ARG(ddns_section, "values");
    assert_non_null(values);
    assert_string_equal(GET_CHAR(values, "domain"), TESTDATA_1_DOMAIN);
    assert_string_equal(GET_CHAR(values, "lookup_host"), TESTDATA_1_DOMAIN);
    assert_string_equal(GET_CHAR(values, "username"), TESTDATA_1_USER);
    assert_string_equal(GET_CHAR(values, "password"), TESTDATA_1_PASSWD);
    assert_string_equal(GET_CHAR(values, "ip_interface"), TESTDATA_1_INTERFACE);
    assert_string_equal(GET_CHAR(values, "ip_source"), "interface");
    assert_string_equal(GET_CHAR(values, "service_name"), TESTDATA_1_SERVER);
    assert_null(GET_CHAR(values, "update_url"));
    assert_string_equal(GET_CHAR(values, "enabled"), "1");
    assert_int_equal(GET_UINT32(values, "check_interval"), 10);
    assert_int_equal(GET_UINT32(values, "force_interval"), 20000);
    assert_int_equal(GET_UINT32(values, "retry_interval"), 60);
    assert_int_equal(GET_UINT32(values, "retry_max_count"), 0);
    amxc_var_delete(&ddns_section);

    assert_int_equal(uci_sections(), 1);
    assert_int_equal(processes_running(), 1);

    //Add new
    rv = add_on_module(TESTDATA_3_INDEX, TESTDATA_3_HOST_INDEX, TESTDATA_3_DOMAIN, "custom", TESTDATA_3_ADDRESS, TESTDATA_3_INTERFACE, NULL, TESTDATA_3_USER, TESTDATA_3_PASSWD, false);
    assert_int_equal(rv, ddns_status_disabled);
    amxc_string_setf(&section_name, "section_%d_%d", TESTDATA_3_INDEX, TESTDATA_3_HOST_INDEX);

    ddns_section = uci_get_ddns_section(amxc_string_get(&section_name, 0), NULL);
    assert_non_null(ddns_section);
    values = GET_ARG(ddns_section, "values");
    assert_non_null(values);
    assert_string_equal(GET_CHAR(values, "domain"), TESTDATA_3_DOMAIN);
    assert_string_equal(GET_CHAR(values, "lookup_host"), TESTDATA_3_DOMAIN);
    assert_string_equal(GET_CHAR(values, "username"), TESTDATA_3_USER);
    assert_string_equal(GET_CHAR(values, "password"), TESTDATA_3_PASSWD);
    assert_string_equal(GET_CHAR(values, "ip_interface"), TESTDATA_3_INTERFACE);
    assert_string_equal(GET_CHAR(values, "ip_source"), "interface");
    assert_string_equal(GET_CHAR(values, "update_url"), TESTDATA_3_ADDRESS);
    assert_string_equal(GET_CHAR(values, "enabled"), "0");
    assert_int_equal(GET_UINT32(values, "check_interval"), 10);
    assert_int_equal(GET_UINT32(values, "force_interval"), 20000);
    assert_int_equal(GET_UINT32(values, "retry_interval"), 60);
    assert_int_equal(GET_UINT32(values, "retry_max_count"), 0);
    amxc_var_delete(&ddns_section);

    assert_int_equal(uci_sections(), 2);
    assert_int_equal(processes_running(), 1);

    //Edit first entry to a custom service
    rv = edit_on_module(TESTDATA_1_INDEX, TESTDATA_1_HOST_INDEX, TESTDATA_2_DOMAIN, "custom", TESTDATA_2_ADDRESS, TESTDATA_2_INTERFACE, NULL, TESTDATA_2_USER, TESTDATA_2_PASSWD, false);
    assert_int_equal(rv, ddns_status_disabled);
    amxc_string_setf(&section_name, "section_%d_%d", TESTDATA_1_INDEX, TESTDATA_1_HOST_INDEX);

    ddns_section = uci_get_ddns_section(amxc_string_get(&section_name, 0), NULL);
    assert_non_null(ddns_section);
    values = GET_ARG(ddns_section, "values");
    assert_non_null(values);
    assert_string_equal(GET_CHAR(values, "domain"), TESTDATA_2_DOMAIN);
    assert_string_equal(GET_CHAR(values, "lookup_host"), TESTDATA_2_DOMAIN);
    assert_string_equal(GET_CHAR(values, "username"), TESTDATA_2_USER);
    assert_string_equal(GET_CHAR(values, "password"), TESTDATA_2_PASSWD);
    assert_string_equal(GET_CHAR(values, "ip_interface"), TESTDATA_2_INTERFACE);
    assert_string_equal(GET_CHAR(values, "ip_source"), "interface");
    assert_null(GET_CHAR(values, "service_name"));
    assert_string_equal(GET_CHAR(values, "update_url"), TESTDATA_2_ADDRESS);
    assert_string_equal(GET_CHAR(values, "enabled"), "0");
    assert_int_equal(GET_UINT32(values, "check_interval"), 10);
    assert_int_equal(GET_UINT32(values, "force_interval"), 20000);
    assert_int_equal(GET_UINT32(values, "retry_interval"), 60);
    assert_int_equal(GET_UINT32(values, "retry_max_count"), 0);
    amxc_var_delete(&ddns_section);

    assert_int_equal(uci_sections(), 2);
    assert_int_equal(processes_running(), 0);

    //Edit first entry back to normal service
    rv = edit_on_module(TESTDATA_1_INDEX, TESTDATA_1_HOST_INDEX, NULL, TESTDATA_2_SERVER, NULL, NULL, NULL, NULL, NULL, true);
    assert_int_equal(rv, ddns_status_updated);
    amxc_string_setf(&section_name, "section_%d_%d", TESTDATA_1_INDEX, TESTDATA_1_HOST_INDEX);

    ddns_section = uci_get_ddns_section(amxc_string_get(&section_name, 0), NULL);
    assert_non_null(ddns_section);
    values = GET_ARG(ddns_section, "values");
    assert_non_null(values);
    assert_string_equal(GET_CHAR(values, "domain"), TESTDATA_2_DOMAIN);
    assert_string_equal(GET_CHAR(values, "lookup_host"), TESTDATA_2_DOMAIN);
    assert_string_equal(GET_CHAR(values, "username"), TESTDATA_2_USER);
    assert_string_equal(GET_CHAR(values, "password"), TESTDATA_2_PASSWD);
    assert_string_equal(GET_CHAR(values, "ip_interface"), TESTDATA_2_INTERFACE);
    assert_string_equal(GET_CHAR(values, "ip_source"), "interface");
    assert_string_equal(GET_CHAR(values, "service_name"), TESTDATA_2_SERVER);
    assert_null(GET_CHAR(values, "update_url"));
    assert_string_equal(GET_CHAR(values, "enabled"), "1");
    assert_int_equal(GET_UINT32(values, "check_interval"), 10);
    assert_int_equal(GET_UINT32(values, "force_interval"), 20000);
    assert_int_equal(GET_UINT32(values, "retry_interval"), 60);
    assert_int_equal(GET_UINT32(values, "retry_max_count"), 0);
    amxc_var_delete(&ddns_section);

    assert_int_equal(uci_sections(), 2);
    assert_int_equal(processes_running(), 1);

    //Edit second entry's URL
    rv = edit_on_module(TESTDATA_3_INDEX, TESTDATA_3_HOST_INDEX, NULL, NULL, TESTDATA_1_ADDRESS, NULL, NULL, NULL, NULL, false);
    assert_int_equal(rv, ddns_status_disabled);
    amxc_string_setf(&section_name, "section_%d_%d", TESTDATA_3_INDEX, TESTDATA_3_HOST_INDEX);

    ddns_section = uci_get_ddns_section(amxc_string_get(&section_name, 0), NULL);
    assert_non_null(ddns_section);
    values = GET_ARG(ddns_section, "values");
    assert_non_null(values);
    assert_string_equal(GET_CHAR(values, "domain"), TESTDATA_3_DOMAIN);
    assert_string_equal(GET_CHAR(values, "lookup_host"), TESTDATA_3_DOMAIN);
    assert_string_equal(GET_CHAR(values, "username"), TESTDATA_3_USER);
    assert_string_equal(GET_CHAR(values, "password"), TESTDATA_3_PASSWD);
    assert_string_equal(GET_CHAR(values, "ip_interface"), TESTDATA_3_INTERFACE);
    assert_string_equal(GET_CHAR(values, "ip_source"), "interface");
    assert_null(GET_CHAR(values, "service_name"));
    assert_string_equal(GET_CHAR(values, "update_url"), TESTDATA_1_ADDRESS);
    assert_string_equal(GET_CHAR(values, "enabled"), "0");
    assert_int_equal(GET_UINT32(values, "check_interval"), 10);
    assert_int_equal(GET_UINT32(values, "force_interval"), 20000);
    assert_int_equal(GET_UINT32(values, "retry_interval"), 60);
    assert_int_equal(GET_UINT32(values, "retry_max_count"), 0);
    amxc_var_delete(&ddns_section);

    assert_int_equal(uci_sections(), 2);
    assert_int_equal(processes_running(), 1);

    rv = (int) check_on_module(TESTDATA_1_INDEX, TESTDATA_1_HOST_INDEX);
    assert_int_equal(rv, ddns_error_no);

    rv = (int) check_on_module(TESTDATA_3_INDEX, TESTDATA_3_HOST_INDEX);
    assert_int_equal(rv, ddns_error_no);

    //Delete
    rv = delete_on_module(TESTDATA_1_INDEX, TESTDATA_1_HOST_INDEX);
    assert_int_equal(rv, 0);

    assert_int_equal(uci_sections(), 1);
    assert_int_equal(processes_running(), 0);

    rv = delete_on_module(TESTDATA_3_INDEX, TESTDATA_3_HOST_INDEX);
    assert_int_equal(rv, 0);

    assert_int_equal(uci_sections(), 0);
    assert_int_equal(processes_running(), 0);

    amxc_string_clean(&section_name);
}