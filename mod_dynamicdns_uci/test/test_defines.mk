MACHINE = $(shell $(CC) -dumpmachine)

SRCDIR = $(realpath ../../src)
OBJDIR = $(realpath ../../output/$(MACHINE)/mod_uci_coverage)
INCDIR = $(realpath ../../include ../../../include_priv ../../../test/mock_includes/ ../../include_priv ../include ../mocks/)
MOCK_SRCDIR = $(realpath ../mocks/)
PARENT_MOCK_SRCDIR = $(realpath ../../../test/mock_src/)
SCRIPT_DIR = $(realpath ../../scripts/)

HEADERS = $(wildcard $(INCDIR)/*.h)
SOURCES = $(wildcard $(SRCDIR)/*.c) \
		  $(wildcard $(PARENT_MOCK_SRCDIR)/*.c) \
		  $(wildcard $(MOCK_SRCDIR)/*.c)

WRAP_FUNC=-Wl,--wrap=

CFLAGS += -Werror -Wall -Wextra -Wno-attributes\
		  --std=gnu99 -fPIC -g3 -Wmissing-declarations \
		  $(addprefix -I ,$(INCDIR)) -I$(OBJDIR)/.. \
		  -fkeep-inline-functions -fkeep-static-functions \
		   -Wno-format-nonliteral \
		  $(shell pkg-config --cflags cmocka) -pthread \
		   -DSAHTRACES_ENABLED -DSAHTRACES_LEVEL=500

LDFLAGS += -fkeep-inline-functions -fkeep-static-functions \
		   $(shell pkg-config --libs cmocka) \
		   -lamxb -lamxc -lamxd -lamxj -lamxm -lamxo -lamxut -lamxp -ldl -lsahtrace -lpthread -lm
