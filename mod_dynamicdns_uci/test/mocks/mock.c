/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <string.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>
#include <amxd/amxd_path.h>

#include <amxo/amxo.h>
#include <amxb/amxb_be_intf.h>
#include <amxb/amxb_register.h>
#include <amxb/amxb.h>

#include <amxm/amxm.h>

#include "mock.h"
#include "uci_mock.h"
#include "amxp_mock.h"
#include "mod_ddns_common.h"
#include "mod_ddns_uci_commit.h"
#include "mod_ddns_uci_scripts.h"
#include "mod_ddns_uci_utils.h"

amxo_parser_t* get_parser(void) {
    return amxut_bus_parser();
}

amxd_dm_t* get_dm(void) {
    return amxut_bus_dm();
}

static void test_init_dummy_fn_resolvers(amxo_parser_t* p_parser) {
    // uci
    amxo_resolver_ftab_add(p_parser, "get", AMXO_FUNC(mock_uci_get));
    amxo_resolver_ftab_add(p_parser, "add", AMXO_FUNC(mock_uci_add));
    amxo_resolver_ftab_add(p_parser, "set", AMXO_FUNC(mock_uci_set));
    amxo_resolver_ftab_add(p_parser, "delete", AMXO_FUNC(mock_uci_delete));
    amxo_resolver_ftab_add(p_parser, "rename", AMXO_FUNC(mock_uci_rename));
    amxo_resolver_ftab_add(p_parser, "commit", AMXO_FUNC(mock_uci_commit));
    amxo_resolver_ftab_add(p_parser, "changes", AMXO_FUNC(mock_uci_changes));
}

void mock_init(void** state) {
    amxd_object_t* root = NULL;
    amxd_dm_t* dm = NULL;
    amxo_parser_t* parser = NULL;

    amxut_bus_setup(state);
    parser = amxut_bus_parser();
    dm = amxut_bus_dm();
    root = amxd_dm_get_root(dm);

    test_init_dummy_fn_resolvers(parser);

    assert_int_equal(amxo_parser_parse_file(parser, "../../../test/mock_odl/mock_uci.odl", root), 0);

    amxut_bus_handle_events();
}

void mock_cleanup(void** state) {
    amxut_bus_handle_events();
    uci_cleanup();
    amxp_mock_cleanup();
    amxut_bus_teardown(state);
    amxm_close_all();
    amxo_resolver_import_close_all();
}
