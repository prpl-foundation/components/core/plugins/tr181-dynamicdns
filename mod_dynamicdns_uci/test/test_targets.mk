MODULE_DIR = $(shell dirname $(TARGET_MODULE))
ifeq ($(MODULE_DIR),)
	MODULE_DIR = .
endif

all: $(TARGET)

run: $(TARGET) $(TARGET_MODULE)
	@rm -rf $(MODULE_DIR)/scripts
	@mkdir -p $(MODULE_DIR)/scripts
	@cp -rf $(SCRIPT_DIR)/* $(MODULE_DIR)/scripts
	set -o pipefail; valgrind --leak-check=full --show-leak-kinds=all --keep-debuginfo=yes --exit-on-first-error=yes --error-exitcode=1 ./$< 2>&1 | tee -a $(OBJDIR)/unit_test_results.txt;
	@rm -rf $(MODULE_DIR)/scripts
	rm -rf ./*.log

$(TARGET): $(OBJECTS)
	$(CC) -o $@ $(OBJECTS) $(LDFLAGS) -fprofile-arcs -ftest-coverage

$(TARGET_MODULE): $(OBJECTS)
	$(CC) -shared $(OBJECTS) -fprofile-arcs -ftest-coverage -Wl,-soname,$(@) -o $@ $(LDFLAGS)

-include $(OBJECTS:.o=.d)

$(OBJDIR)/%.o: ./%.c | $(OBJDIR)/
	$(CC) $(CFLAGS) -fprofile-arcs -ftest-coverage -c -o $@ $<
	@$(CC) $(CFLAGS) -MM -MP -MT '$(@) $(@:.o=.d)' -MF $(@:.o=.d) $(<)

$(OBJDIR)/%.o: $(SRCDIR)/%.c | $(OBJDIR)/
	$(CC) $(CFLAGS) -fprofile-arcs -ftest-coverage -c -o $@ $<
	@$(CC) $(CFLAGS) -MM -MP -MT '$(@) $(@:.o=.d)' -MF $(@:.o=.d) $(<)

$(OBJDIR)/%.o: $(PARENT_MOCK_SRCDIR)/%.c | $(OBJDIR)/
	$(CC) $(CFLAGS) -fprofile-arcs -ftest-coverage -c -o $@ $<
	@$(CC) $(CFLAGS) -MM -MP -MT '$(@) $(@:.o=.d)' -MF $(@:.o=.d) $(<)

$(OBJDIR)/%.o: $(MOCK_SRCDIR)/%.c | $(OBJDIR)/
	$(CC) $(CFLAGS) -fprofile-arcs -ftest-coverage -c -o $@ $<
	@$(CC) $(CFLAGS) -MM -MP -MT '$(@) $(@:.o=.d)' -MF $(@:.o=.d) $(<)

$(OBJDIR)/:
	mkdir -p $@

clean:
	rm -rf $(TARGET) $(OBJDIR)
	rm -f $(TARGET_MODULE) $(OBJDIR)
	rm -rf scripts
	rm -rf ./*.log

.PHONY: clean $(OBJDIR)/
