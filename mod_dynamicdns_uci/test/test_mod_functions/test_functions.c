/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <string.h>

#include <amxc/amxc.h>

#include <amxm/amxm.h>

#include "ddns_error_codes.h"
#include "mock.h"
#include "test_functions.h"
#include "ddns.h"
#include "mod_ddns_common.h"
#include "mod_ddns_uci_commit.h"
#include "mod_ddns_uci_utils.h"
#include "mod_ddns_uci_scripts.h"

#define TEST_ADDR "http://[USERNAME]:[PASSWORD]@freedns.afraid.org/nic/update?hostname=[DOMAIN]&myip=[IP]"

static amxc_var_t* params = NULL;

static void validate_uci_section(amxc_var_t* section, const char* domain, const char* username,
                                 const char* password, const char* enabled) {
    amxc_var_t* tmp_var = NULL;

    tmp_var = GET_ARG(section, "domain");
    assert_non_null(tmp_var);
    if(domain != NULL) {
        assert_string_equal(GET_CHAR(tmp_var, NULL), domain);
    }
    tmp_var = GET_ARG(section, "lookup_host");
    assert_non_null(tmp_var);
    if(domain != NULL) {
        assert_string_equal(GET_CHAR(tmp_var, NULL), domain);
    }
    tmp_var = GET_ARG(section, "username");
    assert_non_null(tmp_var);
    if(username != NULL) {
        assert_string_equal(GET_CHAR(tmp_var, NULL), username);
    }
    tmp_var = GET_ARG(section, "password");
    assert_non_null(tmp_var);
    if(password != NULL) {
        assert_string_equal(GET_CHAR(tmp_var, NULL), password);
    }
    tmp_var = GET_ARG(section, "enabled");
    assert_non_null(tmp_var);
    if(enabled != NULL) {
        assert_string_equal(GET_CHAR(tmp_var, NULL), enabled);
    }
    tmp_var = GET_ARG(section, "service_name");
    if(tmp_var == NULL) {
        tmp_var = GET_ARG(section, "update_url");
        assert_non_null(tmp_var);
        assert_string_equal(GET_CHAR(tmp_var, NULL), TEST_ADDR);
    } else {
        tmp_var = GET_ARG(section, "update_url");
        assert_null(tmp_var);
    }
}

static void set_params(const char* server, const char* force_ipversion, const char* use_ipv6, const char* ip_source, const char* ip) {
    amxc_var_delete(&params);
    amxc_var_new(&params);
    amxc_var_set_type(params, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, params, SERVER_SERVICE, server);
    if(server != NULL) {
        if(strcmp(server, "custom") == 0) {
            amxc_var_add_key(cstring_t, params, SERVER_ADDRESS, TEST_ADDR);
        }
    }
    amxc_var_add_key(cstring_t, params, SERVER_PROTOCOL, "HTTPS");
    amxc_var_add_key(cstring_t, params, SERVER_CHECK_INTERVAL, "600");
    amxc_var_add_key(cstring_t, params, SERVER_RETRY_INTERVAL, "60");
    amxc_var_add_key(cstring_t, params, SERVER_MAX_RETRIES, "0");
    if(!str_empty(force_ipversion)) {
        amxc_var_add_key(cstring_t, params, MOD_FORCE_IPVERSION, force_ipversion);
    }
    if(!str_empty(use_ipv6)) {
        amxc_var_add_key(cstring_t, params, MOD_USE_IPV6, use_ipv6);
    }
    if(!str_empty(ip_source)) {
        amxc_var_add_key(cstring_t, params, MOD_IP_SOURCE, ip_source);
    }
    if(!str_empty(ip)) {
        amxc_var_add_key(cstring_t, params, CLIENT_IP, ip);
    }
}

int test_functions_setup(void** state) {
    mock_init(state);
    return 0;
}

int test_functions_teardown(void** state) {
    mock_cleanup(state);
    amxc_var_delete(&params);
    return 0;
}

void test_uci_operations(UNUSED void** state) {
    int ret_value = -1;
    amxc_var_t* ret_from_op = NULL;
    amxc_var_t* values = NULL;
    amxc_var_t* section = NULL;
    amxc_var_t* tmp_var = NULL;
    amxc_var_t* uci_value = NULL;
    amxc_var_t* new_values = NULL;
    const amxc_htable_t* ht_values = NULL;

    //UCI commit
    assert_int_equal(uci_commit(), 0);

    //The uci config is empty, check if is empty
    ret_from_op = uci_get_ddns_all();
    values = GET_ARG(ret_from_op, "values");
    assert_null(values);
    amxc_var_delete(&ret_from_op);

    //The uci config is empty, check if is empty
    ret_from_op = uci_get_ddns_section("test_section_1", NULL);
    values = GET_ARG(ret_from_op, "values");
    assert_null(values);
    amxc_var_delete(&ret_from_op);

    //Set non supported services
    amxc_var_new(&uci_value);
    amxc_var_set_type(uci_value, AMXC_VAR_ID_HTABLE);
    set_params(NULL, NULL, NULL, NULL, NULL);
    ret_value = uci_set_service(params, uci_value);
    assert_int_equal(ret_value, -1);
    amxc_var_delete(&uci_value);

    //Set supported services
    //Afraid.org
    amxc_var_new(&uci_value);
    amxc_var_set_type(uci_value, AMXC_VAR_ID_HTABLE);
    set_params("afraid.org-basicauth", NULL, NULL, NULL, NULL);
    ret_value = uci_set_service(params, uci_value);
    assert_int_equal(ret_value, 0);
    assert_non_null(uci_value);
    tmp_var = GET_ARG(uci_value, "service_name");
    assert_non_null(tmp_var);
    assert_false(STRING_EMPTY(GET_CHAR(tmp_var, NULL)));
    assert_string_equal(GET_CHAR(tmp_var, NULL), "afraid.org-basicauth");
    tmp_var = GET_ARG(uci_value, "update_url");
    assert_null(tmp_var);
    amxc_var_delete(&uci_value);

    //custom
    amxc_var_new(&uci_value);
    amxc_var_set_type(uci_value, AMXC_VAR_ID_HTABLE);
    set_params("custom", NULL, NULL, NULL, NULL);
    ret_value = uci_set_service(params, uci_value);
    assert_int_equal(ret_value, 0);
    assert_non_null(uci_value);
    tmp_var = GET_ARG(uci_value, "service_name");
    assert_null(tmp_var);
    tmp_var = GET_ARG(uci_value, "update_url");
    assert_non_null(tmp_var);
    assert_false(STRING_EMPTY(GET_CHAR(tmp_var, NULL)));
    assert_string_equal(GET_CHAR(tmp_var, NULL), TEST_ADDR);
    amxc_var_delete(&uci_value);

    //Add valid and invalid entries to UCI
    set_params("afraid.org-basicauth", NULL, NULL, NULL, NULL);
    assert_int_not_equal(uci_add_ddns(NULL, params, "test.com", "eth0", "dnsuser", "password", 0), 0);
    assert_int_not_equal(uci_add_ddns("", params, "test.com", "eth0", "dnsuser", "password", 0), 0);
    assert_int_not_equal(uci_add_ddns("test_section_1", params, NULL, "eth0", "dnsuser", "password", 0), 0);
    assert_int_not_equal(uci_add_ddns("test_section_2", params, "", "eth0", "dnsuser", "password", 0), 0);
    set_params(NULL, NULL, NULL, NULL, NULL);
    assert_int_not_equal(uci_add_ddns("test_section_3", params, "test.com", "eth0", "dnsuser", "password", 0), 0);
    set_params("", NULL, NULL, NULL, NULL);
    assert_int_not_equal(uci_add_ddns("test_section_4", params, "test.com", "eth0", "dnsuser", "password", 0), 0);
    set_params("afraid.org-basicauth", NULL, NULL, "interface", NULL);
    assert_int_not_equal(uci_add_ddns("test_section_6", params, "test.org", NULL, "dnsuser", "password", 0), 0);
    assert_int_not_equal(uci_add_ddns("test_section_7", params, "test.org", "", "dnsuser", "password", 0), 0);
    assert_int_not_equal(uci_add_ddns("test_section_8", params, "test.org", "eth0", NULL, "password", 0), 0);
    assert_int_not_equal(uci_add_ddns("test_section_9", params, "test.org", "eth0", "", "password", 0), 0);
    assert_int_not_equal(uci_add_ddns("test_section_10", params, "test.net", "eth0", "dnsuser", NULL, 0), 0);
    assert_int_not_equal(uci_add_ddns("test_section_11", params, "test.net", "eth0", "dnsuser", "", 0), 0);
    assert_int_equal(uci_add_ddns("test_section_12", params, "test.be", "eth0", "dnsuser", "password", 0), 0);
    assert_int_not_equal(uci_add_ddns("test_section_12", params, "test.br", "eth0", "dnsuser", "password", 0), 0);
    assert_int_equal(uci_add_ddns("test_section_13", params, "test.nl", "eth0", "dnsuser", "password", 0), 0);
    assert_int_equal(uci_add_ddns("test_section_14", params, "test.it", "eth0", "dnsuser", "password", 0), 0);
    assert_int_equal(uci_add_ddns("test_section_15", params, "test.de", "eth0", "dnsuser", "password", 0), 0);
    set_params("custom", NULL, NULL, "interface", NULL);
    assert_int_not_equal(uci_add_ddns("test_section_17", params, "test.org", NULL, "dnsuser", "password", 0), 0);
    assert_int_not_equal(uci_add_ddns("test_section_18", params, "test.org", "", "dnsuser", "password", 0), 0);
    assert_int_not_equal(uci_add_ddns("test_section_19", params, "test.org", "eth0", NULL, "password", 0), 0);
    assert_int_not_equal(uci_add_ddns("test_section_20", params, "test.org", "eth0", "", "password", 0), 0);
    assert_int_not_equal(uci_add_ddns("test_section_21", params, "test.net", "eth0", "dnsuser", NULL, 0), 0);
    assert_int_not_equal(uci_add_ddns("test_section_22", params, "test.net", "eth0", "dnsuser", "", 0), 0);
    assert_int_equal(uci_add_ddns("test_section_23", params, "test.be", "eth0", "dnsuser", "password", 0), 0);
    assert_int_not_equal(uci_add_ddns("test_section_23", params, "test.br", "eth0", "dnsuser", "password", 0), 0);
    assert_int_equal(uci_add_ddns("test_section_24", params, "test.nl", "eth0", "dnsuser", "password", 0), 0);
    assert_int_equal(uci_add_ddns("test_section_25", params, "test.it", "eth0", "dnsuser", "password", 0), 0);
    assert_int_equal(uci_add_ddns("test_section_26", params, "test.de", "eth0", "dnsuser", "password", 0), 0);
    set_params("custom", NULL, NULL, "script", "");
    assert_int_not_equal(uci_add_ddns("test_section_27", params, "test.org", NULL, "dnsuser", "password", 0), 0);
    assert_int_not_equal(uci_add_ddns("test_section_28", params, "test.org", "", "dnsuser", "password", 0), 0);
    assert_int_not_equal(uci_add_ddns("test_section_29", params, "test.org", "eth0", NULL, "password", 0), 0);
    assert_int_not_equal(uci_add_ddns("test_section_30", params, "test.org", "eth0", "", "password", 0), 0);
    assert_int_not_equal(uci_add_ddns("test_section_31", params, "test.net", "eth0", "dnsuser", NULL, 0), 0);
    assert_int_not_equal(uci_add_ddns("test_section_32", params, "test.net", "eth0", "dnsuser", "", 0), 0);
    assert_int_not_equal(uci_add_ddns("test_section_33", params, "test.be", "eth0", "dnsuser", "password", 0), 0);
    assert_int_not_equal(uci_add_ddns("test_section_34", params, "test.br", "eth0", "dnsuser", "password", 0), 0);
    assert_int_not_equal(uci_add_ddns("test_section_35", params, "test.nl", "eth0", "dnsuser", "password", 0), 0);
    assert_int_not_equal(uci_add_ddns("test_section_36", params, "test.it", "eth0", "dnsuser", "password", 0), 0);
    assert_int_not_equal(uci_add_ddns("test_section_37", params, "test.de", "eth0", "dnsuser", "password", 0), 0);

    //Set the UCI values
    assert_int_not_equal(uci_set_ddns("test_section_15", new_values), 0);
    amxc_var_new(&new_values);
    amxc_var_set_type(new_values, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, new_values, "domain", "test.fr");
    amxc_var_add_key(cstring_t, new_values, "lookup_host", "test.fr");
    amxc_var_add_key(cstring_t, new_values, "username", "dnsnewuser");
    amxc_var_add_key(cstring_t, new_values, "enabled", "1");
    assert_int_not_equal(uci_set_ddns("", new_values), 0);
    assert_int_not_equal(uci_set_ddns("test_section_16", new_values), 0);
    assert_int_equal(uci_set_ddns("test_section_15", new_values), 0);
    amxc_var_delete(&new_values);

    //Validate the configuration
    ret_from_op = uci_get_ddns_all();
    values = GET_ARG(ret_from_op, "values");
    assert_non_null(values);
    ht_values = amxc_var_constcast(amxc_htable_t, values);
    assert_int_equal(amxc_htable_size(ht_values), 8);

    section = GET_ARG(values, "test_section_12");
    assert_non_null(section);
    validate_uci_section(section, "test.be", "dnsuser", "password", "0");

    section = GET_ARG(values, "test_section_13");
    assert_non_null(section);
    validate_uci_section(section, "test.nl", "dnsuser", "password", "0");

    section = GET_ARG(values, "test_section_14");
    assert_non_null(section);
    validate_uci_section(section, "test.it", "dnsuser", "password", "0");

    section = GET_ARG(values, "test_section_15");
    assert_non_null(section);
    validate_uci_section(section, "test.fr", "dnsnewuser", "password", "1");

    section = GET_ARG(values, "test_section_26");
    assert_non_null(section);
    validate_uci_section(section, "test.de", "dnsuser", "password", "0");
    amxc_var_delete(&ret_from_op);

    //Get the section
    ret_from_op = uci_get_ddns_section("test_section_15", NULL);
    assert_non_null(ret_from_op);
    values = GET_ARG(ret_from_op, "values");
    assert_non_null(values);
    validate_uci_section(values, "test.fr", "dnsnewuser", "password", "1");
    amxc_var_delete(&ret_from_op);

    //Delete the section
    assert_int_not_equal(uci_delete_ddns(""), 0);
    assert_int_not_equal(uci_delete_ddns("test_section_1"), 0);
    assert_int_equal(uci_delete_ddns("test_section_15"), 0);
    ret_from_op = uci_get_ddns_section("test_section_15", NULL);
    assert_null(ret_from_op);
    values = GET_ARG(ret_from_op, "values");
    assert_null(values);
    amxc_var_delete(&ret_from_op);

    //Rename the section
    assert_int_not_equal(uci_rename_ddns("", ""), 0);
    assert_int_not_equal(uci_rename_ddns("", "test_section_16"), 0);
    assert_int_not_equal(uci_rename_ddns("test_section_14", ""), 0);
    assert_int_not_equal(uci_rename_ddns("test_section_16", "test_section_1"), 0);
    assert_int_equal(uci_rename_ddns("test_section_14", "test_section_1"), 0);

    ret_from_op = uci_get_ddns_section("test_section_14", NULL);
    assert_null(ret_from_op);
    values = GET_ARG(ret_from_op, "values");
    assert_null(values);
    amxc_var_delete(&ret_from_op);

    ret_from_op = uci_get_ddns_section("test_section_1", NULL);
    assert_non_null(ret_from_op);
    values = GET_ARG(ret_from_op, "values");
    assert_non_null(values);
    validate_uci_section(values, "test.it", "dnsuser", "password", "0");
    amxc_var_delete(&ret_from_op);

    //Cleanup UCI entries
    assert_int_equal(uci_clean_ddns_uci_entries(), 0);

    ret_from_op = uci_get_ddns_all();
    values = GET_ARG(ret_from_op, "values");
    assert_non_null(values);
    ht_values = amxc_var_constcast(amxc_htable_t, values);
    assert_int_equal(amxc_htable_size(ht_values), 0);
    amxc_var_delete(&ret_from_op);
}

void test_read_from_file(UNUSED void** state) {
    FILE* p_file = NULL;
    amxc_string_t* string = NULL;
    int result = 0;

    if(access("file_test", F_OK) == 0) {
        remove("file_test");
    }

    p_file = fopen("file_test", "w+");
    assert_non_null(p_file);

    fprintf(p_file, "This will be the output");
    fclose(p_file);

    amxc_string_new(&string, 0);

    read_from_file("file_test", &string);
    if(strcmp(string->buffer, "This will be the output") != 0) {
        result = 1;
        goto evaluate;
    }

evaluate:
    amxc_string_delete(&string);
    if(access("file_test", F_OK) == 0) {
        remove("file_test");
    }
    assert_int_equal(result, 0);
}

void test_read_from_fd(UNUSED void** state) {
    amxc_string_t* string = NULL;
    int fd;
    int newfd;
    int result = 0;

    if(access("fd_test", F_OK) == 0) {
        remove("fd_test");
    }
    fd = open("fd_test", O_RDWR | O_CREAT | O_TRUNC);
    assert_true(fd > 0);
    write(fd, "This will be the output", 23);
    close(fd);

    amxc_string_new(&string, 0);

    newfd = open("fd_test", O_RDWR);
    read_from_fd(newfd, &string);
    close(newfd);
    if(strcmp(string->buffer, "This will be the output") != 0) {
        result = 1;
        goto evaluate;
    }

evaluate:
    amxc_string_delete(&string);
    if(access("fd_test", F_OK) == 0) {
        remove("fd_test");
    }
    assert_int_equal(result, 0);
}

void test_read_json_from_file(UNUSED void** state) {
    amxc_var_t* json_var = NULL;
    amxc_var_t* entry = NULL;

    json_var = read_json_from_file("./test_data/test_json.json");
    assert_non_null(json_var);

    entry = GET_ARG(json_var, "entry_1");
    assert_non_null(entry);
    assert_string_equal(GET_CHAR(entry, NULL), "Test 1");

    entry = GET_ARG(json_var, "entry_2");
    assert_non_null(entry);
    assert_string_equal(GET_CHAR(entry, NULL), "Test 2");

    entry = GET_ARG(json_var, "entry_3");
    assert_non_null(entry);
    assert_string_equal(GET_CHAR(entry, NULL), "Test 3");

    amxc_var_delete(&json_var);
}

void test_uci_ddns_scripts(UNUSED void** state) {
    //Start sections
    assert_int_not_equal(ddns_start_section(""), 0);
    assert_int_equal(ddns_start_section("test_section_1"), 0);
    assert_int_equal(ddns_start_section("test_section_2"), 0);
    assert_int_equal(ddns_start_section("test_section_3"), 0);

    //Check if the sections are running
    assert_false(ddns_is_running(""));
    assert_false(ddns_is_running("test_section_4"));
    assert_true(ddns_is_running("test_section_1"));
    assert_true(ddns_is_running("test_section_2"));
    assert_true(ddns_is_running("test_section_3"));

    //Delete a section
    assert_int_not_equal(ddns_stop_section(""), 0);
    assert_int_not_equal(ddns_stop_section("test_section_4"), 0);
    assert_int_equal(ddns_stop_section("test_section_2"), 0);

    //Check if the sections are running
    assert_true(ddns_is_running("test_section_1"));
    assert_false(ddns_is_running("test_section_2"));
    assert_true(ddns_is_running("test_section_3"));

    //Check the error sections
    assert_int_equal(ddns_get_status("test_section_1"), ddns_error_config);
    assert_int_equal(ddns_get_status("test_section_2"), ddns_error_config);
    assert_int_equal(ddns_get_status("test_section_3"), ddns_error_config);

    //Disable all the services
    assert_int_equal(ddns_disable_service(), 0);

    //Check if the sections are running
    assert_false(ddns_is_running("test_section_1"));
    assert_false(ddns_is_running("test_section_2"));
    assert_false(ddns_is_running("test_section_3"));
}