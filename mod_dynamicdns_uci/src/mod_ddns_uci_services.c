/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <yajl/yajl_gen.h>
#include <yajl/yajl_parse.h>
#include <fcntl.h>


#include <amxc/amxc_variant.h>
#include <amxc/amxc_variant_type.h>
#include <amxc/amxc_macros.h>
#include <amxc/amxc_lqueue.h>
#include <amxp/amxp_signal.h>
#include <amxd/amxd_types.h>
#include <amxp/amxp_slot.h>
#include <amxb/amxb_be_mngr.h>
#include <amxj/amxj_variant.h>
#include <amxb/amxb.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "ddns.h"
#include "mod_ddns_common.h"
#include "mod_ddns_uci_services.h"

#define ME "ddns-uci"

#ifdef DDNS_UNIT_TEST
    #define SERVICES_PATH "../test_data/default"
#else
    #define SERVICES_PATH "/usr/share/ddns/default"
#endif

static int parse_service_file(amxc_var_t* const service_content, amxc_var_t* ret) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    int ret_value = -1;
    amxc_var_t* service = NULL;
    const char* name = NULL;
    amxc_llist_t* service_info = NULL;
    amxc_string_t url;
    amxc_string_t custom_name;
    int char_pos = 0;
    int http_pos = 0;

    amxc_llist_new(&service_info);
    amxc_string_init(&url, 0);
    amxc_string_init(&custom_name, 0);

    when_null(service_content, exit);

    name = GET_CHAR(service_content, "name");
    when_str_empty(name, exit);
    when_str_empty(GETP_CHAR(service_content, "ipv4.url"), exit);
    amxc_string_setf(&url, "%s", GETP_CHAR(service_content, "ipv4.url"));

    http_pos = amxc_string_search(&url, "https://", 0);
    amxc_string_replace(&url, "http://", "", UINT32_MAX);
    amxc_string_replace(&url, "https://", "", UINT32_MAX);
    amxc_string_replace(&url, "www.", "", UINT32_MAX);
    amxc_string_replace(&url, ":", "", UINT32_MAX);
    amxc_string_replace(&url, "@", "", UINT32_MAX);
    amxc_string_replace(&url, "=", "", UINT32_MAX);
    amxc_string_replace(&url, "&", "", UINT32_MAX);
    amxc_string_replace(&url, "?", "", UINT32_MAX);
    char_pos = amxc_string_search(&url, "/", 0);
    if(char_pos >= 0) {
        ret_value = amxc_string_remove_at(&url, char_pos, UINT32_MAX);
        when_failed(ret_value, exit);
    }
    when_str_empty(amxc_string_get(&url, 0), exit);

    service = amxc_var_add_key(amxc_htable_t, ret, name, NULL);
    amxc_string_setf(&custom_name, "uci_%s", name);
    amxc_var_add_key(cstring_t, service, SERVER_NAME, amxc_string_get(&custom_name, 0));
    amxc_var_add_key(cstring_t, service, SERVER_SERVICE, name);
    amxc_var_add_key(cstring_t, service, SERVER_ADDRESS, amxc_string_get(&url, 0));
    if(http_pos >= 0) {
        amxc_var_add_key(cstring_t, service, SERVER_PROTOCOL, "HTTPS");
    } else {
        amxc_var_add_key(cstring_t, service, SERVER_PROTOCOL, "HTTP");
    }

    rv = 0;
exit:
    amxc_llist_delete(&service_info, amxc_string_list_it_free);
    amxc_string_clean(&url);
    amxc_string_clean(&custom_name);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

/**
 * @brief Create service JSON.
 * @return 0 if the function runs with success.
 */
int parse_services(amxc_var_t* ret) {
    SAH_TRACEZ_IN(ME);
    int rv = 1;
    amxc_string_t* service_file = NULL;
    struct dirent* dp = NULL;
    DIR* dfd = NULL;

    amxc_var_set_type(ret, AMXC_VAR_ID_HTABLE);
    amxc_string_new(&service_file, 0);

    dfd = opendir(SERVICES_PATH);
    when_null_trace(dfd, exit, ERROR, "The directory " SERVICES_PATH " doesn't exist.");

    while((dp = readdir(dfd)) != NULL) {
        variant_json_t* json_reader = NULL;
        amxc_var_t* service = NULL;
        int fd = -1;
        int read_length = 0;

        amxc_string_setf(service_file, "%s/%s", SERVICES_PATH, dp->d_name);
        if(amxc_string_search(service_file, "json", 0) == -1) {
            continue;
        }

        amxj_reader_new(&json_reader);
        when_null_trace(json_reader, exit, ERROR, "JSON reader couldn't be initialised.");

        fd = open(amxc_string_get(service_file, 0), O_RDONLY, 0644);
        if(fd == -1) {
            amxj_reader_delete(&json_reader);
            continue;
        }

        read_length = amxj_read(json_reader, fd);
        while(read_length > 0) {
            read_length = amxj_read(json_reader, fd);
        }
        close(fd);

        service = amxj_reader_result(json_reader);
        if(service == NULL) {
            amxj_reader_delete(&json_reader);
            continue;
        }

        parse_service_file(service, ret);
        amxj_reader_delete(&json_reader);
        amxc_var_delete(&service);
    }

    rv = 0;
exit:
    if(dfd != NULL) {
        closedir(dfd);
    }
    amxc_string_delete(&service_file);
    SAH_TRACEZ_OUT(ME);
    return rv;
}