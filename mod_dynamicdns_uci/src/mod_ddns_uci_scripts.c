/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#define _GNU_SOURCE
#include <dlfcn.h>
#include <stdio.h>
#undef _GNU_SOURCE
#include <sys/stat.h>
#include <stdlib.h>
#include <string.h>
#include <libgen.h>
#include <limits.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>

#include <amxb/amxb.h>

#include "ddns_error_codes.h"
#include "mod_ddns_common.h"
#include "mod_ddns_uci_utils.h"
#include "mod_ddns_uci_scripts.h"

#define ME "ddns-uci"

#ifdef DDNS_UNIT_TEST
    #define DDNS_LOG_PATH "./test_%s.log"
#else
    #define DDNS_LOG_PATH "/tmp/log/ddns/%s.log"
#endif
#define DDNS_SERVICE_PATH "/etc/init.d/ddns"
#define GET_PID_SCRIPT_NAME "mod_dynamicdns_uci_get_ddns_pids.sh"
#define DDNS_IS_RUNNING_SCRIPT_NAME "mod_dynamicdns_uci_ddns_is_running.sh"
#define STRING_END_OF_SCRIPT "Finished script"
#define WAIT_TIMEOUT_IN_MS 1000

amxc_var_t* proc_info = NULL;
static const char* module_path = NULL;

static amxc_var_t* get_list_of_pids(amxc_llist_t* stdout_lines, const char* filter) {
    SAH_TRACEZ_IN(ME);
    int num_of_lines = 0;
    amxc_var_t* ret = NULL;
    amxc_string_t* line = NULL;
    int pid_int = -1;

    when_null(stdout_lines, exit);

    num_of_lines = amxc_llist_size(stdout_lines);
    when_false(num_of_lines > 0, exit);

    amxc_llist_for_each(it, stdout_lines) {
        const char* pid_str = NULL;
        amxc_string_t* pid = NULL;
        int line_length = 0;

        line = amxc_llist_it_get_data(it, amxc_string_t, it);

        if(!STRING_EMPTY(filter)) {
            if(strstr(amxc_string_get(line, 0), filter) == NULL) {
                continue;
            }
        }
        if(amxc_string_search(line, STRING_END_OF_SCRIPT, 0) != -1) {
            continue;
        }

        amxc_string_new(&pid, 0);
        line_length = (int) amxc_string_text_length(line);
        for(int index = 0; index < line_length; index++) {
            char character = line->buffer[index];
            if(character == ' ') {
                break;
            } else if((character < '0') || (character > '9')) {
                amxc_string_delete(&pid);
                break;
            }
            amxc_string_append(pid, &character, 1);
        }
        if(amxc_string_text_length(pid) == 0) {
            amxc_string_delete(&pid);
            continue;
        }

        pid_str = amxc_string_get(pid, 0);
        pid_int = atoi(pid_str);

        if(((pid_int == 0) && (strcmp(amxc_string_get(pid, 0), "0") != 0)) ||
           (pid_int < 0)) {
            amxc_string_delete(&pid);
            continue;
        }

        if(ret == NULL) {
            amxc_var_new(&ret);
            amxc_var_set_type(ret, AMXC_VAR_ID_HTABLE);
        }
        amxc_var_add_key(int32_t, ret, pid_str, pid_int);
        amxc_string_delete(&pid);
    }
exit:
    SAH_TRACEZ_OUT(ME);
    return ret;
}

static amxc_var_t* ddns_pids(void) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    amxc_var_t* ret = NULL;
    amxp_subproc_t* proc = NULL;
    amxc_string_t* string_stdout = NULL;
    amxc_string_t* script_path = NULL;
    amxc_llist_t stdout_lines;
    int32_t fd = -1;
    Dl_info info;

    amxc_string_new(&string_stdout, 0);
    amxc_llist_init(&stdout_lines);
    amxc_string_new(&script_path, 0);

    if((dladdr(ddns_pids, &info)) && (module_path == NULL)) {
        module_path = dirname((char*) info.dli_fname);
    }
    amxc_string_setf(script_path, "%s/scripts/%s", module_path, GET_PID_SCRIPT_NAME);

    when_str_empty_trace(amxc_string_get(script_path, 0), exit, ERROR, "Script path is empty.");
    when_failed_trace(access(amxc_string_get(script_path, 0), F_OK), exit, ERROR, "Script '%s' doesn't exist.", amxc_string_get(script_path, 0));

    rv = amxp_subproc_new(&proc);
    when_failed_trace(rv, exit, ERROR, "Failed to initialize the subprocess.");

    fd = amxp_subproc_open_fd(proc, STDOUT_FILENO);
    when_false_trace(fd > 0, exit, ERROR, "Failed to open file descriptor.");

    rv = amxp_subproc_start(proc, (char*) "sh", amxc_string_get(script_path, 0), NULL);
    when_failed_trace(rv, exit, ERROR, "Failed to run the shell script.");

    do {
        read_from_fd(fd, &string_stdout);
        if(amxc_string_search(string_stdout, STRING_END_OF_SCRIPT, 0) != -1) {
            amxp_subproc_kill(proc, SIGTERM);
            break;
        }
    } while (amxp_subproc_wait(proc, 1) == 1);

    when_str_empty(amxc_string_get(string_stdout, 0), exit);
    amxc_string_split_to_llist(string_stdout, &stdout_lines, '\n');

    ret = get_list_of_pids(&stdout_lines, NULL);

exit:
    amxp_subproc_delete(&proc);
    amxc_string_delete(&string_stdout);
    amxc_string_delete(&script_path);
    amxc_llist_clean(&stdout_lines, amxc_string_list_it_free);
    SAH_TRACEZ_OUT(ME);
    return ret;
}

bool ddns_is_running(const char* section_name) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    bool status = false;
    amxc_var_t* ret = NULL;
    amxp_subproc_t* proc = NULL;
    amxc_string_t* string_stdout = NULL;
    amxc_string_t* script_path = NULL;
    amxc_llist_t stdout_lines;
    int32_t fd = -1;
    Dl_info info;

    amxc_string_new(&string_stdout, 0);
    amxc_llist_init(&stdout_lines);
    amxc_string_new(&script_path, 0);

    when_str_empty(section_name, exit);

    if((dladdr(ddns_is_running, &info)) && (module_path == NULL)) {
        module_path = dirname((char*) info.dli_fname);
    }
    amxc_string_setf(script_path, "%s/scripts/%s", module_path, DDNS_IS_RUNNING_SCRIPT_NAME);

    when_str_empty_trace(amxc_string_get(script_path, 0), exit, ERROR, "Script path is empty.");
    when_failed_trace(access(amxc_string_get(script_path, 0), F_OK), exit, ERROR, "Script '%s' doesn't exist.", amxc_string_get(script_path, 0))

    rv = amxp_subproc_new(&proc);
    when_failed_trace(rv, exit, ERROR, "Failed to initialize the subprocess.");

    fd = amxp_subproc_open_fd(proc, STDOUT_FILENO);
    when_false_trace(fd > 0, exit, ERROR, "Failed to open file descriptor.");

    rv = amxp_subproc_start(proc, (char*) "sh", amxc_string_get(script_path, 0), section_name, NULL);
    when_failed_trace(rv, exit, ERROR, "Failed to run the shell script.");

    do {
        read_from_fd(fd, &string_stdout);
        if(amxc_string_search(string_stdout, STRING_END_OF_SCRIPT, 0) != -1) {
            amxp_subproc_kill(proc, SIGTERM);
            break;
        }
    } while (amxp_subproc_wait(proc, 1) == 1);

    when_str_empty(amxc_string_get(string_stdout, 0), exit);
    amxc_string_split_to_llist(string_stdout, &stdout_lines, '\n');
    when_false(amxc_llist_size(&stdout_lines), exit);
    ret = get_list_of_pids(&stdout_lines, section_name);
    when_null(ret, exit);

    status = true;
exit:
    amxp_subproc_delete(&proc);
    amxc_string_delete(&string_stdout);
    amxc_string_delete(&script_path);
    amxc_llist_clean(&stdout_lines, amxc_string_list_it_free);
    amxc_var_delete(&ret);
    SAH_TRACEZ_OUT(ME);
    return status;
}

static amxc_string_t* get_log_filename(const char* section_name) {
    amxc_string_t* log_file = NULL;

    when_str_empty(section_name, exit);

    amxc_string_new(&log_file, 0);
    amxc_string_setf(log_file, DDNS_LOG_PATH, section_name);

exit:
    return log_file;
}

static amxc_string_t* get_log(const char* section_name) {
    amxc_string_t* string_stdout = NULL;
    amxc_string_t* log_file = NULL;

    amxc_string_new(&string_stdout, 0);

    when_str_empty(section_name, exit);

    log_file = get_log_filename(section_name);
    read_from_file(amxc_string_get(log_file, 0), &string_stdout);

exit:
    amxc_string_delete(&log_file);
    return string_stdout;
}

static amxc_llist_t* get_lines_from_log(const char* section_name) {
    amxc_string_t* string_stdout = NULL;
    amxc_string_t* log_file = NULL;
    amxc_llist_t* stdout_lines = NULL;
    int num_of_lines = 0;

    amxc_llist_new(&stdout_lines);

    when_str_empty(section_name, exit);

    string_stdout = get_log(section_name);
    when_str_empty(amxc_string_get(string_stdout, 0), exit);
    amxc_string_split_to_llist(string_stdout, stdout_lines, '\n');
    num_of_lines = amxc_llist_size(stdout_lines);
    when_false(num_of_lines <= 0, exit);
    amxc_llist_delete(&stdout_lines, amxc_string_list_it_free);
    stdout_lines = NULL;

exit:
    amxc_string_delete(&string_stdout);
    amxc_string_delete(&log_file);
    return stdout_lines;
}

static void delete_log_file(const char* section_name) {
    SAH_TRACEZ_IN(ME);
    amxc_string_t* log_file = NULL;

    log_file = get_log_filename(section_name);
    when_null(log_file, exit);
    when_str_empty(amxc_string_get(log_file, 0), exit);

    if(access(amxc_string_get(log_file, 0), F_OK) == 0) {
        remove(amxc_string_get(log_file, 0));
    }

exit:
    amxc_string_delete(&log_file);
    SAH_TRACEZ_OUT(ME);
}

ddns_error_t ddns_get_status(const char* section_name) {
    SAH_TRACEZ_IN(ME);
    bool status = false;
    ddns_error_t error = ddns_error_config;
    amxc_llist_t* stdout_lines = NULL;
    amxc_string_t* line = NULL;
    amxc_var_t* section = NULL;
    amxc_var_t* values = NULL;
    int num_of_lines = 0;

    when_str_empty(section_name, exit);

    section = uci_get_ddns_section(section_name, NULL);
    when_null(section, exit);
    values = GET_ARG(section, "values");
    when_null(values, exit);

    if(!GET_BOOL(values, "enabled")) {
        error = ddns_error_no;
        goto exit;
    }

    error = ddns_error_dns;
    status = ddns_is_running(section_name);
    when_false(status, exit);

    error = ddns_error_config;
    stdout_lines = get_lines_from_log(section_name);
    when_null_trace(stdout_lines, exit, ERROR, "Failed to get the lines from the log.");
    num_of_lines = amxc_llist_size(stdout_lines);
    when_false(num_of_lines > 0, exit);
    line = amxc_string_get_from_llist(stdout_lines, num_of_lines - 1);
    when_null(line, exit);

    if(((amxc_string_search(line, "WARN", 0) != -1) && (amxc_string_search(line, "retry", 0) != -1)) &&
       (amxc_string_search(line, "Get local IP via", 0) != -1)) {
        //Interface is wrong
        error = ddns_error_config;
        SAH_TRACEZ_ERROR(ME, "Failed to get the local IP from the interface.");
        goto exit;
    } else if(((amxc_string_search(line, "WARN", 0) != -1) && (amxc_string_search(line, "retry", 0) != -1)) &&
              (amxc_string_search(line, "Transfer failed", 0) != -1)) {
        //Connection error
        error = ddns_error_conn;
        goto exit;
    } else if(((amxc_string_search(line, "WARN", 0) != -1) && (amxc_string_search(line, "retry", 0) != -1)) ||
              (amxc_string_search(line, "ERROR", 0) != -1) ||
              (amxc_string_search(line, "connection timed out", 0) != -1)) {
        //Connection timed out
        error = ddns_error_timeout;
        SAH_TRACEZ_ERROR(ME, "Connection timeout.");
        goto exit;
    } else if(amxc_string_search(line, "(Check Interval)", 0) != -1) {
        line = amxc_string_get_from_llist(stdout_lines, num_of_lines - 4);
        if(line != NULL) {
            //Error with password, username or domain
            if(amxc_string_search(line, "ERROR", 0) != -1) {
                if(amxc_string_search(line, "has not changed", 0) == -1) {
                    error = ddns_error_auth;
                    SAH_TRACEZ_ERROR(ME, "Password, username or domain is wrong.");
                    goto exit;
                } else {
                    error = ddns_error_no;
                    goto exit;
                }
            } else if(amxc_string_search(line, "Access denied", 0) != -1) {
                error = ddns_error_auth;
                SAH_TRACEZ_ERROR(ME, "Failed to authenticate with user and password.");
                goto exit;
            } else {
                line = amxc_string_get_from_llist(stdout_lines, num_of_lines - 2);
                if(amxc_string_search(line, "ERROR", 0) != -1) {
                    error = ddns_error_auth;
                    SAH_TRACEZ_ERROR(ME, "Authentication error.");
                    goto exit;
                } else {
                    error = ddns_error_no;
                    goto exit;
                }
            }
        }
    } else if(amxc_string_search(line, "/usr/bin/nslookup", 0) != -1) {
        //When there is an error
        line = amxc_string_get_from_llist(stdout_lines, num_of_lines - 3);
        if(line != NULL) {
            if(amxc_string_search(line, "connection timed out", 0) != -1) {
                error = ddns_error_timeout;
                SAH_TRACEZ_ERROR(ME, "Failed to connect.");
                goto exit;
            }
        }
    }

    error = ddns_error_updating;

exit:
    amxc_llist_delete(&stdout_lines, amxc_string_list_it_free);
    amxc_var_delete(&section);
    SAH_TRACEZ_OUT(ME);
    return error;
}

static void stop_process(amxp_subproc_t* proc_ddns) {
    SAH_TRACEZ_IN(ME);
    int ret_value = -1;

    amxp_subproc_kill(proc_ddns, SIGTERM);
    ret_value = amxp_subproc_wait(proc_ddns, 10000);
    if(ret_value == 1) {
        amxp_subproc_kill(proc_ddns, SIGKILL);
    }
    amxp_subproc_delete(&proc_ddns);
    SAH_TRACEZ_OUT(ME);
}

int ddns_stop_section(const char* section_name) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    bool is_running = false;
    amxp_subproc_t* proc_ddns = NULL;
    amxp_subproc_t* allocated_proc = NULL;
    amxc_var_t* section_var = NULL;
    const amxc_htable_t* ht_proc_info = NULL;

    when_str_empty(section_name, exit);

    section_var = GET_ARG(proc_info, section_name);
    when_null(section_var, leftover);
    allocated_proc = amxp_subproc_find(GET_INT32(section_var, NULL));
    when_null(allocated_proc, leftover);
    stop_process(allocated_proc);
    amxc_var_delete(&section_var);
    ht_proc_info = amxc_var_constcast(amxc_htable_t, proc_info);
    if(amxc_htable_size(ht_proc_info) == 0) {
        amxc_var_delete(&proc_info);
        proc_info = NULL;
    }
    rv = 0;

leftover:
    is_running = ddns_is_running(section_name);
    when_false(is_running, exit);

    rv = amxp_subproc_new(&proc_ddns);
    when_failed_trace(rv, exit, ERROR, "Failed to initialize the subprocess.");

    rv = amxp_subproc_start_wait(proc_ddns, WAIT_TIMEOUT_IN_MS, (char*) "sh", "/usr/lib/ddns/dynamic_dns_updater.sh", "-v", "0", "-S", section_name, "--", "stop", NULL);
    when_failed_trace(rv, exit, ERROR, "Failed to stop the service with name %s", section_name);

    rv = amxp_subproc_get_exitstatus(proc_ddns);
exit:
    amxp_subproc_delete(&proc_ddns);
    delete_log_file(section_name);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

int ddns_start_section(const char* section_name) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    int ret_value = -1;
    int pid = 0;
    amxp_subproc_t* proc_ddns = NULL;

    when_str_empty(section_name, exit);

    ddns_stop_section(section_name);
    delete_log_file(section_name);

    ret_value = amxp_subproc_new(&proc_ddns);
    when_failed_trace(ret_value, exit, ERROR, "Failed to initialize the subprocess.");

    ret_value = amxp_subproc_start(proc_ddns, (char*) "sh", "/usr/lib/ddns/dynamic_dns_updater.sh", "-v", "0", "-S", section_name, "--", "start", NULL);
    if(ret_value != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to start the service with section name - %s", section_name);
        amxp_subproc_delete(&proc_ddns);
        goto exit;
    }

    pid = (int) amxp_subproc_get_pid(proc_ddns);
    when_false_trace(pid > 0, exit, ERROR, "Failed to get PID of the process.")

    if(proc_info == NULL) {
        amxc_var_new(&proc_info);
        amxc_var_set_type(proc_info, AMXC_VAR_ID_HTABLE);
    }
    amxc_var_add_key(int32_t, proc_info, section_name, pid);

    rv = 0;
exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

int ddns_disable_service(void) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    amxp_subproc_t* proc_ddns = NULL;
    amxc_var_t* pids = NULL;
    const amxc_htable_t* ht_pids = NULL;

    rv = amxp_subproc_new(&proc_ddns);
    when_failed_trace(rv, exit, ERROR, "Failed to initialize the subprocess.");

    rv = amxp_subproc_start_wait(proc_ddns, WAIT_TIMEOUT_IN_MS, (char*) DDNS_SERVICE_PATH, "stop", NULL);
    when_failed_trace(rv, exit, ERROR, "Failed to stop the service");

    rv = amxp_subproc_start_wait(proc_ddns, WAIT_TIMEOUT_IN_MS, (char*) DDNS_SERVICE_PATH, "disable", NULL);
    when_failed_trace(rv, exit, ERROR, "Failed to disable the service");

    pids = ddns_pids();
    ht_pids = amxc_var_constcast(amxc_htable_t, pids);
    amxc_htable_for_each(it, ht_pids) {
        const char* key = amxc_htable_it_get_key(it);
        amxp_subproc_start_wait(proc_ddns, WAIT_TIMEOUT_IN_MS, (char*) "kill", "-9", key, NULL);
    }

    amxc_var_delete(&proc_info);
    proc_info = NULL;

    rv = 0;
exit:
    amxp_subproc_delete(&proc_ddns);
    amxc_var_delete(&pids);
    SAH_TRACEZ_OUT(ME);
    return rv;
}
