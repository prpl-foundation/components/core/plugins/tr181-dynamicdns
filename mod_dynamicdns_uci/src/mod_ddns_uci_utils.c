/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <arpa/inet.h>
#include <math.h>
#include <sys/stat.h>

#include <amxc/amxc_variant.h>
#include <amxc/amxc_macros.h>
#include <amxc/amxc_lqueue.h>
#include <amxp/amxp_signal.h>
#include <amxd/amxd_types.h>
#include <amxp/amxp_slot.h>
#include <amxb/amxb_be_mngr.h>
#include <amxb/amxb.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "ddns.h"
#include "mod_ddns_common.h"
#include "mod_ddns_uci_commit.h"
#include "mod_ddns_uci_utils.h"

#define ME "ddns-uci"

int create_ip_file(const char* ip) {
    SAH_TRACEZ_IN(ME);
    int retval = -1;
    FILE* fd = NULL;

    when_str_empty(DDNS_IP_TMP, exit);
    if(access(DDNS_IP_TMP, F_OK) == 0) {
        remove(DDNS_IP_TMP);
    }

    when_str_empty(ip, exit);
    fd = fopen(DDNS_IP_TMP, "w");
    when_null(fd, exit);
    fprintf(fd, "%s", ip);
    fclose(fd);
    chmod(DDNS_IP_TMP, strtol("0777", 0, 8));

    retval = 0;

exit:
    SAH_TRACEZ_OUT(ME);
    return retval;
}

/**
 * @brief Perform uci call specified by the method and follow it up with a 'delayed' commit.
 * @param method uci method; get, set, delete, ...
 * @param args variant containing the UCI message
 * @param commit UCI commit will be done if set to true
 */
static int uci_call(const char* method, amxc_var_t* args, amxc_var_t* ret, bool commit) {
    SAH_TRACEZ_IN(ME);
    int rv = 0;
    amxb_bus_ctx_t* ctx = amxb_be_who_has("uci.");

    when_null_trace(ctx, exit, ERROR, "Bus context is NULL");
    rv = amxb_call(ctx, "uci.", method, args, ret, 3);
    when_failed_trace(rv, exit, ERROR, "failed to call UCI %s, function returned %d", method, rv);

    if(commit) {
        uci_commit();
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

/**
 * @brief performs a ubus call uci get '{"config":"ddns","type":"service"}'
 * @return amxc variant pointer to an amxc list containing the service(s)
 */
amxc_var_t* uci_get_ddns_all(void) {
    SAH_TRACEZ_IN(ME);
    amxc_var_t uci_args;
    amxc_var_t* uci_ret = NULL;
    amxc_var_t* tmp = NULL;
    amxc_var_t* ret = NULL;
    amxc_var_t* values = NULL;

    amxc_var_init(&uci_args);
    amxc_var_set_type(&uci_args, AMXC_VAR_ID_HTABLE);
    amxc_var_new(&uci_ret);
    amxc_var_new(&ret);

    amxc_var_add_key(cstring_t, &uci_args, "config", "ddns");
    amxc_var_add_key(cstring_t, &uci_args, "type", "service");

    uci_call("get", &uci_args, uci_ret, false);

    if(amxc_var_type_of(uci_ret) == AMXC_VAR_ID_LIST) {
        tmp = GET_ARG(uci_ret, "0");
        amxc_var_move(ret, tmp);
    } else {
        amxc_var_move(ret, uci_ret);
    }

    amxc_var_clean(&uci_args);
    amxc_var_delete(&uci_ret);

    values = GET_ARG(ret, "values");
    if(values == NULL) {
        amxc_var_delete(&ret);
        ret = NULL;
    }

    SAH_TRACEZ_OUT(ME);
    return ret;
}

/**
 * @brief performs a ubus call uci get '{"config":"ddns","type":"service","section":section}'
 * @param section service name
 * @param option optional, when all options are wanted this should be set to NULL
 * @return amxc variant pointer to an amxc list containing a single element with a htable of the returned option(s)
 */
amxc_var_t* uci_get_ddns_section(const char* section_name, const char* option) {
    SAH_TRACEZ_IN(ME);
    amxc_var_t uci_args;
    amxc_var_t* uci_ret = NULL;
    amxc_var_t* tmp = NULL;
    amxc_var_t* ret = NULL;
    amxc_var_t* values = NULL;

    amxc_var_init(&uci_args);
    amxc_var_set_type(&uci_args, AMXC_VAR_ID_HTABLE);
    amxc_var_new(&uci_ret);
    amxc_var_new(&ret);

    amxc_var_add_key(cstring_t, &uci_args, "config", "ddns");
    amxc_var_add_key(cstring_t, &uci_args, "type", "service");
    amxc_var_add_key(cstring_t, &uci_args, "section", section_name);
    amxc_var_add_key(cstring_t, &uci_args, "option", option);

    uci_call("get", &uci_args, uci_ret, false);

    if(amxc_var_type_of(uci_ret) == AMXC_VAR_ID_LIST) {
        tmp = GET_ARG(uci_ret, "0");
        amxc_var_move(ret, tmp);
    } else {
        amxc_var_move(ret, uci_ret);
    }

    amxc_var_clean(&uci_args);
    amxc_var_delete(&uci_ret);

    values = GET_ARG(ret, "values");
    if(values == NULL) {
        amxc_var_delete(&ret);
        ret = NULL;
    }

    SAH_TRACEZ_OUT(ME);
    return ret;
}

static int init_uci_args(const char* section_name, amxc_var_t* uci_args, uci_action_t uci_action) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;

    if(uci_args == NULL) {
        amxc_var_init(uci_args);
    }
    amxc_var_set_type(uci_args, AMXC_VAR_ID_HTABLE);

    when_str_empty_trace(section_name, exit, ERROR, "Can NOT extract sectionName from parameters");

    amxc_var_add_key(cstring_t, uci_args, "config", "ddns");
    if(uci_action != UCI_RENAME) {
        if(strcmp(section_name, "global") == 0) {
            amxc_var_add_key(cstring_t, uci_args, "type", "ddns");
        } else {
            amxc_var_add_key(cstring_t, uci_args, "type", "service");
        }
    }
    if(uci_action == UCI_ADD) {
        amxc_var_add_key(cstring_t, uci_args, "name", section_name);
    } else {
        amxc_var_add_key(cstring_t, uci_args, "section", section_name);
    }

    rv = 0;
exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

/**
 * @brief Set the UCI server.
 * @param params Input parameters.
 * @param uci_values UCI values
 * @return 0 if the function runs with success.
 */
int uci_set_service(amxc_var_t* params, amxc_var_t* uci_values) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    amxc_var_t* tmp_var = NULL;
    amxc_string_t* tmp_str = NULL;
    int check_interval = 0;

    amxc_string_new(&tmp_str, 0);

    when_null_trace(params, exit, ERROR, "The parameters are NULL.");
    when_null_trace(uci_values, exit, ERROR, "The UCI values object is empty.");

    tmp_var = GET_ARG(params, SERVER_SERVICE);
    when_null_trace(tmp_var, exit, ERROR, "%s was not found in the parameters.", SERVER_SERVICE);
    when_str_empty_trace(GET_CHAR(tmp_var, NULL), exit, ERROR, "%s is empty.", SERVER_SERVICE);
    if(strstr(GET_CHAR(tmp_var, NULL), "custom") != NULL) {
        tmp_var = GET_ARG(params, SERVER_ADDRESS);
        when_null_trace(tmp_var, exit, ERROR, "%s was not found in the parameters.", SERVER_ADDRESS);
        when_str_empty_trace(GET_CHAR(tmp_var, NULL), exit, ERROR, "%s is empty.", SERVER_ADDRESS);
        amxc_var_add_key(cstring_t, uci_values, "update_url", GET_CHAR(tmp_var, NULL));
    } else {
        amxc_var_add_key(cstring_t, uci_values, "service_name", GET_CHAR(tmp_var, NULL));
    }

    tmp_var = GET_ARG(params, SERVER_PROTOCOL);
    when_null_trace(tmp_var, exit, ERROR, "%s was not found in the parameters.", SERVER_PROTOCOL);
    when_str_empty_trace(GET_CHAR(tmp_var, NULL), exit, ERROR, "%s is empty.", SERVER_PROTOCOL);
    if(strcmp(GET_CHAR(tmp_var, NULL), "HTTPS") == 0) {
        amxc_var_add_key(cstring_t, uci_values, "use_https", "1");
    } else {
        amxc_var_add_key(cstring_t, uci_values, "use_https", "0");
    }

    tmp_var = GET_ARG(params, SERVER_CHECK_INTERVAL);
    when_null_trace(tmp_var, exit, ERROR, "%s was not found in the parameters.", SERVER_CHECK_INTERVAL);
    check_interval = (int) ceil((double) GET_UINT32(tmp_var, NULL) / (double) 60);
    amxc_string_setf(tmp_str, "%d", check_interval);
    amxc_var_add_key(cstring_t, uci_values, "check_interval", amxc_string_get(tmp_str, 0));
    amxc_var_add_key(cstring_t, uci_values, "check_unit", "minutes");
    //Avoid forcing the update setting it to force the update in more than 50 years.
    //Requirement from CDROUTER to not force the update.
    amxc_var_add_key(cstring_t, uci_values, "force_interval", "20000");
    amxc_var_add_key(cstring_t, uci_values, "force_unit", "days");

    tmp_var = GET_ARG(params, SERVER_RETRY_INTERVAL);
    when_null_trace(tmp_var, exit, ERROR, "%s was not found in the parameters.", SERVER_RETRY_INTERVAL);
    amxc_string_setf(tmp_str, "%d", GET_UINT32(tmp_var, NULL));
    amxc_var_add_key(cstring_t, uci_values, "retry_interval", amxc_string_get(tmp_str, 0));
    amxc_var_add_key(cstring_t, uci_values, "retry_unit", "seconds");

    tmp_var = GET_ARG(params, SERVER_MAX_RETRIES);
    when_null_trace(tmp_var, exit, ERROR, "%s was not found in the parameters.", SERVER_MAX_RETRIES);
    amxc_string_setf(tmp_str, "%d", GET_UINT32(tmp_var, NULL));
    amxc_var_add_key(cstring_t, uci_values, "retry_max_count", amxc_string_get(tmp_str, 0));

    rv = 0;
exit:
    amxc_string_delete(&tmp_str);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

/**
 * @brief Add a section to UCI.
 * @param section_name service name
 * @param params Parameters
 * @param domain domain url
 * @param server ddns service
 * @param interface interface
 * @param user username option
 * @param passwd password option
 * @param enabled enable option
 * @return 0 if the function runs with success.
 */
int uci_add_ddns(const char* section_name, amxc_var_t* params, const char* domain,
                 const char* interface, const char* user, const char* passwd, int enabled) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    int ret_value = -1;
    amxc_var_t* tmp_var = NULL;
    amxc_var_t* values = NULL;
    amxc_var_t uci_args;
    amxc_var_t uci_ret;
    amxc_string_t str_enable;
    const char* ip_source = NULL;

    amxc_var_init(&uci_args);
    amxc_var_init(&uci_ret);
    amxc_string_init(&str_enable, 0);

    when_failed_trace(init_uci_args(section_name, &uci_args, UCI_ADD), exit, ERROR, "Failed to initialize the UCI arguments.");

    values = amxc_var_add_key(amxc_htable_t, &uci_args, "values", NULL);

    ret_value = uci_set_service(params, values);
    when_failed(ret_value, exit);

    tmp_var = GET_ARG(params, CLIENT_IP);
    if(tmp_var != NULL) {
        create_ip_file(GET_CHAR(tmp_var, NULL));
    }

    tmp_var = GET_ARG(params, MOD_IP_SOURCE);
    when_null_trace(tmp_var, exit, ERROR, "ip_source is an empty string or NULL.");
    ip_source = GET_CHAR(tmp_var, NULL);

    amxc_var_add_key(cstring_t, values, "ip_source", ip_source);
    if(strstr(ip_source, "interface") != NULL) {
        when_str_empty_trace(interface, exit, ERROR, "interface is an empty string or NULL.");
    } else if(strstr(ip_source, "script") != NULL) {
        when_str_empty_trace(GET_CHAR(tmp_var, CLIENT_IP), exit, ERROR, "IP is an empty string or NULL.");
    }

    if(!str_empty(interface)) {
        amxc_var_add_key(cstring_t, values, "ip_interface", interface);
    }

    amxc_var_add_key(cstring_t, values, "ip_script", "/usr/lib/amx/scripts/ddns_scripts_echo_ip.sh");

    when_str_empty(domain, exit);
    amxc_var_add_key(cstring_t, values, "domain", domain);
    amxc_var_add_key(cstring_t, values, "lookup_host", domain);

    when_str_empty(user, exit);
    amxc_var_add_key(cstring_t, values, "username", user);

    when_str_empty(passwd, exit);
    amxc_var_add_key(cstring_t, values, "password", passwd);

    tmp_var = GET_ARG(params, CLIENT_DNS);
    if(tmp_var != NULL) {
        amxc_var_add_key(cstring_t, values, "dns_server", GET_CHAR(tmp_var, NULL));
    }

    tmp_var = GET_ARG(params, MOD_FORCE_IPVERSION);
    if(tmp_var != NULL) {
        amxc_var_add_key(cstring_t, values, "force_ipversion", GET_CHAR(tmp_var, NULL));
    }

    tmp_var = GET_ARG(params, MOD_USE_IPV6);
    if(tmp_var != NULL) {
        amxc_var_add_key(cstring_t, values, "use_ipv6", GET_CHAR(tmp_var, NULL));
    }

    amxc_string_setf(&str_enable, "%d", enabled ? 1 : 0);
    amxc_var_add_key(cstring_t, values, "enabled", amxc_string_get(&str_enable, 0));

    rv = uci_call("add", &uci_args, &uci_ret, true);
    when_failed_trace(rv, exit, ERROR, "Fail to make the UCI call.");

exit:
    amxc_string_clean(&str_enable);
    amxc_var_clean(&uci_args);
    amxc_var_clean(&uci_ret);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

/**
 * @brief Edit a section to UCI.
 * @param section_name service name
 * @param new_values new values to be changed on the existing section.
 * @return 0 if the function runs with success.
 */
int uci_set_ddns(const char* section_name, amxc_var_t* new_values) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    amxc_var_t* values = NULL;
    amxc_var_t uci_args;
    amxc_var_t uci_ret;
    const amxc_htable_t* ht_values = amxc_var_constcast(amxc_htable_t, new_values);
    bool changed = false;

    amxc_var_init(&uci_args);
    amxc_var_init(&uci_ret);

    when_null_trace(ht_values, exit, ERROR, "The table with values to be added is empty.")

    when_failed(init_uci_args(section_name, &uci_args, UCI_SET), exit);

    values = amxc_var_add_key(amxc_htable_t, &uci_args, "values", NULL);
    amxc_htable_for_each(it, ht_values) {
        const char* key = amxc_htable_it_get_key(it);
        const amxc_var_t* value = amxc_htable_it_get_data(it, amxc_var_t, hit);
        amxc_var_add_key(cstring_t, values, key, GET_CHAR(value, NULL));
        changed = true;
    }

    if(changed) {
        rv = uci_call("set", &uci_args, &uci_ret, true);
        when_failed_trace(rv, exit, ERROR, "Failed to set the DDNS config on UCI.");
    }

    rv = 0;

exit:
    amxc_var_clean(&uci_args);
    amxc_var_clean(&uci_ret);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

/**
 * @brief Delete the section from UCI.
 * @param section_name service name
 * @return 0 if the function runs with success.
 */
int uci_delete_ddns(const char* section_name) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    amxc_var_t uci_args;
    amxc_var_t uci_ret;

    amxc_var_init(&uci_args);
    amxc_var_init(&uci_ret);

    when_failed(init_uci_args(section_name, &uci_args, UCI_DELETE), exit);

    rv = uci_call("delete", &uci_args, &uci_ret, true);
    when_failed_trace(rv, exit, ERROR, "Failed to delete the DDNS section on UCI.");

exit:
    amxc_var_clean(&uci_args);
    amxc_var_clean(&uci_ret);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

/**
 * @brief Delete an option from a section from UCI.
 * @param section_name service name
 * @param option option to be deleted
 * @return 0 if the function runs with success.
 */
int uci_delete_ddns_option(const char* section_name, const char* option) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    amxc_var_t uci_args;
    amxc_var_t uci_ret;

    amxc_var_init(&uci_args);
    amxc_var_init(&uci_ret);

    when_str_empty(option, exit);

    when_failed(init_uci_args(section_name, &uci_args, UCI_DELETE), exit);
    amxc_var_add_key(cstring_t, &uci_args, "option", option);

    rv = uci_call("delete", &uci_args, &uci_ret, true);
    when_failed_trace(rv, exit, ERROR, "Failed to delete the DDNS option on UCI.");

exit:
    amxc_var_clean(&uci_args);
    amxc_var_clean(&uci_ret);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

int uci_rename_ddns(const char* section_name, const char* new_section_name) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    amxc_var_t uci_args;
    amxc_var_t uci_ret;

    amxc_var_init(&uci_args);
    amxc_var_init(&uci_ret);

    when_failed(init_uci_args(section_name, &uci_args, UCI_RENAME), exit);
    when_str_empty_trace(new_section_name, exit, ERROR, "New section name is NULL.");

    amxc_var_add_key(cstring_t, &uci_args, "name", new_section_name);

    rv = uci_call("rename", &uci_args, &uci_ret, true);
    when_failed_trace(rv, exit, ERROR, "Failed to rename the DDNS section on UCI.");

    rv = 0;
exit:
    amxc_var_clean(&uci_args);
    amxc_var_clean(&uci_ret);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

/**
 * @brief Edit the global section on UCI.
 * @param dateformat Dateformat.
 * @param loglines Loglines.
 * @param privateip Private IP.
 * @return 0 if the function runs with success.
 */
int uci_set_ddns_global(const char* dateformat, const char* loglines, const char* privateip) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    amxc_var_t* values = NULL;
    amxc_var_t uci_args;
    amxc_var_t uci_ret;
    bool changed = false;

    amxc_var_init(&uci_args);
    amxc_var_init(&uci_ret);

    when_failed(init_uci_args("global", &uci_args, UCI_SET), exit);

    values = amxc_var_add_key(amxc_htable_t, &uci_args, "values", NULL);
    if(!STRING_EMPTY(dateformat)) {
        amxc_var_add_key(cstring_t, values, "ddns_dateformat", dateformat);
        changed = true;
    }
    if(!STRING_EMPTY(loglines)) {
        amxc_var_add_key(cstring_t, values, "ddns_loglines", loglines);
        changed = true;
    }
    if(!STRING_EMPTY(privateip)) {
        amxc_var_add_key(cstring_t, values, "upd_privateip", privateip);
        changed = true;
    }

    if(changed) {
        rv = uci_call("set", &uci_args, &uci_ret, true);
        when_failed_trace(rv, exit, ERROR, "Failed to set the DDNS config on UCI.");
    }

    rv = 0;

exit:
    amxc_var_clean(&uci_args);
    amxc_var_clean(&uci_ret);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

/**
 * @brief Clean all the UCI entries to match the datamodel.
 * @return 0 if the function runs with success.
 */
int uci_clean_ddns_uci_entries(void) {
    SAH_TRACEZ_IN(ME);
    int rv = 0;
    amxc_var_t* ddns_get_result = NULL;
    amxc_var_t* ddns_services = NULL;
    const amxc_htable_t* ht_values = NULL;

    ddns_get_result = uci_get_ddns_all();
    when_null(ddns_get_result, exit);

    ddns_services = GET_ARG(ddns_get_result, "values");
    when_null(ddns_services, exit);

    ht_values = amxc_var_constcast(amxc_htable_t, ddns_services);
    when_null(ht_values, exit);

    amxc_htable_for_each(it, ht_values) {
        const char* section_name = (cstring_t) amxc_htable_it_get_key(it);
        rv = uci_delete_ddns(section_name);
    }
    when_failed(rv, exit);

    rv = 0;
exit:
    amxc_var_delete(&ddns_get_result);
    SAH_TRACEZ_OUT(ME);
    return rv;
}