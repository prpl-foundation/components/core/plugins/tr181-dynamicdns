/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <regex.h>
#include <fcntl.h>
#include <yajl/yajl_gen.h>
#include <dirent.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxj/amxj_variant.h>
#include <amxd/amxd_dm.h>

#include <amxb/amxb.h>

#include "mod_ddns_common.h"

#define ME "ddns-uci"

#define STDOUT_BUFFER_SIZE 2282

/**
   @brief
   Read the information from the file and save to a string.

   @param[in] fname file name.
   @param[out] string string containing the information of the file appended.
 */
void read_from_file(const char* fname, amxc_string_t** string) {
    FILE* p_file = NULL;
    cstring_t buffer = NULL;
    long length = 0;
    size_t bytes_read = 0;
    int rv = -1;

    when_str_empty_trace(fname, exit, ERROR, "Filename is empty.");
    when_false(access(fname, F_OK) == 0, exit);

    p_file = fopen(fname, "rb");
    when_null(p_file, exit);

    fseek(p_file, 0, SEEK_END);
    length = ftell(p_file);
    when_false(length != 0, close_file);
    fseek(p_file, 0, SEEK_SET);
    when_false(length > 0, close_file);

    length++;
    buffer = calloc(1, length);
    when_null(buffer, close_file);

    bytes_read = fread(buffer, 1, length, p_file);
    when_false(bytes_read > 0, clean);

    //As the string is not necessarily initialized, it needs to be allocated by force
    amxc_string_delete(string);
    amxc_string_new(string, 0);
    rv = amxc_string_push_buffer(*string, buffer, length);
    when_true(rv == 0, close_file);

clean:
    free(buffer);
close_file:
    buffer = NULL;
    fclose(p_file);
exit:
    return;
}

/**
   @brief
   Read the information from the file descriptor and save to a string.

   @param[in] fd file descriptor.
   @param[out] string string containing the information of the fd appended.
 */
void read_from_fd(const int fd, amxc_string_t** string) {
    char buffer[STDOUT_BUFFER_SIZE];
    ssize_t bytes = 0;

    when_null_trace(string, exit, ERROR, "Pointer is NULL.");
    when_null_trace(*string, exit, ERROR, "Output string is not initialised.");
    when_false_trace(fd > 0, exit, ERROR, "File descriptor is not bigger than 0.");

    bytes = read(fd, buffer, STDOUT_BUFFER_SIZE);
    when_false(bytes > 0, exit);

    buffer[bytes] = 0;
    amxc_string_append(*string, buffer, bytes);

exit:
    return;
}

amxc_var_t* read_json_from_file(const char* fname) {
    int fd = -1;
    variant_json_t* reader = NULL;
    amxc_var_t* data = NULL;
    // create a json reader
    when_failed_trace(amxj_reader_new(&reader), exit, ERROR, "Failed to create json file reader");

    // open the json file
    fd = open(fname, O_RDONLY);
    if(fd == -1) {
        SAH_TRACEZ_ERROR(ME, "File open file %s - error 0x%8.8X\n", fname, errno);
        goto exit;
    }

    // read the json file and parse the json text
    while(amxj_read(reader, fd) > 0) {
    }

    // get the variant
    data = amxj_reader_result(reader);
    if(data == NULL) {
        SAH_TRACEZ_ERROR(ME, "Invalid JSON in file %s", fname);
    }

    close(fd);
exit:
    amxj_reader_delete(&reader);
    return data;
}

amxc_llist_t* files_from_path(const char* path) {
    DIR* d = NULL;
    struct dirent* dir = NULL;
    amxc_llist_t* directories = NULL;
    int num_of_lines = 0;

    amxc_llist_new(&directories);

    d = opendir(path);
    if(d) {
        while((dir = readdir(d)) != NULL) {
            amxc_llist_add_string(directories, dir->d_name);
        }
        closedir(d);
    }
    num_of_lines = amxc_llist_size(directories);
    if(num_of_lines == 0) {
        amxc_llist_delete(&directories, amxc_string_list_it_free);
        directories = NULL;
    }

    return directories;
}
