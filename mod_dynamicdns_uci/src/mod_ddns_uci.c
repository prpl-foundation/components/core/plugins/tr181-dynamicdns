/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <arpa/inet.h>

#include <amxc/amxc_variant.h>
#include <amxm/amxm.h>
#include <amxc/amxc_macros.h>
#include <amxc/amxc_lqueue.h>
#include <amxp/amxp_signal.h>
#include <amxd/amxd_types.h>
#include <amxp/amxp_slot.h>
#include <amxb/amxb_be_mngr.h>
#include <amxb/amxb.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "ddns.h"
#include "ddns_error_codes.h"
#include "mod_ddns_common.h"
#include "mod_ddns_uci_commit.h"
#include "mod_ddns_uci_utils.h"
#include "mod_ddns_uci_scripts.h"
#include "mod_ddns_uci_services.h"

// module trace zone
#define ME "ddns-uci"

static void build_section_name(amxc_string_t* string, uint32_t client_index, uint32_t host_index) {
    SAH_TRACEZ_IN(ME);
    amxc_string_setf(string, "section_%d_%d", client_index, host_index);
    SAH_TRACEZ_OUT(ME);
}

/**
 * @brief Change the section.
 * @param params Input parameters in the module.
 * @param new_values New values to be added to the section.
 * @param section_name Section name.
 *
 * @return 0 if the function runs with success.
 */
static int change_section(amxc_var_t* params, amxc_var_t* new_values, const char* section_name) {
    int ret_value = -1;

    ret_value = uci_set_service(params, new_values);
    when_failed_trace(ret_value, exit, ERROR, "Failed to add the new service configuration.");

    if(str_empty(GET_CHAR(new_values, "service_name"))) {
        uci_delete_ddns_option(section_name, "service_name");
    }
    if(str_empty(GET_CHAR(new_values, "update_url"))) {
        uci_delete_ddns_option(section_name, "update_url");
    }

exit:
    return ret_value;
}

/**
 * @brief Set the DDNS parameters.
 * @param section_name service name
 * @param params Input parameters in the module.
 * @return 0 if the function runs with success.
 */
static int mod_set_ddns_params(const char* section_name, amxc_var_t* params) {
    SAH_TRACEZ_IN(ME);
    int rv = ddns_status_error_config;
    amxc_var_t* section_value = NULL;
    amxc_var_t* values = NULL;
    amxc_var_t new_values;
    amxc_string_t str_enable;
    const char* section = section_name;
    const char* string_from_param = NULL;
    const char* string_from_section = NULL;
    bool enable = GET_BOOL(params, CLIENT_ENABLE);
    bool section_change = false;
    int ret_value = -1;

    amxc_string_init(&str_enable, 0);
    amxc_var_init(&new_values);
    amxc_var_set_type(&new_values, AMXC_VAR_ID_HTABLE);

    section_value = uci_get_ddns_section(section, NULL);
    when_null_trace(section_value, exit, ERROR, "DDNS configuration doesn't exist.");
    values = GET_ARG(section_value, "values");
    when_null_trace(values, exit, ERROR, "Section values don't exist.");

    string_from_param = GET_CHAR(params, SERVER_SERVICE);
    string_from_section = GET_CHAR(values, "service_name");
    if(GET_ARG(params, SERVER_SERVICE) != NULL) {
        if(str_empty(string_from_section)) {
            if(strstr(string_from_param, "custom") != NULL) {
                section_change = true;
            }
        } else if(strcmp(string_from_section, string_from_param) != 0) {
            section_change = true;
        }
    } else {
        if(!str_empty(string_from_section)) {
            amxc_var_add_key(cstring_t, params, SERVER_SERVICE, string_from_section);
        } else {
            amxc_var_add_key(cstring_t, params, SERVER_SERVICE, "custom");
        }
    }

    string_from_param = GET_CHAR(params, SERVER_ADDRESS);
    string_from_section = GET_CHAR(values, "update_url");
    if(GET_ARG(params, SERVER_ADDRESS) != NULL) {
        if(str_empty(string_from_section)) {
            section_change = true;
        } else if(strcmp(string_from_section, string_from_param) != 0) {
            section_change = true;
        }
    } else {
        if(!str_empty(string_from_section)) {
            amxc_var_add_key(cstring_t, params, SERVER_ADDRESS, string_from_section);
        }
    }

    if(GET_ARG(params, SERVER_PROTOCOL) != NULL) {
        when_str_empty_trace(GET_CHAR(values, "use_https"), exit, ERROR, "Failed to get the use_https from the UCI config.");
        if(((GET_UINT32(values, "use_https") == 1) && (strcmp(GET_CHAR(params, SERVER_PROTOCOL), "HTTP") == 0)) ||
           ((GET_UINT32(values, "use_https") == 0) && (strcmp(GET_CHAR(params, SERVER_PROTOCOL), "HTTPS") == 0))) {
            section_change = true;
        }
    } else {
        if(GET_UINT32(values, "use_https") == 1) {
            amxc_var_add_key(cstring_t, params, SERVER_PROTOCOL, "HTTPS");
        } else {
            amxc_var_add_key(cstring_t, params, SERVER_PROTOCOL, "HTTP");
        }
    }

    if(GET_ARG(params, SERVER_CHECK_INTERVAL) != NULL) {
        if(GET_UINT32(values, "check_interval") != GET_UINT32(params, SERVER_CHECK_INTERVAL)) {
            section_change = true;
        }
    } else {
        amxc_var_add_key(uint32_t, params, SERVER_CHECK_INTERVAL, GET_UINT32(values, "check_interval") * 60);
    }

    if(GET_ARG(params, SERVER_RETRY_INTERVAL) != NULL) {
        if(GET_UINT32(values, "retry_interval") != GET_UINT32(params, SERVER_RETRY_INTERVAL)) {
            section_change = true;
        }
    } else {
        amxc_var_add_key(uint32_t, params, SERVER_RETRY_INTERVAL, GET_UINT32(values, "retry_interval"));
    }

    if(GET_ARG(params, SERVER_MAX_RETRIES) != NULL) {
        if(GET_UINT32(values, "retry_max_count") != GET_UINT32(params, SERVER_MAX_RETRIES)) {
            section_change = true;
        }
    } else {
        amxc_var_add_key(uint32_t, params, SERVER_MAX_RETRIES, GET_UINT32(values, "retry_max_count"));
    }

    if(section_change) {
        ret_value = change_section(params, &new_values, section);
        when_failed(ret_value, exit);
    }

    if(GET_ARG(params, MOD_DOMAIN) != NULL) {
        when_str_empty_trace(GET_CHAR(values, "domain"), exit, ERROR, "Failed to get the domain from the UCI config.");
        when_str_empty_trace(GET_CHAR(params, MOD_DOMAIN), exit, ERROR, "Domain can't be empty, set the right hostname.");
        if(strcmp(GET_CHAR(values, "domain"), GET_CHAR(params, MOD_DOMAIN)) != 0) {
            amxc_var_add_key(cstring_t, &new_values, "domain", GET_CHAR(params, MOD_DOMAIN));
            amxc_var_add_key(cstring_t, &new_values, "lookup_host", GET_CHAR(params, MOD_DOMAIN));
        }
    }
    if(GET_ARG(params, CLIENT_INTERFACE) != NULL) {
        string_from_param = GET_CHAR(params, CLIENT_INTERFACE) == NULL ? "" : GET_CHAR(params, CLIENT_INTERFACE);
        string_from_section = GET_CHAR(values, "ip_interface") == NULL ? "" : GET_CHAR(values, "ip_interface");
        if(strcmp(string_from_section, string_from_param) != 0) {
            amxc_var_add_key(cstring_t, &new_values, "ip_interface", GET_CHAR(params, CLIENT_INTERFACE));
        }
    }
    if(GET_ARG(params, CLIENT_IP) != NULL) {
        create_ip_file(GET_CHAR(params, CLIENT_IP));
    }
    if(GET_ARG(params, MOD_IP_SOURCE) != NULL) {
        when_str_empty_trace(GET_CHAR(values, "ip_source"), exit, ERROR, "Failed to get the ip_source from the UCI config.");
        when_str_empty_trace(GET_CHAR(params, MOD_IP_SOURCE), exit, ERROR, "ip_source can't be empty, set the right interface.");
        if(strcmp(GET_CHAR(values, "ip_source"), GET_CHAR(params, MOD_IP_SOURCE)) != 0) {
            amxc_var_add_key(cstring_t, &new_values, "ip_source", GET_CHAR(params, MOD_IP_SOURCE));
        }
    }
    if(GET_ARG(params, MOD_FORCE_IPVERSION) != NULL) {
        string_from_param = GET_CHAR(params, MOD_FORCE_IPVERSION) == NULL ? "" : GET_CHAR(params, MOD_FORCE_IPVERSION);
        string_from_section = GET_CHAR(values, "force_ipversion") == NULL ? "" : GET_CHAR(values, "force_ipversion");
        if(strcmp(string_from_section, string_from_param) != 0) {
            amxc_var_add_key(cstring_t, &new_values, "force_ipversion", GET_CHAR(params, MOD_FORCE_IPVERSION));
        }
    }
    if(GET_ARG(params, MOD_USE_IPV6) != NULL) {
        string_from_param = GET_CHAR(params, MOD_USE_IPV6) == NULL ? "" : GET_CHAR(params, MOD_USE_IPV6);
        string_from_section = GET_CHAR(values, "use_ipv6") == NULL ? "" : GET_CHAR(values, "use_ipv6");
        if(strcmp(string_from_section, string_from_param) != 0) {
            amxc_var_add_key(cstring_t, &new_values, "use_ipv6", GET_CHAR(params, MOD_USE_IPV6));
        }
    }
    if(GET_ARG(params, CLIENT_DNS) != NULL) {
        string_from_param = GET_CHAR(params, CLIENT_DNS) == NULL ? "" : GET_CHAR(params, CLIENT_DNS);
        string_from_section = GET_CHAR(values, "dns_server") == NULL ? "" : GET_CHAR(values, "dns_server");
        if(strcmp(string_from_section, string_from_param) != 0) {
            amxc_var_add_key(cstring_t, &new_values, "dns_server", GET_CHAR(params, CLIENT_DNS));
        }
    }
    if(GET_ARG(params, CLIENT_USERNAME) != NULL) {
        when_str_empty_trace(GET_CHAR(values, "username"), exit, ERROR, "Failed to get the username from the UCI config.");
        if((GET_CHAR(params, CLIENT_USERNAME) != NULL) &&
           (strcmp(GET_CHAR(values, "username"), GET_CHAR(params, CLIENT_USERNAME)) != 0)) {
            amxc_var_add_key(cstring_t, &new_values, "username", GET_CHAR(params, CLIENT_USERNAME));
        }
    }
    if(GET_ARG(params, CLIENT_PASSWORD) != NULL) {
        when_str_empty_trace(GET_CHAR(values, "password"), exit, ERROR, "Failed to get the password from the UCI config.");
        if((GET_CHAR(params, CLIENT_PASSWORD) != NULL) &&
           (strcmp(GET_CHAR(values, "password"), GET_CHAR(params, CLIENT_PASSWORD)) != 0)) {
            amxc_var_add_key(cstring_t, &new_values, "password", GET_CHAR(params, CLIENT_PASSWORD));
        }
    }
    amxc_string_setf(&str_enable, "%d", enable ? 1 : 0);
    amxc_var_add_key(cstring_t, &new_values, "enabled", amxc_string_get(&str_enable, 0));

    ret_value = uci_set_ddns(section, &new_values);
    when_failed_trace(ret_value, exit, ERROR, "Failed to set the DDNS parameters on UCI.");

    if(!enable) {
        rv = ddns_status_disabled;
        goto exit;
    } else {
        ret_value = ddns_start_section(section);
        if(ret_value != 0) {
            rv = ddns_status_error;
            goto exit;
        }
    }

    rv = ddns_status_updated;
exit:
    amxc_string_clean(&str_enable);
    amxc_var_clean(&new_values);
    amxc_var_delete(&section_value);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

static int mod_add_ddns(UNUSED const char* function_name,
                        amxc_var_t* params,
                        UNUSED amxc_var_t* ret) {
    SAH_TRACEZ_IN(ME);
    ddns_status_t status = ddns_status_error_config;
    amxc_var_t* uci_current = NULL;
    amxc_string_t section_name;
    uint32_t client_index = GET_UINT32(params, "ClientInstance");
    uint32_t host_index = GET_UINT32(params, "HostnameInstance");
    const char* domain = GET_CHAR(params, MOD_DOMAIN);
    bool enable = GET_BOOL(params, CLIENT_ENABLE);
    int ret_value = -1;

    amxc_string_init(&section_name, 0);

    when_false_trace(client_index > 0, exit, ERROR, "The Client instance index must be > 0.");
    when_false_trace(host_index > 0, exit, ERROR, "The Hostname instance index must be > 0.");
    when_str_empty_trace(domain, exit, ERROR, "The Domain is empty.");

    build_section_name(&section_name, client_index, host_index);

    // Get current uci values of the ddns service
    uci_current = uci_get_ddns_section(amxc_string_get(&section_name, 0), NULL);
    if(uci_current != NULL) {
        SAH_TRACEZ_ERROR(ME, "DDNS configuration already exists.");
        goto exit;
    }

    ret_value = uci_add_ddns(amxc_string_get(&section_name, 0), params, domain,
                             GET_CHAR(params, CLIENT_INTERFACE),
                             GET_CHAR(params, CLIENT_USERNAME),
                             GET_CHAR(params, CLIENT_PASSWORD),
                             enable ? 1 : 0);
    when_failed_trace(ret_value, exit, ERROR, "Failed to add the new ddns.");

    if(!enable) {
        status = ddns_status_disabled;
    } else {
        ret_value = ddns_start_section(amxc_string_get(&section_name, 0));
        if(ret_value != 0) {
            status = ddns_status_error;
        } else {
            status = ddns_status_updated;
        }
    }

exit:
    amxc_string_clean(&section_name);
    amxc_var_delete(&uci_current);
    SAH_TRACEZ_OUT(ME);
    return status;
}

static int mod_edit_ddns(UNUSED const char* function_name,
                         amxc_var_t* params,
                         UNUSED amxc_var_t* ret) {
    SAH_TRACEZ_IN(ME);
    ddns_status_t status = ddns_status_error_config;
    amxc_var_t* uci_current = NULL;
    amxc_string_t section_name;
    uint32_t client_index = GET_UINT32(params, "ClientInstance");
    uint32_t host_index = GET_UINT32(params, "HostnameInstance");

    amxc_string_init(&section_name, 0);

    when_false_trace(client_index > 0, exit, ERROR, "The Client instance index must be > 0.");
    when_false_trace(host_index > 0, exit, ERROR, "The Hostname instance index must be > 0.");

    build_section_name(&section_name, client_index, host_index);

    // Get current uci values of the ddns service
    uci_current = uci_get_ddns_section(amxc_string_get(&section_name, 0), NULL);
    when_null_trace(uci_current, exit, ERROR, "UCI config doesn't exist.");

    ddns_stop_section(amxc_string_get(&section_name, 0));

    status = mod_set_ddns_params(amxc_string_get(&section_name, 0), params);

exit:
    amxc_string_clean(&section_name);
    amxc_var_delete(&uci_current);
    SAH_TRACEZ_OUT(ME);
    return status;
}

static int mod_delete_ddns(UNUSED const char* function_name,
                           amxc_var_t* params,
                           UNUSED amxc_var_t* ret) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    amxc_var_t* uci_current = NULL;
    amxc_string_t section_name;
    uint32_t client_index = GET_UINT32(params, "ClientInstance");
    uint32_t host_index = GET_UINT32(params, "HostnameInstance");
    int ret_value = -1;

    amxc_string_init(&section_name, 0);

    when_false_trace(client_index > 0, exit, ERROR, "The Client instance index must be > 0.");
    when_false_trace(host_index > 0, exit, ERROR, "The Hostname instance index must be > 0.");

    build_section_name(&section_name, client_index, host_index);

    // Get current uci values of the ddns service
    uci_current = uci_get_ddns_section(amxc_string_get(&section_name, 0), NULL);
    when_null_trace(uci_current, exit, ERROR, "UCI config doesn't exist.");

    ddns_stop_section(amxc_string_get(&section_name, 0));

    ret_value = uci_delete_ddns(amxc_string_get(&section_name, 0));
    when_failed_trace(ret_value, exit, ERROR, "Failed to delete the DDNS section %s.", amxc_string_get(&section_name, 0));

    rv = 0;

exit:
    amxc_string_clean(&section_name);
    amxc_var_delete(&uci_current);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

static int mod_check_ddns(UNUSED const char* function_name,
                          amxc_var_t* params,
                          UNUSED amxc_var_t* ret) {
    SAH_TRACEZ_IN(ME);
    ddns_error_t rv = ddns_error_config;
    amxc_var_t* uci_current = NULL;
    amxc_string_t section_name;
    uint32_t client_index = GET_UINT32(params, "ClientInstance");
    uint32_t host_index = GET_UINT32(params, "HostnameInstance");

    amxc_string_init(&section_name, 0);

    when_false_trace(client_index > 0, exit, ERROR, "The Client instance index must be > 0.");
    when_false_trace(host_index > 0, exit, ERROR, "The Hostname instance index must be > 0.");

    build_section_name(&section_name, client_index, host_index);

    // Get current uci values of the ddns service
    uci_current = uci_get_ddns_section(amxc_string_get(&section_name, 0), NULL);
    when_null_trace(uci_current, exit, ERROR, "UCI config doesn't exist.");

    rv = ddns_get_status(amxc_string_get(&section_name, 0));
    if((rv != ddns_error_updating) && (rv != ddns_error_no)) {
        ddns_stop_section(amxc_string_get(&section_name, 0));
    }

exit:
    amxc_string_clean(&section_name);
    amxc_var_delete(&uci_current);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

static int mod_stop_ddns(UNUSED const char* function_name,
                         amxc_var_t* params,
                         UNUSED amxc_var_t* ret) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    amxc_string_t section_name;
    uint32_t client_index = GET_UINT32(params, "ClientInstance");
    uint32_t host_index = GET_UINT32(params, "HostnameInstance");

    amxc_string_init(&section_name, 0);

    when_false_trace(client_index > 0, exit, ERROR, "The Client instance index must be > 0.");
    when_false_trace(host_index > 0, exit, ERROR, "The Hostname instance index must be > 0.");

    build_section_name(&section_name, client_index, host_index);

    rv = ddns_stop_section(amxc_string_get(&section_name, 0));

exit:
    amxc_string_clean(&section_name);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

static int mod_supported_services(UNUSED const char* function_name,
                                  UNUSED amxc_var_t* params,
                                  amxc_var_t* ret) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;

    if(ret == NULL) {
        amxc_var_new(&ret);
        amxc_var_set_type(ret, AMXC_VAR_ID_HTABLE);
    }

    rv = parse_services(ret);
    when_failed_trace(rv, exit, ERROR, "Failed to parse the services.");

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

static int mod_init(UNUSED const char* function_name,
                    UNUSED amxc_var_t* params,
                    UNUSED amxc_var_t* ret) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;

    ddns_disable_service();
    uci_clean_ddns_uci_entries();
    rv = uci_set_ddns_global(NULL, NULL, "1");
    when_failed_trace(rv, exit, ERROR, "Failed to set values to the global UCI configuration.");

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

static AMXM_CONSTRUCTOR ddns_uci_start(void) {
    int rv = -1;
    amxm_shared_object_t* so = amxm_so_get_current();
    amxm_module_t* mod = NULL;

    rv = amxm_module_register(&mod, so, MOD_DDNS_CTRL);
    when_failed_trace(rv, error, ERROR, "Failed to register the module %s", MOD_DDNS_CTRL);

    rv = amxm_module_add_function(mod, "add-ddns", mod_add_ddns);
    when_failed_trace(rv, error, ERROR, "Failed to add the function %s from the module %s", "add-ddns", MOD_DDNS_CTRL);

    rv = amxm_module_add_function(mod, "edit-ddns", mod_edit_ddns);
    when_failed_trace(rv, error, ERROR, "Failed to add the function %s from the module %s", "edit-ddns", MOD_DDNS_CTRL);

    rv = amxm_module_add_function(mod, "delete-ddns", mod_delete_ddns);
    when_failed_trace(rv, error, ERROR, "Failed to add the function %s from the module %s", "delete-ddns", MOD_DDNS_CTRL);

    rv = amxm_module_add_function(mod, "stop-ddns", mod_stop_ddns);
    when_failed_trace(rv, error, ERROR, "Failed to add the function %s from the module %s", "stop-ddns", MOD_DDNS_CTRL);

    rv = amxm_module_add_function(mod, "check-ddns", mod_check_ddns);
    when_failed_trace(rv, error, ERROR, "Failed to add the function %s from the module %s", "check-ddns", MOD_DDNS_CTRL);

    rv = amxm_module_add_function(mod, "supported-services", mod_supported_services);
    when_failed_trace(rv, error, ERROR, "Failed to add the function %s from the module %s", "supported-services", MOD_DDNS_CTRL);

    rv = amxm_module_add_function(mod, "init", mod_init);
    when_failed_trace(rv, error, ERROR, "Failed to add the function %s from the module %s", "init", MOD_DDNS_CTRL);

error:
    return rv;
}

static AMXM_DESTRUCTOR ddns_uci_stop(void) {
    return 0;
}
