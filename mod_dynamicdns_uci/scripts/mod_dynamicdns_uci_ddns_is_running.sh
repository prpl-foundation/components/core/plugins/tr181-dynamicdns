#!/bin/bash

set +x
pgrep -f -a "$1 -- start"
echo "Finished script"
sleep 3
