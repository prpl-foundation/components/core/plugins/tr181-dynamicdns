/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__DDNS_ERROR_CODES_H__)
#define __DDNS_ERROR_CODES_H__

#ifdef __cplusplus
extern "C" {
#endif

#define DDNS_STATUS(status_ddns) \
    str_ddns_status[status_ddns]

#define DDNS_ERROR(error_ddns) \
    str_ddns_error[error_ddns]

#define DDNS_HOSTNAME_STATUS(status_ddns_hostname) \
    str_ddns_hostname[status_ddns_hostname]

typedef enum {
    ddns_status_connecting,
    ddns_status_authenticating,
    ddns_status_updated,
    ddns_status_error_config,
    ddns_status_error,
    ddns_status_disabled,
    ddns_status_count
} ddns_status_t;

typedef enum {
    ddns_error_no,
    ddns_error_config,
    ddns_error_dns,
    ddns_error_conn,
    ddns_error_auth,
    ddns_error_timeout,
    ddns_error_protocol,
    ddns_error_updating,
    ddns_error_count
} ddns_error_t;

typedef enum {
    ddns_hostname_registered,
    ddns_hostname_up_needed,
    ddns_hostname_updating,
    ddns_hostname_error,
    ddns_hostname_disabled,
    ddns_hostname_count
} ddns_hostname_status_t;

#define DDNS_STATUS_CONNECTING     "Connecting"
#define DDNS_STATUS_AUTHENTICATING "Authenticating"
#define DDNS_STATUS_UPDATED        "Updated"
#define DDNS_STATUS_ERROR_MISC     "Error_Misconfigured"
#define DDNS_STATUS_ERROR          "Error"
#define DDNS_STATUS_DISABLED       "Disabled"

#define DDNS_ERROR_NO              "NO_ERROR"
#define DDNS_ERROR_MISC            "MISCONFIGURATION_ERROR"
#define DDNS_ERROR_DNS             "DNS_ERROR"
#define DDNS_ERROR_CONNECTION      "CONNECTION_ERROR"
#define DDNS_ERROR_AUTHENTICATION  "AUTHENTICATION_ERROR"
#define DDNS_ERROR_TIMEOUT         "TIMEOUT_ERROR"
#define DDNS_ERROR_PROTOCOL        "PROTOCOL_ERROR"
#define DDNS_ERROR_UPDATING        "UPDATING"

#define DDNS_HOSTNAME_REGISTERED   "Registered"
#define DDNS_HOSTNAME_UP_NEEDED    "UpdateNeeded"
#define DDNS_HOSTNAME_UPDATING     "Updating"
#define DDNS_HOSTNAME_ERROR        "Error"
#define DDNS_HOSTNAME_DISABLED     "Disabled"

extern const cstring_t str_ddns_status[ddns_status_count];
extern const cstring_t str_ddns_error[ddns_error_count];
extern const cstring_t str_ddns_hostname[ddns_hostname_count];

#ifdef __cplusplus
}
#endif

#endif // __DDNS_ERROR_CODES_H__
