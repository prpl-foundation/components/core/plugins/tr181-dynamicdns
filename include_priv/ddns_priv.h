/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__TR_DDNS_PRIV_H__)
#define __TR_DDNS_PRIV_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <amxc/amxc_macros.h>
#include <amxc/amxc.h>
#include <amxp/amxp_signal.h>
#include <amxd/amxd_transaction.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_dm.h>
#include <amxo/amxo.h>
#include <amxo/amxo_types.h>
#include <amxp/amxp_slot.h>
#include <amxp/amxp_timer.h>
#include <amxb/amxb.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <netmodel/common_api.h>

#define OBJECT_NAME(OBJECT) amxd_object_get_name(OBJECT, AMXD_OBJECT_NAMED)

typedef enum ddns_status {
    ddns_error = -1,
    ddns_ok = 0
} ddns_plugin_status_t;

typedef enum {
    IPvAll = 0,
    IPv4 = 4,
    IPv6 = 6
} ipversion_t;

int _dynamic_dns_main(int reason, amxd_dm_t* dm, amxo_parser_t* parser);

amxd_status_t _Reset(amxd_object_t* object,
                     amxd_function_t* func,
                     amxc_var_t* args,
                     amxc_var_t* ret);

amxd_status_t _interface_destroy(amxd_object_t* intf,
                                 amxd_param_t* param,
                                 amxd_action_t reason,
                                 const amxc_var_t* const args,
                                 amxc_var_t* const retval,
                                 void* priv);

amxd_status_t _lastchange_read(amxd_object_t* intf,
                               amxd_param_t* param, amxd_action_t reason,
                               const amxc_var_t* const args,
                               amxc_var_t* const retval, void* priv);

int extract_address_from_query(amxc_var_t** new_intf_var, uint32_t* new_num_of_addrs, const amxc_var_t* var, ipversion_t ipversion);
int ddns_init(amxd_dm_t* dm, amxo_parser_t* parser);
void ddns_shutdown(void);
amxd_dm_t* ddns_get_dm(void);
amxo_parser_t* ddns_get_parser(void);
amxb_bus_ctx_t* ddns_get_ctx(void);
const char* ddns_get_wan_logical_interface(void);
const char* ddns_get_wan_ip_interface(void);
const char* ddns_get_vendor_prefix(void);
const cstring_t ddns_get_string_param(amxd_object_t* object, const cstring_t parameter);
uint32_t ddns_get_uint32_param(amxd_object_t* object, const cstring_t parameter);
ipversion_t int_to_ipversion(int value);
ipversion_t string_to_ipversion(const char* value);

netmodel_query_t* ddns_get_wan_query(void);
amxd_object_t* ddns_get_root_object(void);
int set_on_dm(amxd_object_t* obj, const cstring_t key, const cstring_t value);
char* remove_device_prefix(const char* str);

/* Debugging Event Printer */
void _print_event(const char* const sig_name,
                  const amxc_var_t* const data,
                  void* const priv);

/* Debugging Event Logger */
void _syslog_event(const char* const sig_name,
                   const amxc_var_t* const data,
                   void* const priv);

int ddns_get_current_time(amxc_var_t* var);

#ifdef __cplusplus
}
#endif

#endif // __TR_DDNS_PRIV_H__
