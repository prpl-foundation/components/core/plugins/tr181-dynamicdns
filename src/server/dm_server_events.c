/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <netmodel/common_api.h>

#include "ddns_priv.h"
#include "ddns.h"
#include "ddns_error_codes.h"
#include "ddns_config.h"
#include "server/dm_server.h"

#define ME "ddns"

/**
   @brief
   Get the Server object from the Data Model.

   @param[in] dm Pointer to datamodel.

   @return
   Object or NULL if the Server is not available.
 */
amxd_object_t* ddns_server_template(amxd_dm_t* dm) {
    amxd_object_t* templ = amxd_dm_findf(dm, DDNS ".Server.");
    return templ;
}

amxc_var_t* get_server_param(const char* path, const char* key) {
    amxc_var_t* tmp_var = NULL;
    amxd_object_t* obj_server = NULL;
    char* real_path = remove_device_prefix(path);

    when_str_empty(path, exit);
    when_str_empty(key, exit);

    obj_server = amxd_dm_findf(ddns_get_dm(), "%s.", real_path);
    when_null(obj_server, exit);

    tmp_var = (amxc_var_t*) amxd_object_get_param_value(obj_server, key);

exit:
    free(real_path);
    return tmp_var;
}

int add_new_server(amxc_var_t* values) {
    int rv = -1;
    amxd_object_t* templ_server = NULL;
    amxd_trans_t* trans = NULL;
    const cstring_t key = NULL;
    amxc_string_t alias;

    amxc_string_init(&alias, 0);
    templ_server = ddns_server_template(ddns_get_dm());
    when_null(templ_server, exit);
    when_failed(amxd_trans_new(&trans), exit);
    amxd_trans_set_attr(trans, amxd_tattr_change_ro, true);
    when_failed(amxd_trans_select_object(trans, templ_server), exit);
    amxd_trans_add_inst(trans, 0, NULL);

    amxc_var_for_each(value, values) {
        key = amxc_var_key(value);
        amxd_trans_set_param(trans, key, value);
    }

    when_str_empty(GETP_CHAR(values, SERVER_SERVICE), exit);
    amxc_string_setf(&alias, "cpe-%s", GETP_CHAR(values, SERVER_SERVICE));
    amxc_string_replace(&alias, ".", "_", UINT32_MAX);
    amxd_trans_set_cstring_t(trans, "Alias", amxc_string_get(&alias, 0));
    when_failed(amxd_trans_apply(trans, ddns_get_dm()), exit);

    rv = 0;
exit:
    amxd_trans_delete(&trans);
    amxc_string_clean(&alias);
    return rv;
}

int edit_server(amxc_var_t* values) {
    int rv = -1;
    amxd_object_t* templ_server = NULL;
    amxd_object_t* server_obj = NULL;
    amxd_trans_t* trans = NULL;
    const cstring_t key = NULL;
    const cstring_t name = NULL;

    when_null(values, exit);
    templ_server = ddns_server_template(ddns_get_dm());
    when_null(templ_server, exit);
    name = GETP_CHAR(values, SERVER_NAME);
    when_str_empty(name, exit);
    server_obj = amxd_object_findf(templ_server, "[" SERVER_NAME " == '%s']", name);
    when_null(server_obj, exit);

    when_failed(amxd_trans_new(&trans), exit);
    amxd_trans_set_attr(trans, amxd_tattr_change_ro, true);
    when_failed(amxd_trans_select_object(trans, server_obj), exit);

    amxc_var_for_each(value, values) {
        key = amxc_var_key(value);
        amxd_trans_set_param(trans, key, value);
    }

    when_failed_trace(amxd_trans_apply(trans, ddns_get_dm()), exit, ERROR, "Failed to change the Server table.");

    rv = 0;
exit:
    amxd_trans_delete(&trans);
    return rv;
}

int add_new_supported_service(const char* service) {
    int rv = -1;
    amxd_object_t* templ_ddns = ddns_get_root_object();
    amxc_var_t* tmp_var = NULL;
    const char* supported_services;
    amxc_string_t* tmp_string = NULL;
    int service_pos = 0;
    amxc_var_t* value = NULL;
    amxd_trans_t* trans = NULL;

    amxc_string_new(&tmp_string, 0);
    amxc_var_new(&value);

    when_null_trace(templ_ddns, exit, ERROR, "DynamicDNS object is NULL.");
    tmp_var = (amxc_var_t*) amxd_object_get_param_value(templ_ddns, DDNS_SUPPORTED_SERVICES);
    when_null_trace(tmp_var, exit, ERROR, "%s is NULL.", DDNS_SUPPORTED_SERVICES);
    supported_services = GET_CHAR(tmp_var, NULL);

    if(!str_empty(supported_services)) {
        amxc_string_setf(tmp_string, "%s", supported_services);
        amxc_string_appendf(tmp_string, ",");
    }
    service_pos = amxc_string_search(tmp_string, service, 0);
    when_false_trace(service_pos < 0, exit, NOTICE, "The service is already in the list.");

    amxc_string_appendf(tmp_string, "%s", service);

    when_failed(amxd_trans_new(&trans), exit);
    amxd_trans_set_attr(trans, amxd_tattr_change_ro, true);
    when_failed(amxd_trans_select_object(trans, templ_ddns), exit);
    amxc_var_set(cstring_t, value, amxc_string_get(tmp_string, 0));
    amxd_trans_set_param(trans, DDNS_SUPPORTED_SERVICES, value);
    when_failed(amxd_trans_apply(trans, ddns_get_dm()), exit);

    rv = 0;
exit:
    amxd_trans_delete(&trans);
    amxc_string_delete(&tmp_string);
    amxc_var_delete(&value);
    return rv;
}