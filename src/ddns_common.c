/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <string.h>
#include <amxc/amxc.h>
#include <amxp/amxp_signal.h>
#include <amxm/amxm.h>
#include <amxd/amxd_object.h>
#include <netmodel/client.h>

#include "ddns.h"
#include "ddns_priv.h"
#include "ddns_error_codes.h"
#include "ddns_config.h"
#include "client/dm_client.h"

#define ME "ddns"

#define ODL_DEFAULTS "?include '${odl.directory}/${name}.odl':'${odl.dm-defaults}';"

static dynamic_dns_t dynamic_dns;

const cstring_t str_ddns_status[ddns_status_count] = {
    DDNS_STATUS_CONNECTING,
    DDNS_STATUS_AUTHENTICATING,
    DDNS_STATUS_UPDATED,
    DDNS_STATUS_ERROR_MISC,
    DDNS_STATUS_ERROR,
    DDNS_STATUS_DISABLED
};

const cstring_t str_ddns_error[ddns_error_count] = {
    DDNS_ERROR_NO,
    DDNS_ERROR_MISC,
    DDNS_ERROR_DNS,
    DDNS_ERROR_CONNECTION,
    DDNS_ERROR_AUTHENTICATION,
    DDNS_ERROR_TIMEOUT,
    DDNS_ERROR_PROTOCOL,
    DDNS_ERROR_UPDATING
};

const cstring_t str_ddns_hostname[ddns_hostname_count] = {
    DDNS_HOSTNAME_REGISTERED,
    DDNS_HOSTNAME_UP_NEEDED,
    DDNS_HOSTNAME_UPDATING,
    DDNS_HOSTNAME_ERROR,
    DDNS_HOSTNAME_DISABLED
};

amxd_dm_t* ddns_get_dm(void) {
    return dynamic_dns.dm;
}

amxo_parser_t* ddns_get_parser(void) {
    return dynamic_dns.parser;
}

amxb_bus_ctx_t* ddns_get_ctx(void) {
    return dynamic_dns.ctx;
}

netmodel_query_t* ddns_get_wan_query(void) {
    return dynamic_dns.query_wan_address;
}

amxd_object_t* ddns_get_root_object(void) {
    return amxd_dm_findf(ddns_get_dm(), DDNS ".");
}

static const char* ddns_get_config_string(const char* name) {
    amxc_var_t* setting = amxo_parser_get_config(ddns_get_parser(), name);
    const char* value = amxc_var_constcast(cstring_t, setting);
    return (value != NULL) ? value : "";
}

const char* ddns_get_wan_logical_interface(void) {
    return ddns_get_config_string("logical_intf_wan");
}

const char* ddns_get_wan_ip_interface(void) {
    return ddns_get_config_string("ip_intf_wan");
}

const char* ddns_get_vendor_prefix(void) {
    return ddns_get_config_string("vendor_prefix");
}

static const amxc_var_t* get_parameter(amxd_object_t* object, const cstring_t parameter) {
    amxc_string_t param_string;
    amxc_string_t param_string_vendor;
    const amxc_var_t* var = NULL;

    amxc_string_init(&param_string, 0);
    amxc_string_init(&param_string_vendor, 0);

    when_str_empty_trace(parameter, out, ERROR, "The parameter is empty.");
    when_null_trace(object, out, ERROR, "Object is NULL");

    amxc_string_setf(&param_string_vendor, "%s%s", ddns_get_vendor_prefix(), parameter);
    amxc_string_setf(&param_string, "%s", parameter);

    var = amxd_object_get_param_value(object, amxc_string_get(&param_string_vendor, 0));
    when_not_null(var, out);
    var = amxd_object_get_param_value(object, amxc_string_get(&param_string, 0));

out:
    amxc_string_clean(&param_string);
    amxc_string_clean(&param_string_vendor);
    return var;
}

const cstring_t ddns_get_string_param(amxd_object_t* object, const cstring_t parameter) {
    const cstring_t value = NULL;
    const amxc_var_t* var = NULL;

    var = get_parameter(object, parameter);
    when_null(var, out);

    value = GET_CHAR(var, NULL);

out:
    return value;
}

uint32_t ddns_get_uint32_param(amxd_object_t* object, const cstring_t parameter) {
    uint32_t value = 0;
    const amxc_var_t* var = NULL;

    var = get_parameter(object, parameter);
    when_null(var, out);

    value = GET_UINT32(var, NULL);

out:
    return value;
}

ipversion_t int_to_ipversion(int value) {
    ipversion_t version = IPv4;

    if(value == 4) {
        version = IPv4;
    } else if(value == 6) {
        version = IPv6;
    }

    return version;
}

ipversion_t string_to_ipversion(const char* value) {
    ipversion_t version = IPv4;

    if(strcmp(value, "ipv4") == 0) {
        version = IPv4;
    } else if(strcmp(value, "ipv6") == 0) {
        version = IPv6;
    }

    return version;
}

static ddns_plugin_status_t ddns_init_bus_ctx(void) {
    ddns_plugin_status_t status = ddns_error;
    dynamic_dns.ctx = amxb_be_who_has(DDNS);
    when_null_trace(dynamic_dns.ctx, exit, ERROR, "No bus context can provide the object.");
    status = ddns_ok;
exit:
    return status;
}

static void ddns_on_delete_fd(UNUSED const char* signame,
                              const amxc_var_t* const data,
                              UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    int fd = -1;
    int retval;

    when_true(amxc_var_type_of(data) != AMXC_VAR_ID_FD, leave)

    fd = amxc_var_dyncast(fd_t, data);
    when_true(fd != amxb_get_fd(dynamic_dns.ctx), leave)

    SAH_TRACEZ_WARNING(ME, "Current active connection is closed");
    retval = ddns_init_bus_ctx();
    when_failed_trace(retval, leave, ERROR, "Unable to get new bus context");

leave:
    SAH_TRACEZ_OUT(ME);
    return;
}

int extract_address_from_query(amxc_var_t** new_intf_var, uint32_t* new_num_of_addrs, const amxc_var_t* var, ipversion_t ipversion) {
    SAH_TRACEZ_IN(ME);
    const amxc_htable_t* ht_params = NULL;
    const char* ipaddress = NULL;
    const char* family = NULL;
    int rv = -1;

    when_null(new_intf_var, exit);
    when_null(*new_intf_var, exit);
    when_null(new_num_of_addrs, exit);
    when_null(var, exit);

    ht_params = amxc_var_constcast(amxc_htable_t, var);
    ipaddress = GET_CHAR(var, "Address");
    family = GET_CHAR(var, "Family");

    when_null(ht_params, exit);
    when_str_empty(ipaddress, exit);
    when_str_empty(family, exit);

    when_true(ipversion != IPvAll && (string_to_ipversion(family) != ipversion), exit);

    amxc_var_t* new_address_var = amxc_var_get_key(*new_intf_var, ipaddress, AMXC_VAR_FLAG_DEFAULT);
    if(new_address_var == NULL) {
        amxc_var_add_key(amxc_htable_t, *new_intf_var, ipaddress, ht_params);
        (*new_num_of_addrs)++;
    }

    rv = 0;

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

static void fill_addresses_data_cb(const char* sig_name UNUSED, const amxc_var_t* result, UNUSED void* userdata) {
    SAH_TRACEZ_IN(ME);
    amxc_var_t* new_intf_var = NULL;
    uint32_t new_num_of_addrs = 0;
    int rv = -1;
    int compare_res = -1;

    when_null_trace(result, exit, ERROR, "Result is empty");

    amxc_var_new(&new_intf_var);
    amxc_var_set_type(new_intf_var, AMXC_VAR_ID_HTABLE);

    amxc_var_for_each(var, result) {
        rv = extract_address_from_query(&new_intf_var, &new_num_of_addrs, var, IPvAll);
        if(rv == -1) {
            continue;
        }
    }
    //In case 'result' is not a list of variants, or not iterable, this extra run guarantees that the information is properly extracted.
    extract_address_from_query(&new_intf_var, &new_num_of_addrs, result, IPvAll);

    rv = amxc_var_compare(dynamic_dns.wan_addresses, new_intf_var, &compare_res);
    if((rv == 0) && (compare_res != 0)) {
        amxc_var_delete(&(dynamic_dns.wan_addresses));
        amxc_var_move(dynamic_dns.wan_addresses, new_intf_var);
        // Only update the clients if the address exists and changed
        if(new_num_of_addrs > 0) {
            check_all_clients();
            dynamic_dns.is_up = true;
        } else {
            dynamic_dns.is_up = false;
        }
    }

exit:
    amxc_var_delete(&new_intf_var);
    SAH_TRACEZ_OUT(ME);
    return;
}

/**
   @brief
   Fetch the list of IP addresses of the WAN Interface.

   @return
   0 if the query is opened.
 */
static int init_query_wan_address(void) {
    SAH_TRACEZ_IN(ME);
    const cstring_t interface = ddns_get_wan_logical_interface();
    int rv = -1;

    when_str_empty_trace(interface, exit, ERROR, "The WAN interface is empty.");

    amxc_var_new(&dynamic_dns.wan_addresses);
    amxc_var_set_type(dynamic_dns.wan_addresses, AMXC_VAR_ID_HTABLE);
    dynamic_dns.is_up = false;
    dynamic_dns.query_wan_address = netmodel_openQuery_getAddrs(interface, ME, "", netmodel_traverse_down, fill_addresses_data_cb, NULL);

    rv = 0;

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

int ddns_init(amxd_dm_t* dm, amxo_parser_t* parser) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;

    dynamic_dns.dm = dm;
    dynamic_dns.parser = parser;

    netmodel_initialize();

    rv = ddns_init_bus_ctx(); // sets dynamic_dns.ctx
    when_failed_trace(rv, leave, ERROR, "Unable to initialise bus context, retval = %d", rv);

    // It is possible that the remote side closes the connection, in
    // that case amxrt will make sure that a "connection-deleted" signal is
    // send, which can be handled in this application.
    //
    // if needed you can add an slot (= ambiorix signal callback) that handles
    // the ambiorix signal
    rv = amxp_slot_connect(NULL, "connection-deleted", NULL, ddns_on_delete_fd, NULL);
    when_failed_trace(rv, leave, ERROR, "Unable to connect to signal 'connection-deleted', retval = %d", rv);

    // load the controllers
    rv = ddns_open_cfgctrlrs();
    when_failed_trace(rv, leave, ERROR, "Failed to load all supported modules");

    // Query WAN address
    rv = init_query_wan_address();
    when_failed_trace(rv, leave, ERROR, "Failed to initialize the WAN interface query.");

    // load the defaults after the configuration module is loaded so the
    // default/saved configuration can be applied at start-up.
    amxo_parser_parse_string(parser, ODL_DEFAULTS, amxd_dm_get_root(dm));

    // Load the server configuration from the the modules.
    cfgctrlr_ddns_fetch_servers();

leave:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

void ddns_shutdown(void) {
    amxm_close_all();
    close_queries();
    netmodel_closeQuery(ddns_get_wan_query());
    netmodel_cleanup();
}

int set_on_dm(amxd_object_t* obj, const cstring_t key, const cstring_t value) {
    int rv = -1;
    amxc_var_t* tmp_var = NULL;
    amxd_trans_t* trans = NULL;

    when_null(obj, exit);
    when_failed(amxd_trans_new(&trans), exit);
    amxd_trans_set_attr(trans, amxd_tattr_change_ro, true);
    when_failed(amxd_trans_select_object(trans, obj), exit);
    amxc_var_new(&tmp_var);
    amxc_var_set(cstring_t, tmp_var, value);
    amxd_trans_set_param(trans, key, tmp_var);
    when_failed(amxd_trans_apply(trans, ddns_get_dm()), exit);

    rv = 0;
exit:
    amxd_trans_delete(&trans);
    amxc_var_delete(&tmp_var);
    return rv;
}

char* remove_device_prefix(const char* str) {
    amxc_string_t object_path;
    char* result = NULL;

    amxc_string_init(&object_path, 0);
    amxc_string_setf(&object_path, "%s", str);
    amxc_string_replace(&object_path, "Device.", "", UINT32_MAX);

    result = amxc_string_take_buffer(&object_path);

    amxc_string_clean(&object_path);
    return result;
}

/* Debugging Event Logger */
void _syslog_event(UNUSED const char* const sig_name,
                   const amxc_var_t* const data,
                   UNUSED void* const priv) {
    SAH_TRACEZ_INFO(ME, "Signal received - %s", sig_name);
    SAH_TRACEZ_INFO(ME, "Signal data = ");
    if(!amxc_var_is_null(data)) {
        amxc_var_log(data);
    }
}

/**
   @brief
   Get the current time.

   @param[out] var Variant with the time.

   @return
   Not 0 if the function fails.
 */
int ddns_get_current_time(amxc_var_t* var) {
    amxc_string_t* str = NULL;
    amxc_ts_t ts;
    int rv = -1;

    amxc_string_new(&str, 0);

    when_null_trace(var, exit, ERROR, "Output variant is not initialised.");

    when_failed_trace(amxc_ts_now(&ts), exit, ERROR, "Failed to get the current time.");
    amxc_string_setf(str, "0000-00-00T00:00:00.000000Z");
    amxc_ts_format_precision(&ts, str->buffer, amxc_string_buffer_length(str), 6);

    amxc_var_set(cstring_t, var, str->buffer);

    rv = 0;

exit:
    amxc_string_delete(&str);
    return rv;
}
