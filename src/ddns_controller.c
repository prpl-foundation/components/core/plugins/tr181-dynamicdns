/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <string.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxm/amxm.h>
#include <debug/sahtrace.h>

#include "ddns.h"
#include "ddns_priv.h"
#include "ddns_config.h"
#include "server/dm_server.h"

#define ME "ddns"

int ddns_open_cfgctrlrs(void) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    amxc_var_t* args = NULL;
    amxc_var_t* uci_services = NULL;
    amxc_var_t lcontrollers;
    amxc_string_t mod_path;
    amxm_shared_object_t* so = NULL;
    amxd_object_t* ddns_obj = amxd_dm_findf(ddns_get_dm(), DDNS);
    amxo_parser_t* parser = ddns_get_parser();
    const amxc_var_t* controllers = amxd_object_get_param_value(ddns_obj, "SupportedControllers");
    const char* const mod_dir = GETP_CHAR(&parser->config, "mod-dir");

    amxc_var_init(&lcontrollers);
    amxc_string_init(&mod_path, 0);
    amxc_var_new(&args);
    amxc_var_new(&uci_services);

    amxc_var_convert(&lcontrollers, controllers, AMXC_VAR_ID_LIST);
    //Load controllers
    amxc_var_for_each(controller, &lcontrollers) {
        const char* name = GET_CHAR(controller, NULL);
        amxc_string_setf(&mod_path, "%s/%s.so", mod_dir, name);
        rv = amxm_so_open(&so, name, amxc_string_get(&mod_path, 0));
        SAH_TRACEZ_INFO(ME, "Loading controller '%s'", name);
        when_failed_trace(rv, exit, ERROR, "Failed to load the controller '%s'", name);
    }

    //Initialize controllers
    amxc_var_for_each(controller, &lcontrollers) {
        const char* name = GET_CHAR(controller, NULL);
        rv = amxm_execute_function(name, MOD_DDNS_CTRL, "init", args, uci_services);
        if(rv != 0) {
            SAH_TRACEZ_ERROR(ME, "Failed to initialize the controller '%s'", name);
        }
    }

    rv = 0;
exit:
    amxc_string_clean(&mod_path);
    amxc_var_clean(&lcontrollers);
    amxc_var_delete(&args);
    amxc_var_delete(&uci_services);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

int cfgctrlr_ddns_fetch_servers(void) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    int ret_value = -1;
    amxc_var_t args;
    amxc_var_t uci_services;
    const cstring_t service_name = NULL;
    amxc_var_t lcontrollers;
    amxd_object_t* ddns_obj = amxd_dm_findf(ddns_get_dm(), DDNS);
    const amxc_var_t* controllers = amxd_object_get_param_value(ddns_obj, "SupportedControllers");

    amxc_var_init(&lcontrollers);
    amxc_var_init(&args);

    when_null_trace(controllers, exit, ERROR, "SupportedControllers is NULL.");

    amxc_var_convert(&lcontrollers, controllers, AMXC_VAR_ID_LIST);
    amxc_var_for_each(controller, &lcontrollers) {
        const char* name = GET_CHAR(controller, NULL);
        amxc_var_init(&uci_services);

        //Fill the available servers from each module.
        rv = amxm_execute_function(name, MOD_DDNS_CTRL, "supported-services", &args, &uci_services);
        if(rv != 0) {
            SAH_TRACEZ_ERROR(ME, "Could not execute 'supported-services' on the %s controller", name);
            amxc_var_clean(&uci_services);
            continue;
        }

        amxc_var_for_each(service, &uci_services) {
            service_name = amxc_var_key(service);

            rv = add_new_supported_service(service_name);
            if(rv != 0) {
                continue;
            }

            rv = add_new_server(service);
            if(rv != 0) {
                rv = edit_server(service);
                if(rv != 0) {
                    continue;
                }
            }
        }

        amxc_var_clean(&uci_services);
    }

    // Add the service "custom" to allow custom DDNS servers
    add_new_supported_service("custom");

    ret_value = 0;

exit:
    amxc_var_clean(&lcontrollers);
    amxc_var_clean(&args);
    amxc_var_clean(&uci_services);
    SAH_TRACEZ_OUT(ME);
    return ret_value;
}

/**
 * @brief Executes a function on the DynamicDNS's configuration controller
 * @param obj The object from which to get the module name to use, if NULL mod_dynamicdns_uci is used
 * @param func_name The name of the function to execute
 * @param args variant structure of the arguments to the function. All functions expect the same fields in the variant!
 * @param ret variant structure of the return values from the function
 * @return 0 if the function executed without errors, otherwise it return -1
 */
int cfgctrlr_execute_function(amxd_object_t* obj,
                              const char* const func_name,
                              amxc_var_t* args,
                              amxc_var_t* ret) {
    SAH_TRACEZ_IN(ME);
    int status = -1;
    const amxc_var_t* ddns_ctrl = amxd_object_get_param_value(obj, "Controller");
    const char* ddns_ctrlr_name = NULL;

    // find controller for the configuration
    if(obj == NULL) {
        // If no object was given we want to use the uci module
        ddns_ctrlr_name = "mod_dynamicdns_uci";
    } else {
        ddns_ctrlr_name = GET_CHAR(ddns_ctrl, NULL);
        when_str_empty_trace(ddns_ctrlr_name, exit, ERROR, "Could not find the controller for this configuration");
    }

    // Call controller-specific implementation
    status = amxm_execute_function(ddns_ctrlr_name, MOD_DDNS_CTRL, func_name, args, ret);
    if(status < 0) {
        SAH_TRACEZ_ERROR(ME, "Could not execute '%s' on the %s controller", func_name, ddns_ctrlr_name);
        goto exit;
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return status;
}

int cfgctrlr_ddns_execute_function(amxd_object_t* ddns_obj,
                                   const char* function,
                                   amxc_var_t* data,
                                   amxc_var_t* ret) {
    int rv = -1;

    when_str_empty_trace(function, exit, ERROR, "Can not execute function, empty string");
    when_null_trace(ddns_obj, exit, ERROR, "can not execute %s, no object given", function);

    rv = cfgctrlr_execute_function(ddns_obj, function, data, ret);
    if(rv < 0) {
        SAH_TRACEZ_ERROR(ME, "function %s failed", function);
    }

exit:
    return rv;
}