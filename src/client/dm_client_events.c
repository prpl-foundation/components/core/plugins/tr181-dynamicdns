/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <netmodel/common_api.h>

#include "ddns_priv.h"
#include "ddns.h"
#include "ddns_error_codes.h"
#include "ddns_config.h"
#include "client/dm_client.h"
#include "client/client.h"
#include "server/dm_server.h"

#define ME "ddns"

#define CHECK_AFTER_ADD_INTERVAL_DEFAULT 3000      //Intermediate checks to update the status
#define CHECK_AFTER_ADD_MAX_INTERVAL_DEFAULT 30000 //Must wait 30s for the UCI module
#define CHECK_AFTER_ADD_MAX_COUNTER 10             //ceil(CHECK_AFTER_ADD_MAX_INTERVAL_DEFAULT / CHECK_AFTER_ADD_INTERVAL_DEFAULT)

static bool get_server_enable(amxd_object_t* obj_ddns_client, const amxc_var_t* server_param);

/**
   @brief
   Get the Client object from the Data Model.

   @param[in] dm Pointer to datamodel.

   @return
   Object or NULL if the Client is not available.
 */
amxd_object_t* ddns_client_template(amxd_dm_t* dm) {
    amxd_object_t* templ = amxd_dm_findf(dm, "DynamicDNS.Client.");
    return templ;
}

static ddns_hostname_status_t get_hostname_status(ddns_status_t status) {
    ddns_hostname_status_t host_status = ddns_hostname_error;
    if(status == ddns_status_disabled) {
        host_status = ddns_hostname_disabled;
    } else if(status == ddns_status_updated) {
        host_status = ddns_hostname_updating;
    }
    return host_status;
}

static void set_status_on_dm(ddns_status_t status, amxd_object_t* obj_ddns_client, bool enable, bool enable_global) {
    int host_num_of_entries = 0;
    amxd_object_t* tmpl_ddns_client_hostname = NULL;

    when_null(obj_ddns_client, exit);
    tmpl_ddns_client_hostname = amxd_object_findf(obj_ddns_client, ".Hostname.");
    when_null(tmpl_ddns_client_hostname, exit);

    host_num_of_entries = amxd_object_get_instance_count(tmpl_ddns_client_hostname);

    if((status == ddns_status_updated) && enable) {
        set_on_dm(obj_ddns_client, "Status", DDNS_STATUS(ddns_status_authenticating));
    } else {
        if((host_num_of_entries <= 1) || (enable_global == false)) {
            set_on_dm(obj_ddns_client, "Status", DDNS_STATUS(status));
        }
    }
    if((status == ddns_status_error_config) && (enable_global == true)) {
        set_on_dm(obj_ddns_client, "LastError", DDNS_ERROR(ddns_error_config));
    }

exit:
    return;
}

static void check_status_only(amxp_timer_t* timer, void* data) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    ddns_error_t error = ddns_error_dns;
    const amxc_var_t* tmp_var = NULL;
    const cstring_t status_hostname = NULL;
    const cstring_t server = NULL;
    bool last_error_config = false;
    int host_num_of_entries = 0;
    int count_error = 0;
    int count_disable = 0;
    amxd_object_t* obj_ddns_client = NULL;
    amxd_object_t* tmpl_ddns_client_hostname = NULL;
    ddns_hostname_check_t* host_check = NULL;
    amxd_object_t* obj_ddns_client_hostname = (amxd_object_t*) data;
    ddns_client_t* client = NULL;
    amxc_var_t* curr_time = NULL;
    int ret_value = 0;

    amxc_var_new(&curr_time);
    ret_value = ddns_get_current_time(curr_time);
    if(ret_value != 0) {
        amxc_var_delete(&curr_time);
        curr_time = NULL;
    }

    when_null_trace(obj_ddns_client_hostname, exit, INFO, "Data is NULL.");
    host_check = (ddns_hostname_check_t*) obj_ddns_client_hostname->priv;
    when_null_trace(host_check, exit, INFO, "Wrapper is NULL.");

    obj_ddns_client = amxd_object_findf(obj_ddns_client_hostname, ".^.^");
    when_null_trace(obj_ddns_client, exit, INFO, "DynamicDNS.Client object is NULL");
    client = (ddns_client_t*) obj_ddns_client->priv;
    when_null_trace(client, exit, INFO, "Client struct is NULL.");
    tmpl_ddns_client_hostname = amxd_object_findf(obj_ddns_client, ".Hostname.");
    when_null_trace(tmpl_ddns_client_hostname, exit, INFO, "DynamicDNS.Client.Hostname template is NULL");

    tmp_var = amxd_object_get_param_value(obj_ddns_client, "LastError");
    if(tmp_var != NULL) {
        if(strcmp(GET_CHAR(tmp_var, NULL), DDNS_ERROR(ddns_error_config)) == 0) {
            last_error_config = true;
        }
    }

    tmp_var = amxd_object_get_param_value(obj_ddns_client_hostname, "Status");
    if(tmp_var != NULL) {
        status_hostname = GET_CHAR(tmp_var, NULL);
    } else {
        status_hostname = "";
    }

    tmp_var = amxd_object_get_param_value(obj_ddns_client, "Server");
    if(tmp_var != NULL) {
        server = GET_CHAR(tmp_var, NULL);
    }

    error = check_ddns(obj_ddns_client, amxd_object_get_index(obj_ddns_client_hostname));
    if(error == ddns_error_updating) {
        if(host_check->counter >= CHECK_AFTER_ADD_MAX_COUNTER) {
            error = ddns_error_timeout;
        } else {
            amxp_timer_start(timer, CHECK_AFTER_ADD_INTERVAL_DEFAULT);
            host_check->counter++;
            goto rearm;
        }
    }
    if(error == ddns_error_dns) {
        if(!str_empty(server)) {
            if((strcmp(status_hostname, "Error") != 0) && (!last_error_config)) {
                set_on_dm(obj_ddns_client, "LastError", DDNS_ERROR(error));
            }
        } else {
            set_on_dm(obj_ddns_client, "LastError", DDNS_ERROR(ddns_error_config));
            last_error_config = true;
        }
        set_on_dm(obj_ddns_client_hostname, "Status", DDNS_HOSTNAME_STATUS(ddns_hostname_error));
        host_check->last_status = ddns_hostname_error;
    } else if(error != ddns_error_no) {
        set_on_dm(obj_ddns_client, "LastError", DDNS_ERROR(error));
        last_error_config = (error == ddns_error_config);
        set_on_dm(obj_ddns_client_hostname, "Status", DDNS_HOSTNAME_STATUS(ddns_hostname_error));
        host_check->last_status = ddns_hostname_error;
    } else {
        if(!str_empty(server)) {
            if(!host_check->enable || !client->enable) {
                set_on_dm(obj_ddns_client_hostname, "Status", DDNS_HOSTNAME_STATUS(ddns_hostname_disabled));
                host_check->last_status = ddns_hostname_disabled;
            } else {
                if((host_check->last_status != ddns_hostname_registered) || client->changed_dm) {
                    if(curr_time != NULL) {
                        set_on_dm(obj_ddns_client_hostname, "LastUpdate", GET_CHAR(curr_time, NULL));
                    }
                }
                set_on_dm(obj_ddns_client_hostname, "Status", DDNS_HOSTNAME_STATUS(ddns_hostname_registered));
                host_check->last_status = ddns_hostname_registered;
            }
        } else {
            set_on_dm(obj_ddns_client_hostname, "Status", DDNS_HOSTNAME_STATUS(ddns_hostname_error));
            host_check->last_status = ddns_hostname_error;
        }
    }

    amxd_object_for_each(instance, it, tmpl_ddns_client_hostname) {
        amxd_object_t* intance = amxc_container_of(it, amxd_object_t, it);
        host_num_of_entries++;

        tmp_var = amxd_object_get_param_value(intance, "Status");
        if(tmp_var != NULL) {
            status_hostname = GET_CHAR(tmp_var, NULL);
        }

        if(status_hostname != NULL) {
            if(strcmp(status_hostname, "Error") == 0) {
                count_error++;
            } else if(strcmp(status_hostname, "Disabled") == 0) {
                count_disable++;
            }
        }
    }

    if(str_empty(server)) {
        set_on_dm(obj_ddns_client, "Status", DDNS_STATUS(ddns_status_error_config));
        set_on_dm(obj_ddns_client, "LastError", DDNS_ERROR(ddns_error_config));
    } else if(count_error == host_num_of_entries) {
        if(last_error_config) {
            set_on_dm(obj_ddns_client, "Status", DDNS_STATUS(ddns_status_error_config));
        } else {
            set_on_dm(obj_ddns_client, "Status", DDNS_STATUS(ddns_status_error));
        }
    } else if(count_disable == host_num_of_entries) {
        set_on_dm(obj_ddns_client, "Status", DDNS_STATUS(ddns_status_disabled));
        set_on_dm(obj_ddns_client, "LastError", DDNS_ERROR(ddns_error_no));
    } else {
        set_on_dm(obj_ddns_client, "Status", DDNS_STATUS(ddns_status_updated));
        set_on_dm(obj_ddns_client, "LastError", DDNS_ERROR(ddns_error_no));
    }

exit:
    stop_ddns(obj_ddns_client, amxd_object_get_index(obj_ddns_client_hostname));
    rv = amxp_timer_stop(timer);
    if(rv != 0) {
        amxp_timer_delete(&timer);
    }
    if(host_check != NULL) {
        host_check->checking = false;
    }
    if(client != NULL) {
        client->changed_dm = false;
    }

rearm:
    amxc_var_delete(&curr_time);
    SAH_TRACEZ_OUT(ME);
}

/**
   @brief
   Initialize check only timer.

   @param[in] instance Object with the Client.Hostname's instance.
   @param[in] enable_hostname Client.Hostname.Enable parameter
 */
static void init_check_only_timer(amxd_object_t* instance, bool enable_hostname) {
    SAH_TRACEZ_IN(ME);
    ddns_hostname_check_t* host_check = NULL;
    int rv = 0;

    when_null_trace(instance, exit, ERROR, "DynamicDNS.Client.Hostname object is NULL");

    host_check = (ddns_hostname_check_t*) instance->priv;
    if(host_check == NULL) {
        host_check = (ddns_hostname_check_t*) calloc(1, sizeof(ddns_hostname_check_t));
        host_check->timer = NULL;
        host_check->last_status = ddns_hostname_error;
        host_check->enable = enable_hostname;
        instance->priv = host_check;
    }

    if(host_check->timer == NULL) {
        amxp_timer_new(&(host_check->timer), check_status_only, instance);
    }
    host_check->counter = 0;
    rv = amxp_timer_start(host_check->timer, CHECK_AFTER_ADD_INTERVAL_DEFAULT);
    if(rv == 0) {
        host_check->checking = true;
    } else {
        amxp_timer_delete(&(host_check->timer));
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

/**
   @brief
   Stop the timer.

   @param[in] instance Object with the Client's instance.
 */
static void stop_timer(amxd_object_t* instance) {
    SAH_TRACEZ_IN(ME);
    ddns_client_t* client = (ddns_client_t*) instance->priv;
    when_null(client, exit);
    amxp_timer_stop(client->timer_check_interval);
exit:
    return;
}

/**
   @brief
   Re-arm the timer.

   @param[in] instance Object with the Client's instance.
 */
static void rearm_timer(amxd_object_t* instance) {
    SAH_TRACEZ_IN(ME);
    ddns_client_t* client = (ddns_client_t*) instance->priv;
    when_null(client, exit);

    amxp_timer_stop(client->timer_check_interval);
    amxp_timer_start(client->timer_check_interval, client->dm_check_interval);
exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

/**
   @brief
   Routine that must be called after the edit_ddns or add_ddns methods.

   @param[in] obj_ddns_client Client. instance
   @param[in] obj_ddns_client_hostname Client.Hostname. instance
   @param[in] method_status Status returned from the edit_ddns or add_ddns methods.
   @param[in] enable_server Server.Enable parameter
   @param[in] enable_client Client.Enable parameter
   @param[in] enable_hostname Client.Hostname.Enable parameter
 */
static void post_ddns_operations(amxd_object_t* obj_ddns_client, amxd_object_t* obj_ddns_client_hostname,
                                 ddns_status_t method_status, bool enable_server, bool enable_client, bool enable_hostname) {
    ddns_hostname_status_t host_status = ddns_hostname_error;
    ddns_hostname_check_t* host_check = NULL;
    ddns_client_t* client = NULL;
    bool enable = enable_hostname;

    if(!enable_server || !enable_client) {
        enable = false;
    }

    if(obj_ddns_client != NULL) {
        client = (ddns_client_t*) obj_ddns_client->priv;
    }

    if(obj_ddns_client_hostname != NULL) {
        host_check = (ddns_hostname_check_t*) obj_ddns_client_hostname->priv;
    }

    set_status_on_dm(method_status, obj_ddns_client, enable, (enable_server & enable_client));

    host_status = get_hostname_status(method_status);
    set_on_dm(obj_ddns_client_hostname, "Status", DDNS_HOSTNAME_STATUS(host_status));
    if(host_check != NULL) {
        if(host_status != ddns_hostname_updating) {
            host_check->last_status = host_status;
        }
        host_check->enable = enable_hostname;
    }
    if(client != NULL) {
        client->enable = enable_client && enable_server;
    }
    init_check_only_timer(obj_ddns_client_hostname, enable_hostname);

    if(!enable_server || !enable_client) {
        stop_timer(obj_ddns_client);
    } else if(enable_hostname) {
        rearm_timer(obj_ddns_client);
    }
}

/**
   @brief
   Iterate over the Client.Hostname instances and call edit ddns.

   @param[in] obj_ddns_client Client. instance
   @param[in] tmpl_hostname Client.Hostname. object
   @param[in] enable_server Server.Enable parameter
   @param[in] enable_client Client.Enable parameter
   @param[in] server Client.Server parameter
   @param[in] interface Client.Interface parameter
   @param[in] username Client.Username parameter
   @param[in] password Client.Password parameter
   @param[in] server_param Hash table with server parameters
   @param[in] skip_on_failure Skip iteration on failure.

   @return
   Return the amount of disabled hostnames.
 */
static int edit_all_hostnames(amxd_object_t* obj_ddns_client, amxd_object_t* tmpl_hostname, bool enable_server, bool enable_client,
                              const char* server, const char* interface, const char* username, const char* password, const amxc_var_t* server_param,
                              bool skip_on_failure) {
    SAH_TRACEZ_IN(ME);
    ddns_status_t status;
    int count_disabled = 0;
    amxd_object_t* obj_ddns_client_hostname = NULL;
    const amxc_var_t* tmp_var = NULL;
    const cstring_t domain = NULL;

    amxd_object_for_each(instance, it, tmpl_hostname) {
        bool enable = false;
        bool enable_local = false;

        obj_ddns_client_hostname = amxc_container_of(it, amxd_object_t, it);
        if((obj_ddns_client_hostname == NULL) && skip_on_failure) {
            SAH_TRACEZ_ERROR(ME, "DynamicDNS.Client.Hostname object is NULL");
            continue;
        }

        tmp_var = amxd_object_get_param_value(obj_ddns_client_hostname, "Name");
        if(tmp_var != NULL) {
            domain = GET_CHAR(tmp_var, NULL);
        } else if(skip_on_failure) {
            SAH_TRACEZ_ERROR(ME, "The domain could not be extracted.");
            continue;
        }

        tmp_var = amxd_object_get_param_value(obj_ddns_client_hostname, "Enable");
        if(tmp_var != NULL) {
            enable_local = GET_BOOL(tmp_var, NULL);
            enable = enable_local;
        } else if(skip_on_failure) {
            SAH_TRACEZ_ERROR(ME, "The enable could not be extracted.");
            continue;
        }

        if(!enable_server || !enable_client) {
            enable = false;
        }

        if(!enable) {
            count_disabled++;
        }

        status = edit_ddns(obj_ddns_client, amxd_object_get_index(obj_ddns_client_hostname), domain, server, interface, username, password, enable, server_param);
        post_ddns_operations(obj_ddns_client, obj_ddns_client_hostname, status, enable_server, enable_client, enable_local);
    }
    SAH_TRACEZ_OUT(ME);
    return count_disabled;
}

static int check_routine_ddns_dm(amxd_object_t* obj_ddns_client) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    const cstring_t interface = NULL;
    bool enable_server = false;
    bool enable_global = false;
    int host_num_of_entries = 0;
    amxd_object_t* tmpl_ddns_client_hostname = NULL;
    ddns_client_t* client = NULL;

    when_null_trace(obj_ddns_client, exit, ERROR, "DynamicDNS.Client object is NULL");
    tmpl_ddns_client_hostname = amxd_object_findf(obj_ddns_client, ".Hostname.");
    when_null_trace(tmpl_ddns_client_hostname, exit, ERROR, "DynamicDNS.Client.Hostname template is NULL");

    host_num_of_entries = amxd_object_get_instance_count(tmpl_ddns_client_hostname);
    enable_server = get_server_enable(obj_ddns_client, NULL);
    enable_global = amxd_object_get_value(bool, obj_ddns_client, "Enable", NULL);

    client = (ddns_client_t*) obj_ddns_client->priv;
    if(client != NULL) {
        interface = amxc_string_get(&(client->linux_intf), 0);
    }

    if(host_num_of_entries > 0) {
        edit_all_hostnames(obj_ddns_client, tmpl_ddns_client_hostname, enable_server, enable_global, NULL, interface, NULL, NULL, NULL, false);
    }
    rv = 0;

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

static void check_routine_ddns(UNUSED amxp_timer_t* const timer, void* data) {
    SAH_TRACEZ_IN(ME);
    amxc_string_t* path = (amxc_string_t*) data;
    amxd_object_t* obj_ddns_client = NULL;
    ddns_client_t* client = NULL;

    when_null(path, exit);
    obj_ddns_client = amxd_object_findf(ddns_get_root_object(), "%s", amxc_string_get(path, 0));
    when_null_trace(obj_ddns_client, exit, ERROR, "DynamicDNS.Client object is NULL");
    client = (ddns_client_t*) obj_ddns_client->priv;
    when_null_trace(client, exit, ERROR, "Client struct is NULL");

    check_routine_ddns_dm(obj_ddns_client);
exit:
    SAH_TRACEZ_OUT(ME);
}

/**
   @brief
   Initialise the Client internal structure.

   @param[in] instance Object with the Client's instance.
   @param[in] index Index of the client's instance.

   @return
   0 if success, 1 if failed.
 */
static int ddns_client_init(amxd_object_t* instance, int index) {
    SAH_TRACEZ_IN(ME);
    ddns_client_t* client = NULL;
    int ret_value = 1;

    when_null(instance, exit);
    when_false(index > 0, exit);

    client = (ddns_client_t*) instance->priv;
    if(client == NULL) {
        client = create_ddns_client();
        if(client == NULL) {
            SAH_TRACEZ_ERROR(ME, "Out of memory");
            goto exit;
        }
        instance->priv = client;
        client->obj_ddns_client = instance;
    }

    amxc_string_setf(&(client->path), ".Client.%d.", index);
    amxp_timer_new(&(client->timer_check_interval), check_routine_ddns, &(client->path));
    client->changed_dm = false;

    ret_value = 0;
exit:
    SAH_TRACEZ_OUT(ME);
    return ret_value;
}

/**
   @brief
   Check all the Clients.
 */
void check_all_clients(void) {
    SAH_TRACEZ_IN(ME);
    amxd_object_t* tmpl_ddns_client = NULL;
    amxd_object_t* obj_ddns_client = NULL;

    tmpl_ddns_client = ddns_client_template(ddns_get_dm());
    when_null_trace(tmpl_ddns_client, exit, ERROR, "DynamicDNS.Client. template is NULL");

    amxd_object_for_each(instance, it, tmpl_ddns_client) {
        obj_ddns_client = amxc_container_of(it, amxd_object_t, it);
        if(obj_ddns_client == NULL) {
            continue;
        }
        check_routine_ddns_dm(obj_ddns_client);
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

static void update_all_hostnames(amxd_object_t* obj_ddns_client, const char* server,
                                 const char* username, const char* password, bool enable_global, int host_num_of_entries) {
    SAH_TRACEZ_IN(ME);
    bool enable_server = false;
    int count_disabled = 0;
    amxd_object_t* tmpl_ddns_client_hostname = NULL;
    ddns_client_t* client = NULL;
    const char* interface = NULL;

    when_null_trace(obj_ddns_client, exit, ERROR, "DynamicDNS.Client object is NULL");

    tmpl_ddns_client_hostname = amxd_object_findf(obj_ddns_client, ".Hostname.");
    when_null_trace(tmpl_ddns_client_hostname, exit, ERROR, "DynamicDNS.Client.Hostname template is NULL");
    client = (ddns_client_t*) obj_ddns_client->priv;
    when_null(client, exit);

    enable_server = get_server_enable(obj_ddns_client, NULL);

    interface = amxc_string_get(&(client->linux_intf), 0);

    if(host_num_of_entries > 0) {
        count_disabled = edit_all_hostnames(obj_ddns_client, tmpl_ddns_client_hostname, enable_server, enable_global, server, interface, username, password, NULL, false);
        if(!enable_server || !enable_global || (count_disabled >= host_num_of_entries)) {
            stop_timer(obj_ddns_client);
        } else {
            rearm_timer(obj_ddns_client);
        }
    }
exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

static void set_netdev_interface(const amxc_var_t* result, ddns_client_t* client, amxd_object_t* obj_ddns_client) {
    SAH_TRACEZ_IN(ME);
    const char* interface_name = NULL;
    amxc_string_t* intf_name = NULL;
    amxc_string_t* client_intf = NULL;

    amxc_string_new(&intf_name, 0);
    amxc_string_new(&client_intf, 0);

    when_null(obj_ddns_client, error);
    when_null(client, error);
    amxc_string_setf(client_intf, "%s", amxc_string_get(&(client->linux_intf), 0));
    when_null(result, exit);

    interface_name = GET_CHAR(result, NULL);
    when_str_empty(interface_name, exit);
    amxc_string_setf(intf_name, "%s", interface_name);

exit:
    amxc_string_setf(&(client->linux_intf), "%s", amxc_string_get(intf_name, 0));
    if(strcmp(amxc_string_get(intf_name, 0), amxc_string_get(client_intf, 0)) != 0) {
        client->changed_dm = true;
        check_routine_ddns_dm(obj_ddns_client);
    }
error:
    amxc_string_delete(&intf_name);
    amxc_string_delete(&client_intf);
    SAH_TRACEZ_OUT(ME);
    return;
}

static void get_interface_cb(const char* sig_name UNUSED, const amxc_var_t* result, void* userdata) {
    SAH_TRACEZ_IN(ME);
    amxd_object_t* instance = (amxd_object_t*) userdata;
    ddns_client_t* client = (ddns_client_t*) instance->priv;
    set_netdev_interface(result, client, instance);
    SAH_TRACEZ_OUT(ME);
}

/**
   @brief
   Fetch the interface name.

   @param[in] client Client instance.
   @param[in] interface NetModel interface.
 */
static void fetch_interface(amxd_object_t* instance, const char* interface, bool add_instance) {
    SAH_TRACEZ_IN(ME);
    amxc_var_t* ret = NULL;
    ddns_client_t* client = NULL;
    const char* flags = NULL;

    when_null_trace(instance, exit, ERROR, "Client instance is NULL. Query can't be created.");
    client = (ddns_client_t*) instance->priv;
    when_null_trace(client, exit, ERROR, "Client struct is NULL. Query can't be created.");

    if(client->ipversion == IPv4) {
        flags = "netdev-bound && ipv4";
    } else if(client->ipversion == IPv6) {
        flags = "netdev-bound && ipv6";
    } else {
        flags = "netdev-bound";
    }

    if(client->query != NULL) {
        netmodel_closeQuery(client->query);
        client->query = NULL;
    }
    if(interface != NULL) {
        amxc_string_setf(&(client->interface), "%s", interface);
    }
    if(!str_empty(interface) && (strcmp(amxc_string_get(&(client->interface), amxc_string_text_length(&(client->interface)) - 1), ".") != 0)) {
        amxc_string_appendf(&(client->interface), ".");
    }
    client->query = netmodel_openQuery_getFirstParameter(amxc_string_get(&(client->interface), 0), ME, "NetDevName", flags,
                                                         netmodel_traverse_down, get_interface_cb, instance);
    if(!add_instance) {
        ret = netmodel_getFirstParameter(amxc_string_get(&(client->interface), 0), "NetDevName", flags, netmodel_traverse_down);
        set_netdev_interface(ret, client, instance);
    }
exit:
    amxc_var_delete(&ret);
    SAH_TRACEZ_OUT(ME);
}

static void fill_addresses_data_cb(const char* sig_name UNUSED, const amxc_var_t* result, UNUSED void* userdata) {
    SAH_TRACEZ_IN(ME);
    amxd_object_t* instance = (amxd_object_t*) userdata;
    ddns_client_t* client = NULL;
    amxc_var_t* new_intf_var = NULL;
    uint32_t new_num_of_addrs = 0;
    int rv = -1;
    int compare_res = -1;

    when_null_trace(result, exit, ERROR, "Result is empty");
    when_null_trace(instance, exit, ERROR, "Instance is NULL");
    when_null_trace(instance, exit, ERROR, "Client object is NULL");
    client = (ddns_client_t*) instance->priv;
    when_null_trace(client, exit, ERROR, "Client struct is NULL");

    amxc_var_new(&new_intf_var);
    amxc_var_set_type(new_intf_var, AMXC_VAR_ID_HTABLE);

    amxc_var_for_each(var, result) {
        rv = extract_address_from_query(&new_intf_var, &new_num_of_addrs, var, client->ipversion);
        if(rv == -1) {
            continue;
        }
    }
    //In case 'result' is not a list of variants, or not iterable, this extra run guarantees that the information is properly extracted.
    extract_address_from_query(&new_intf_var, &new_num_of_addrs, result, client->ipversion);

    rv = amxc_var_compare(client->ip_addresses, new_intf_var, &compare_res);
    if((rv == 0) && (compare_res != 0)) {
        amxc_var_delete(&(client->ip_addresses));
        amxc_var_move(client->ip_addresses, new_intf_var);
        // Only update the clients if the address exists and changed
        if(new_num_of_addrs > 0) {
            client->changed_dm = true;
            check_routine_ddns_dm(client->obj_ddns_client);
        }
    }

exit:
    amxc_var_delete(&new_intf_var);
    SAH_TRACEZ_OUT(ME);
    return;
}

/**
   @brief
   Fetch the list of IP addresses of the Interface on DynamicDNS.Client.

   @return
   0 if the query is opened.
 */
static int query_intf_address(amxd_object_t* instance, const char* interface) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    ddns_client_t* client = NULL;
    const char* flags = NULL;

    when_null_trace(instance, exit, ERROR, "Client instance is NULL. Query can't be created.");
    client = (ddns_client_t*) instance->priv;
    when_null_trace(client, exit, ERROR, "Client struct is NULL. Query can't be created.");

    if(client->ipversion == IPv4) {
        flags = "ipv4";
    } else if(client->ipversion == IPv6) {
        flags = "ipv6";
    } else {
        flags = "(ipv4 || ipv6)";
    }

    if(client->ip_query != NULL) {
        netmodel_closeQuery(client->ip_query);
        amxc_var_delete(&(client->ip_addresses));
        client->ip_query = NULL;
    }

    if(interface != NULL) {
        amxc_string_setf(&(client->interface), "%s", interface);
    }

    amxc_var_new(&client->ip_addresses);
    amxc_var_set_type(client->ip_addresses, AMXC_VAR_ID_HTABLE);

    client->ip_query = netmodel_openQuery_getAddrs(amxc_string_get(&(client->interface), 0), ME, flags, netmodel_traverse_down, fill_addresses_data_cb, instance);

    rv = 0;

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

/**
   @brief
   Get if the server is enabled.

   @param[in] obj_ddns_client Client instance.
   @param[in] server_param Server parameters.
 */
static bool get_server_enable(amxd_object_t* obj_ddns_client, const amxc_var_t* server_param) {
    SAH_TRACEZ_IN(ME);
    const amxc_var_t* tmp_var = NULL;
    bool enable_server = false;
    const cstring_t server = NULL;

    if(server_param != NULL) {
        tmp_var = GET_ARG(server_param, SERVER_ENABLE);
    }

    if(tmp_var != NULL) {
        enable_server = GET_BOOL(tmp_var, "to");
    } else {
        when_null(obj_ddns_client, exit);
        tmp_var = amxd_object_get_param_value(obj_ddns_client, "Server");
        when_null(tmp_var, exit);
        server = GET_CHAR(tmp_var, NULL);
        when_str_empty(server, exit);
        tmp_var = get_server_param(server, SERVER_ENABLE);
        if(tmp_var != NULL) {
            enable_server = GET_BOOL(tmp_var, NULL);
        }
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return enable_server;
}

/**
   @brief
   Close all the Clients' queries.
 */
void close_queries(void) {
    SAH_TRACEZ_IN(ME);
    amxd_object_t* tmpl_ddns_client = NULL;
    amxd_object_t* obj_ddns_client = NULL;

    tmpl_ddns_client = ddns_client_template(ddns_get_dm());
    when_null_trace(tmpl_ddns_client, exit, ERROR, "DynamicDNS.Client. template is NULL");

    amxd_object_for_each(instance, it, tmpl_ddns_client) {
        ddns_client_t* client = NULL;

        obj_ddns_client = amxc_container_of(it, amxd_object_t, it);
        if(obj_ddns_client == NULL) {
            continue;
        }
        client = (ddns_client_t*) obj_ddns_client->priv;
        if(client == NULL) {
            continue;
        }
        if(client->query != NULL) {
            netmodel_closeQuery(client->query);
            client->query = NULL;
        }
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

static const cstring_t change_wan_interface(const cstring_t interface) {
    const cstring_t changed_interface = NULL;
    int pos = -1;
    amxc_string_t ip_wan_intf;

    amxc_string_init(&ip_wan_intf, 0);

    when_str_empty(interface, exit);
    changed_interface = interface;

    amxc_string_setf(&ip_wan_intf, "%s", ddns_get_wan_ip_interface());
    when_str_empty(amxc_string_get(&ip_wan_intf, 0), exit);
    pos = amxc_string_text_length(&ip_wan_intf);
    when_false(pos > 0, exit);
    if(ip_wan_intf.buffer[pos - 1] == '.') {
        amxc_string_remove_at(&ip_wan_intf, pos - 1, 1);
    }
    pos = amxc_string_search(&ip_wan_intf, ".", 0);
    when_false(pos > 0, exit);

    if(strstr(interface, amxc_string_get(&ip_wan_intf, 0)) != NULL) {
        changed_interface = ddns_get_wan_logical_interface();
    }
exit:
    amxc_string_clean(&ip_wan_intf);
    return changed_interface == NULL ? "" : changed_interface;
}

void _client_added(UNUSED const char* const sig_name,
                   const amxc_var_t* const event_data,
                   UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    const amxc_var_t* parameters = NULL;
    const amxc_var_t* tmp_var = NULL;
    const amxc_var_t* tmp_var_server = NULL;
    const char* interface = NULL;
    const char* server = NULL;
    amxd_object_t* tmpl_ddns_client = amxd_dm_signal_get_object(ddns_get_dm(), event_data);
    amxd_object_t* obj_ddns_client = amxd_object_findf(tmpl_ddns_client, ".%d.", GET_INT32(event_data, "index"));
    amxd_object_t* tmpl_ddns = amxd_object_findf(tmpl_ddns_client, ".^");
    ddns_client_t* client = NULL;

    SAH_TRACEZ_INFO(ME, "Received event: %s", sig_name);

    when_null(tmpl_ddns_client, exit);
    when_null_trace(obj_ddns_client, exit, ERROR, "DynamicDNS.Client object is NULL");
    when_null(tmpl_ddns, exit);

    when_failed(ddns_client_init(obj_ddns_client, GET_INT32(event_data, "index")), exit);
    client = (ddns_client_t*) obj_ddns_client->priv;
    when_null_trace(client, exit, ERROR, "Client struct is NULL");

    parameters = GET_ARG(event_data, "parameters");
    when_null(parameters, exit);

    tmp_var = GET_ARG(parameters, "Server");
    if(tmp_var != NULL) {
        server = GET_CHAR(tmp_var, NULL);
        tmp_var_server = get_server_param(server, SERVER_CHECK_INTERVAL);
        if(tmp_var_server != NULL) {
            client->dm_check_interval = GET_UINT32(tmp_var_server, NULL) * 1000;
        }
    }

    client->ipversion = int_to_ipversion(ddns_get_uint32_param(obj_ddns_client, CLIENT_IPVERSION));

    tmp_var = GET_ARG(parameters, "Interface");
    if(tmp_var != NULL) {
        interface = change_wan_interface(GET_CHAR(tmp_var, NULL));
    }

    set_on_dm(obj_ddns_client, "Status", DDNS_STATUS(ddns_status_error_config));
    set_on_dm(obj_ddns_client, "LastError", DDNS_ERROR(ddns_error_config));

    fetch_interface(obj_ddns_client, interface, true);
    query_intf_address(obj_ddns_client, interface);
exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

void _client_hostname_added(UNUSED const char* const sig_name,
                            const amxc_var_t* const event_data,
                            UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    ddns_status_t status = ddns_status_error_config;
    const amxc_var_t* tmp_var = NULL;
    const amxc_var_t* parameters = NULL;
    const cstring_t domain = NULL;
    bool enable_server = false;
    bool enable_global = false;
    bool enable_local = false;
    bool enable = false;
    amxd_object_t* tmpl_ddns_client_hostname = amxd_dm_signal_get_object(ddns_get_dm(), event_data);
    amxd_object_t* obj_ddns_client = NULL;
    amxd_object_t* obj_ddns_client_hostname = NULL;

    SAH_TRACEZ_INFO(ME, "Received event: %s", sig_name);

    when_null_trace(tmpl_ddns_client_hostname, exit, ERROR, "DynamicDNS.Client.Hostname template is NULL");
    obj_ddns_client = amxd_object_findf(tmpl_ddns_client_hostname, ".^");
    when_null_trace(obj_ddns_client, exit, ERROR, "DynamicDNS.Client object is NULL");
    obj_ddns_client_hostname = amxd_object_findf(tmpl_ddns_client_hostname, ".%d.", GET_INT32(event_data, "index"));
    when_null_trace(obj_ddns_client_hostname, exit, ERROR, "DynamicDNS.Client.Hostname object is NULL");
    stop_timer(obj_ddns_client);

    parameters = GET_ARG(event_data, "parameters");
    when_null(parameters, exit);

    tmp_var = GET_ARG(parameters, "Name");
    if(tmp_var != NULL) {
        domain = GET_CHAR(tmp_var, NULL);
    }
    tmp_var = GET_ARG(parameters, "Enable");
    if(tmp_var != NULL) {
        enable_local = GET_BOOL(tmp_var, NULL);
    }

    enable_server = get_server_enable(obj_ddns_client, NULL);
    enable_global = amxd_object_get_value(uint32_t, obj_ddns_client, "Enable", NULL);

    if(!enable_server || !enable_global) {
        enable = false;
    } else {
        enable = enable_local;
    }

    status = add_ddns(obj_ddns_client, amxd_object_get_index(obj_ddns_client_hostname), domain, enable);
    post_ddns_operations(obj_ddns_client, obj_ddns_client_hostname, status, enable_server, enable_global, enable_local);
exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

void _client_changed(UNUSED const char* const sig_name,
                     const amxc_var_t* const event_data,
                     UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    const amxc_var_t* tmp_var = NULL;
    const amxc_var_t* tmp_var_server = NULL;
    const amxc_var_t* parameters = NULL;
    const cstring_t server = NULL;
    const cstring_t user = NULL;
    const cstring_t password = NULL;
    const cstring_t interface = NULL;
    bool enable_global = false;
    int host_num_of_entries = 0;
    amxd_object_t* obj_ddns_client = amxd_dm_signal_get_object(ddns_get_dm(), event_data);
    amxd_object_t* tmpl_ddns_client_hostname = NULL;
    ddns_client_t* client = NULL;


    SAH_TRACEZ_NOTICE(ME, "Received event: %s", sig_name);

    when_null_trace(obj_ddns_client, exit, ERROR, "DynamicDNS.Client object is NULL");
    stop_timer(obj_ddns_client);
    tmpl_ddns_client_hostname = amxd_object_findf(obj_ddns_client, ".Hostname.");

    client = (ddns_client_t*) obj_ddns_client->priv;
    when_null_trace(client, exit, ERROR, "Client struct is NULL");

    parameters = GET_ARG(event_data, "parameters");
    when_null(parameters, exit);

    if(tmpl_ddns_client_hostname != NULL) {
        host_num_of_entries = amxd_object_get_instance_count(tmpl_ddns_client_hostname);
    }

    tmp_var = GET_ARG(parameters, "Interface");
    if(tmp_var != NULL) {
        interface = change_wan_interface(GET_CHAR(tmp_var, "to"));
        client->ipversion = int_to_ipversion(ddns_get_uint32_param(obj_ddns_client, CLIENT_IPVERSION));
        fetch_interface(obj_ddns_client, interface, false);
        query_intf_address(obj_ddns_client, interface);
    } else {
        amxc_string_t param;
        amxc_string_init(&param, 0);
        amxc_string_setf(&param, "%s"CLIENT_IPVERSION, ddns_get_vendor_prefix());
        tmp_var = GET_ARG(parameters, amxc_string_get(&param, 0));
        if(tmp_var != NULL) {
            client->ipversion = int_to_ipversion(GET_UINT32(tmp_var, "to"));
            fetch_interface(obj_ddns_client, NULL, false);
            query_intf_address(obj_ddns_client, NULL);
        }
        amxc_string_clean(&param);
    }

    tmp_var = GET_ARG(parameters, "Server");
    if(tmp_var != NULL) {
        server = GET_CHAR(tmp_var, "to");
        tmp_var_server = get_server_param(server, SERVER_CHECK_INTERVAL);
        if(tmp_var_server != NULL) {
            client->dm_check_interval = GET_UINT32(tmp_var_server, NULL) * 1000;
        }
    }
    tmp_var = GET_ARG(parameters, "Username");
    if(tmp_var != NULL) {
        user = GET_CHAR(tmp_var, "to");
    }
    tmp_var = GET_ARG(parameters, "Password");
    if(tmp_var != NULL) {
        password = GET_CHAR(tmp_var, "to");
    }
    tmp_var = GET_ARG(parameters, "Enable");
    if(tmp_var != NULL) {
        enable_global = GET_BOOL(tmp_var, "to");
    } else {
        enable_global = amxd_object_get_value(bool, obj_ddns_client, "Enable", NULL);
    }

    client->changed_dm = true;
    update_all_hostnames(obj_ddns_client, server, user, password, enable_global, host_num_of_entries);

exit:
    SAH_TRACEZ_OUT(ME);
}

void _client_hostname_changed(UNUSED const char* const sig_name,
                              const amxc_var_t* const event_data,
                              UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    ddns_status_t status = ddns_status_error_config;
    const amxc_var_t* tmp_var = NULL;
    const amxc_var_t* parameters = NULL;
    const cstring_t interface = NULL;
    const cstring_t new_domain = NULL;
    bool enable_server = false;
    bool enable_global = false;
    bool enable_local = false;
    bool enable = false;
    amxd_object_t* obj_ddns_client_hostname = amxd_dm_signal_get_object(ddns_get_dm(), event_data);
    amxd_object_t* tmpl_ddns_client_hostname = NULL;
    amxd_object_t* obj_ddns_client = NULL;
    ddns_client_t* client = NULL;

    SAH_TRACEZ_NOTICE(ME, "Received event: %s", sig_name);

    when_null_trace(obj_ddns_client_hostname, exit, ERROR, "DynamicDNS.Client.Hostname object is NULL");
    tmpl_ddns_client_hostname = amxd_object_findf(obj_ddns_client_hostname, ".^");
    when_null_trace(tmpl_ddns_client_hostname, exit, ERROR, "DynamicDNS.Client.Hostname template is NULL");
    obj_ddns_client = amxd_object_findf(obj_ddns_client_hostname, ".^.^");
    when_null_trace(obj_ddns_client, exit, ERROR, "DynamicDNS.Client object is NULL");
    stop_timer(obj_ddns_client);

    parameters = GET_ARG(event_data, "parameters");
    when_null(parameters, exit);

    tmp_var = GET_ARG(parameters, "Name");
    if(tmp_var != NULL) {
        new_domain = GET_CHAR(tmp_var, "to");
    }

    tmp_var = GET_ARG(parameters, "Enable");
    if(tmp_var != NULL) {
        enable_local = GET_BOOL(tmp_var, "to");
    } else {
        tmp_var = amxd_object_get_param_value(obj_ddns_client_hostname, "Enable");
        when_null_trace(tmp_var, exit, ERROR, "The enable could not be extracted.");
        enable_local = GET_BOOL(tmp_var, NULL);
    }
    enable = enable_local;

    enable_server = get_server_enable(obj_ddns_client, NULL);
    enable_global = amxd_object_get_value(bool, obj_ddns_client, "Enable", NULL);

    if(!enable_server || !enable_global) {
        enable = false;
    }

    client = (ddns_client_t*) obj_ddns_client->priv;
    if(client != NULL) {
        interface = amxc_string_get(&(client->linux_intf), 0);
        client->changed_dm = true;
    }

    status = edit_ddns(obj_ddns_client, amxd_object_get_index(obj_ddns_client_hostname), new_domain, NULL, interface, NULL, NULL, enable, NULL);
    post_ddns_operations(obj_ddns_client, obj_ddns_client_hostname, status, enable_server, enable_global, enable_local);

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

void _propagate_server_change(UNUSED const char* const sig_name,
                              const amxc_var_t* const event_data,
                              UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t ret_resolve = amxd_status_ok;
    const amxc_var_t* tmp_var = NULL;
    const amxc_var_t* tmp_var_server = NULL;
    const amxc_var_t* parameters = NULL;
    const cstring_t interface = NULL;
    const cstring_t server = NULL;
    bool enable_server = false;
    bool enable_global = false;
    int index = 0;
    amxd_object_t* obj_ddns_server = amxd_dm_signal_get_object(ddns_get_dm(), event_data);
    amxd_object_t* tmpl_ddns_client = NULL;
    amxd_object_t* obj_ddns_client = NULL;
    amxd_object_t* tmpl_ddns_client_hostname = NULL;
    amxc_llist_t* path_list = NULL;
    amxc_string_t* path = NULL;
    ddns_client_t* client = NULL;

    SAH_TRACEZ_NOTICE(ME, "Received event: %s", sig_name);

    amxc_llist_new(&path_list);

    parameters = GET_ARG(event_data, "parameters");
    when_null(parameters, exit);

    tmpl_ddns_client = ddns_client_template(ddns_get_dm());
    when_null_trace(tmpl_ddns_client, exit, ERROR, "DynamicDNS.Client. template is NULL");
    when_null_trace(obj_ddns_server, exit, ERROR, "DynamicDNS.Server. object is NULL");
    index = amxd_object_get_index(obj_ddns_server);
    when_false_trace(index > 0, exit, ERROR, "Index of the Server object is < 0.");
    ret_resolve = amxd_object_resolve_pathf(tmpl_ddns_client, path_list, "[" CLIENT_SERVER " == 'DynamicDNS.Server.%d' || " CLIENT_SERVER " == 'Device.DynamicDNS.Server.%d']", index, index);
    when_false_trace(ret_resolve == amxd_status_ok, exit, ERROR, "No Client was found with the Server path.");

    amxc_llist_for_each(it_path, path_list) {
        char* real_path = NULL;
        path = amxc_llist_it_get_data(it_path, amxc_string_t, it);
        real_path = remove_device_prefix(amxc_string_get(path, 0));
        obj_ddns_client = amxd_dm_findf(ddns_get_dm(), "%s", real_path);
        free(real_path);
        if(obj_ddns_client == NULL) {
            SAH_TRACEZ_ERROR(ME, "DynamicDNS.Client object is NULL");
            continue;
        }

        tmpl_ddns_client_hostname = amxd_object_findf(obj_ddns_client, ".Hostname.");
        if(tmpl_ddns_client_hostname == NULL) {
            SAH_TRACEZ_ERROR(ME, "DynamicDNS.Client.Hostname template is NULL");
            continue;
        }

        client = (ddns_client_t*) obj_ddns_client->priv;
        if(obj_ddns_client == NULL) {
            SAH_TRACEZ_ERROR(ME, "Client struct is NULL");
            continue;
        }

        stop_timer(obj_ddns_client);

        enable_server = get_server_enable(obj_ddns_client, parameters);
        enable_global = amxd_object_get_value(bool, obj_ddns_client, "Enable", NULL);

        tmp_var = amxd_object_get_param_value(obj_ddns_client, "Server");
        if(tmp_var != NULL) {
            server = GET_CHAR(tmp_var, NULL);
            tmp_var_server = get_server_param(server, SERVER_CHECK_INTERVAL);
            if(tmp_var_server != NULL) {
                client->dm_check_interval = GET_UINT32(tmp_var_server, NULL) * 1000;
            }
        }

        client = (ddns_client_t*) obj_ddns_client->priv;
        if(client != NULL) {
            interface = amxc_string_get(&(client->linux_intf), 0);
            client->changed_dm = true;
        }

        edit_all_hostnames(obj_ddns_client, tmpl_ddns_client_hostname, enable_server, enable_global, NULL, interface, NULL, NULL, parameters, true);
    }

exit:
    amxc_llist_delete(&path_list, amxc_string_list_it_free);
    SAH_TRACEZ_OUT(ME);
    return;
}