/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <string.h>
#include <netdb.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "ddns_priv.h"
#include "ddns.h"
#include "ddns_error_codes.h"
#include "client/dm_client.h"
#include "client/client.h"

#define ME "ddns"

/**
   @brief
   Cleanup the Client internal structure.

   @param[in] instance Object with the Client's instance to be cleaned-up.

   @return
   0 if success, 1 if failed.
 */
static int ddns_client_cleanup(amxd_object_t* instance) {
    ddns_client_t* client = NULL;
    int ret_value = 1;

    when_null(instance, exit);

    client = (ddns_client_t*) instance->priv;
    when_null(client, exit);

    if(client->query != NULL) {
        netmodel_closeQuery(client->query);
        client->query = NULL;
    }

    if(client->ip_query != NULL) {
        netmodel_closeQuery(client->ip_query);
        client->ip_query = NULL;
    }

    amxp_timer_stop(client->timer_check_interval);
    amxp_timer_delete(&(client->timer_check_interval));

    amxc_string_clean(&(client->path));
    amxc_string_clean(&(client->linux_intf));
    amxc_string_clean(&(client->interface));

    amxc_var_delete(&(client->ip_addresses));

    amxc_llist_it_take(&client->it);

    free(client);
    instance->priv = NULL;
    client = NULL;

    ret_value = 0;
exit:
    return ret_value;
}

/**
   @brief
   Clean the host_check associated with an Hostname object.

   @param[in] instance Object with the Client.Hostname's instance.
 */
static void clean_hostname_check(amxd_object_t* instance) {
    ddns_hostname_check_t* host_check = NULL;

    when_null_trace(instance, exit, ERROR, "DynamicDNS.Client.Hostname object is NULL");
    host_check = (ddns_hostname_check_t*) instance->priv;
    when_null(host_check, exit);
    if(host_check->timer != NULL) {
        amxp_timer_delete(&(host_check->timer));
    }
    free(host_check);
    instance->priv = NULL;

exit:
    return;
}

amxd_status_t _client_instance_cleanup(amxd_object_t* object,
                                       UNUSED amxd_param_t* param,
                                       amxd_action_t reason,
                                       UNUSED const amxc_var_t* const args,
                                       UNUSED amxc_var_t* const retval,
                                       UNUSED void* priv) {
    amxd_status_t status = amxd_status_unknown_error;
    const amxc_var_t* tmp_var = NULL;
    const cstring_t alias = NULL;
    int host_num_of_entries = 0;
    amxd_object_t* tmpl_ddns_client_hostname = amxd_object_findf(object, ".Hostname.");
    amxd_object_t* obj_ddns_client_hostname = NULL;

    when_null(object, exit);
    when_true_status(reason != action_object_destroy,
                     exit,
                     status = amxd_status_function_not_implemented);

    SAH_TRACEZ_NOTICE(ME, "Client[%s] object about to be destroyed", OBJECT_NAME(object));

    when_null(tmpl_ddns_client_hostname, exit);

    tmp_var = amxd_object_get_param_value(object, "Alias");
    if(tmp_var != NULL) {
        alias = GET_CHAR(tmp_var, NULL);
    }

    host_num_of_entries = amxd_object_get_instance_count(tmpl_ddns_client_hostname);

    if(host_num_of_entries > 0) {
        amxd_object_for_each(instance, it, tmpl_ddns_client_hostname) {
            obj_ddns_client_hostname = amxc_container_of(it, amxd_object_t, it);
            if(alias != NULL) {
                delete_ddns(amxd_object_get_index(object), amxd_object_get_index(obj_ddns_client_hostname));
            }
            clean_hostname_check(obj_ddns_client_hostname);
        }
    }

    status = amxd_status_ok;
exit:
    ddns_client_cleanup(object);
    return status;
}

amxd_status_t _client_hostname_instance_cleanup(amxd_object_t* object,
                                                UNUSED amxd_param_t* param,
                                                amxd_action_t reason,
                                                UNUSED const amxc_var_t* const args,
                                                UNUSED amxc_var_t* const retval,
                                                UNUSED void* priv) {
    amxd_status_t status = amxd_status_unknown_error;
    const amxc_var_t* tmp_var = NULL;
    const cstring_t alias = NULL;
    amxd_object_t* obj_ddns_client = amxd_object_findf(object, ".^.^");

    when_null(object, exit);
    when_null(obj_ddns_client, exit);

    when_true_status(reason != action_object_destroy,
                     exit,
                     status = amxd_status_function_not_implemented);

    SAH_TRACEZ_NOTICE(ME, "Client[%s] object about to be destroyed", OBJECT_NAME(object));

    tmp_var = amxd_object_get_param_value(obj_ddns_client, "Alias");
    if(tmp_var != NULL) {
        alias = GET_CHAR(tmp_var, NULL);
    }

    if(alias != NULL) {
        delete_ddns(amxd_object_get_index(obj_ddns_client), amxd_object_get_index(object));
    }
    clean_hostname_check(object);

    status = amxd_status_ok;
exit:
    return status;
}