/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "ddns_priv.h"
#include "ddns.h"
#include "ddns_error_codes.h"
#include "ddns_config.h"
#include "client/dm_client.h"
#include "client/client.h"
#include "server/dm_server.h"

#define ME "ddns"

static amxc_llist_t ddns_client_llist;

/**
   @brief
   Allocate the memory and set default values of a Client
   internal structure.

   @return
   Pointer to the Client internal structure.
 */
ddns_client_t* create_ddns_client(void) {
    SAH_TRACEZ_IN(ME);
    ddns_client_t* client = NULL;

    client = (ddns_client_t*) calloc(1, sizeof(ddns_client_t));
    when_null(client, exit);

    amxc_string_init(&client->path, 0);
    amxc_string_init(&client->linux_intf, 0);
    amxc_string_init(&client->interface, 0);
    client->timer_check_interval = NULL;
    client->query = NULL;
    client->added = false;
    client->dm_check_interval = DM_CHECK_INTERVAL_DEFAULT;
    client->ipversion = IPv4;

    amxc_llist_append(&ddns_client_llist, &client->it);
exit:
    SAH_TRACEZ_OUT(ME);
    return client;
}

static int fill_server_info(amxc_var_t* mod_args, const char* server, const amxc_var_t* server_param) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    amxc_var_t* tmp_var = NULL;

    if(server_param != NULL) {
        tmp_var = GET_ARG(server_param, SERVER_SERVICE);
        if(tmp_var != NULL) {
            amxc_var_add_key(cstring_t, mod_args, SERVER_SERVICE, GET_CHAR(tmp_var, "to"));
        }
        tmp_var = GET_ARG(server_param, SERVER_ADDRESS);
        if(tmp_var != NULL) {
            amxc_var_add_key(cstring_t, mod_args, SERVER_ADDRESS, GET_CHAR(tmp_var, "to"));
        }
        tmp_var = GET_ARG(server_param, SERVER_PORT);
        if(tmp_var != NULL) {
            amxc_var_add_key(uint32_t, mod_args, SERVER_PORT, GET_UINT32(tmp_var, "to"));
        }
        tmp_var = GET_ARG(server_param, SERVER_PROTOCOL);
        if(tmp_var != NULL) {
            amxc_var_add_key(cstring_t, mod_args, SERVER_PROTOCOL, GET_CHAR(tmp_var, "to"));
        }
        tmp_var = GET_ARG(server_param, SERVER_CHECK_INTERVAL);
        if(tmp_var != NULL) {
            amxc_var_add_key(uint32_t, mod_args, SERVER_CHECK_INTERVAL, GET_UINT32(tmp_var, "to"));
        }
        tmp_var = GET_ARG(server_param, SERVER_RETRY_INTERVAL);
        if(tmp_var != NULL) {
            amxc_var_add_key(uint32_t, mod_args, SERVER_RETRY_INTERVAL, GET_UINT32(tmp_var, "to"));
        }
        tmp_var = GET_ARG(server_param, SERVER_MAX_RETRIES);
        if(tmp_var != NULL) {
            amxc_var_add_key(uint32_t, mod_args, SERVER_MAX_RETRIES, GET_UINT32(tmp_var, "to"));
        }
    } else {
        tmp_var = get_server_param(server, SERVER_SERVICE);
        if(tmp_var != NULL) {
            amxc_var_add_key(cstring_t, mod_args, SERVER_SERVICE, GET_CHAR(tmp_var, NULL));
        } else {
            amxc_var_add_key(cstring_t, mod_args, SERVER_SERVICE, "");
        }
        tmp_var = get_server_param(server, SERVER_ADDRESS);
        if(tmp_var != NULL) {
            amxc_var_add_key(cstring_t, mod_args, SERVER_ADDRESS, GET_CHAR(tmp_var, NULL));
        } else {
            amxc_var_add_key(cstring_t, mod_args, SERVER_ADDRESS, "");
        }
        tmp_var = get_server_param(server, SERVER_PORT);
        if(tmp_var != NULL) {
            amxc_var_add_key(uint32_t, mod_args, SERVER_PORT, GET_UINT32(tmp_var, NULL));
        } else {
            amxc_var_add_key(cstring_t, mod_args, SERVER_PORT, "");
        }
        tmp_var = get_server_param(server, SERVER_PROTOCOL);
        if(tmp_var != NULL) {
            amxc_var_add_key(cstring_t, mod_args, SERVER_PROTOCOL, GET_CHAR(tmp_var, NULL));
        } else {
            amxc_var_add_key(cstring_t, mod_args, SERVER_PROTOCOL, "");
        }
        tmp_var = get_server_param(server, SERVER_CHECK_INTERVAL);
        if(tmp_var != NULL) {
            amxc_var_add_key(uint32_t, mod_args, SERVER_CHECK_INTERVAL, GET_UINT32(tmp_var, NULL));
        } else {
            amxc_var_add_key(cstring_t, mod_args, SERVER_CHECK_INTERVAL, "");
        }
        tmp_var = get_server_param(server, SERVER_RETRY_INTERVAL);
        if(tmp_var != NULL) {
            amxc_var_add_key(uint32_t, mod_args, SERVER_RETRY_INTERVAL, GET_UINT32(tmp_var, NULL));
        } else {
            amxc_var_add_key(cstring_t, mod_args, SERVER_RETRY_INTERVAL, "");
        }
        tmp_var = get_server_param(server, SERVER_MAX_RETRIES);
        if(tmp_var != NULL) {
            amxc_var_add_key(uint32_t, mod_args, SERVER_MAX_RETRIES, GET_UINT32(tmp_var, NULL));
        } else {
            amxc_var_add_key(cstring_t, mod_args, SERVER_MAX_RETRIES, "");
        }
    }

    rv = 0;
    SAH_TRACEZ_OUT(ME);
    return rv;
}

static void fill_in_common_client_params(amxd_object_t* obj_ddns_client, amxc_var_t** mod_args, const cstring_t domain) {
    SAH_TRACEZ_IN(ME);
    ddns_client_t* client = NULL;
    const cstring_t ip = NULL;
    const cstring_t dns = NULL;

    when_null_trace(obj_ddns_client, exit, ERROR, "DynamicDNS.Client object is NULL");

    client = (ddns_client_t*) obj_ddns_client->priv;

    amxc_var_add_key(cstring_t, *mod_args, MOD_FORCE_IPVERSION, "1");
    if(client != NULL) {
        if(client->ipversion == IPv4) {
            amxc_var_add_key(cstring_t, *mod_args, MOD_USE_IPV6, "0");
        } else {
            amxc_var_add_key(cstring_t, *mod_args, MOD_USE_IPV6, "1");
        }
    } else {
        amxc_var_add_key(cstring_t, *mod_args, MOD_USE_IPV6, "0");
    }

    ip = ddns_get_string_param(obj_ddns_client, CLIENT_IP);
    if(!str_empty(ip)) {
        amxc_var_add_key(cstring_t, *mod_args, CLIENT_IP, ip);
        amxc_var_add_key(cstring_t, *mod_args, MOD_IP_SOURCE, "script");
    } else {
        amxc_var_add_key(cstring_t, *mod_args, MOD_IP_SOURCE, "interface");
    }

    dns = ddns_get_string_param(obj_ddns_client, CLIENT_DNS);
    if(!str_empty(dns)) {
        amxc_var_add_key(cstring_t, *mod_args, CLIENT_DNS, dns);
    } else {
        amxc_var_add_key(cstring_t, *mod_args, CLIENT_DNS, "");
    }

    if(domain != NULL) {
        amxc_var_add_key(cstring_t, *mod_args, MOD_DOMAIN, domain);
    }
exit:
    SAH_TRACEZ_OUT(ME);
}

/**
   @brief
   Add the DDNS on module

   @param[in] obj_ddns_client Client object.
   @param[in] host_index Hostname index.
   @param[in] domain Domain URL.
   @param[in] enable Enable boolean.

   @return
   ddns_status_t.
 */
ddns_status_t add_ddns(amxd_object_t* obj_ddns_client, uint32_t host_index, const cstring_t domain, bool enable) {
    SAH_TRACEZ_IN(ME);
    amxd_object_t* ddns_object = NULL;
    const amxc_var_t* tmp_var = NULL;
    amxc_var_t* mod_args = NULL;
    amxc_var_t* mod_ret = NULL;
    const cstring_t server = NULL;
    const cstring_t interface = NULL;
    ddns_client_t* client = NULL;
    ddns_status_t rv = ddns_status_error_config;

    amxc_var_new(&mod_args);
    amxc_var_set_type(mod_args, AMXC_VAR_ID_HTABLE);
    amxc_var_new(&mod_ret);
    amxc_var_set_type(mod_ret, AMXC_VAR_ID_HTABLE);

    ddns_object = ddns_get_root_object();
    when_null_trace(ddns_object, exit, ERROR, "DynamicDNS root object is NULL");
    when_null_trace(obj_ddns_client, exit, ERROR, "DynamicDNS.Client object is NULL");
    client = (ddns_client_t*) obj_ddns_client->priv;

    amxc_var_add_key(uint32_t, mod_args, "ClientInstance", amxd_object_get_index(obj_ddns_client));

    if(host_index > 0) {
        amxc_var_add_key(uint32_t, mod_args, "HostnameInstance", host_index);
    }

    tmp_var = amxd_object_get_param_value(obj_ddns_client, "Server");
    when_null_trace(tmp_var, exit, ERROR, "Failed to get the Server parameter from the Client object.");
    server = GET_CHAR(tmp_var, NULL);

    when_failed_trace(fill_server_info(mod_args, server, NULL), exit, ERROR, "Failed to fill in the server information.");

    tmp_var = amxd_object_get_param_value(obj_ddns_client, CLIENT_USERNAME);
    if(tmp_var != NULL) {
        amxc_var_add_key(cstring_t, mod_args, CLIENT_USERNAME, GET_CHAR(tmp_var, NULL));
    }
    tmp_var = amxd_object_get_param_value(obj_ddns_client, CLIENT_PASSWORD);
    if(tmp_var != NULL) {
        amxc_var_add_key(cstring_t, mod_args, CLIENT_PASSWORD, GET_CHAR(tmp_var, NULL));
    }

    if(client != NULL) {
        interface = amxc_string_get(&(client->linux_intf), 0);
        if(interface != NULL) {
            amxc_var_add_key(cstring_t, mod_args, CLIENT_INTERFACE, interface);
        }
    }

    fill_in_common_client_params(obj_ddns_client, &mod_args, domain);

    amxc_var_add_key(bool, mod_args, CLIENT_ENABLE, enable);

    rv = cfgctrlr_ddns_execute_function(ddns_object, "add-ddns", mod_args, mod_ret);
    if(rv != ddns_status_error_config) {
        if(client != NULL) {
            client->added = true;
        }
    }

exit:
    amxc_var_delete(&mod_args);
    amxc_var_delete(&mod_ret);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

/**
   @brief
   Edit the DDNS on module

   @param[in] obj_ddns_client Client object.
   @param[in] host_index Hostname index.
   @param[in] domain Domain URL.
   @param[in] server Server of the DDNS.
   @param[in] interface Interface to take the IP for the DDNS.
   @param[in] username Username.
   @param[in] password Password.
   @param[in] enable Enable boolean.
   @param[in] server_param Server parameters coming from event.

   @return
   ddns_status_t.
 */
ddns_status_t edit_ddns(amxd_object_t* obj_ddns_client, uint32_t host_index, const char* domain, const char* server,
                        const char* interface, const char* username, const char* password, bool enable, const amxc_var_t* server_param) {
    SAH_TRACEZ_IN(ME);
    amxd_object_t* ddns_object = NULL;
    amxc_var_t* mod_args = NULL;
    amxc_var_t* mod_ret = NULL;
    ddns_client_t* client = NULL;
    ddns_status_t rv = ddns_status_error_config;

    amxc_var_new(&mod_args);
    amxc_var_set_type(mod_args, AMXC_VAR_ID_HTABLE);
    amxc_var_new(&mod_ret);
    amxc_var_set_type(mod_ret, AMXC_VAR_ID_HTABLE);

    ddns_object = ddns_get_root_object();
    when_null_trace(ddns_object, exit, ERROR, "DynamicDNS root object is NULL");
    when_null_trace(obj_ddns_client, exit, ERROR, "DynamicDNS.Client object is NULL");
    client = (ddns_client_t*) obj_ddns_client->priv;

    amxc_var_add_key(uint32_t, mod_args, "ClientInstance", amxd_object_get_index(obj_ddns_client));

    if(host_index > 0) {
        amxc_var_add_key(uint32_t, mod_args, "HostnameInstance", host_index);
    }
    if((server != NULL) || (server_param != NULL)) {
        when_failed_trace(fill_server_info(mod_args, server, server_param), exit, ERROR, "Failed to fill in the server information.");
    }
    if(interface != NULL) {
        amxc_var_add_key(cstring_t, mod_args, CLIENT_INTERFACE, interface);
    }
    if(username != NULL) {
        amxc_var_add_key(cstring_t, mod_args, CLIENT_USERNAME, username);
    }
    if(password != NULL) {
        amxc_var_add_key(cstring_t, mod_args, CLIENT_PASSWORD, password);
    }

    fill_in_common_client_params(obj_ddns_client, &mod_args, domain);

    amxc_var_add_key(bool, mod_args, "Enable", enable);

    if((client != NULL) && (client->added == false)) {
        rv = add_ddns(obj_ddns_client, host_index, domain, enable);
        goto exit;
    }

    rv = cfgctrlr_ddns_execute_function(ddns_object, "edit-ddns", mod_args, mod_ret);

exit:
    amxc_var_delete(&mod_args);
    amxc_var_delete(&mod_ret);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

/**
   @brief
   Delete the DDNS on module

   @param[in] alias Alias of the object.
   @param[in] host_index Hostname index.

   @return
   0 if the ddns is deleted.
 */
int delete_ddns(uint32_t client_index, uint32_t host_index) {
    SAH_TRACEZ_IN(ME);
    amxd_object_t* ddns_object = NULL;
    amxc_var_t* mod_args = NULL;
    amxc_var_t* mod_ret = NULL;
    int rv = 0;

    amxc_var_new(&mod_args);
    amxc_var_set_type(mod_args, AMXC_VAR_ID_HTABLE);
    amxc_var_new(&mod_ret);
    amxc_var_set_type(mod_ret, AMXC_VAR_ID_HTABLE);

    ddns_object = ddns_get_root_object();

    if(client_index > 0) {
        amxc_var_add_key(uint32_t, mod_args, "ClientInstance", client_index);
    }
    if(host_index > 0) {
        amxc_var_add_key(uint32_t, mod_args, "HostnameInstance", host_index);
    }

    rv = cfgctrlr_ddns_execute_function(ddns_object, "delete-ddns", mod_args, mod_ret);

    amxc_var_delete(&mod_args);
    amxc_var_delete(&mod_ret);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

/**
   @brief
   Check the DDNS on module

   @param[in] obj_ddns_client Client object.
   @param[in] host_index Hostname index.

   @return
   0 if the ddns is deleted.
 */
ddns_error_t check_ddns(amxd_object_t* obj_ddns_client, uint32_t host_index) {
    SAH_TRACEZ_IN(ME);
    amxd_object_t* ddns_object = NULL;
    amxc_var_t* mod_args = NULL;
    amxc_var_t* mod_ret = NULL;
    ddns_error_t rv = ddns_error_no;

    amxc_var_new(&mod_args);
    amxc_var_set_type(mod_args, AMXC_VAR_ID_HTABLE);
    amxc_var_new(&mod_ret);
    amxc_var_set_type(mod_ret, AMXC_VAR_ID_HTABLE);

    ddns_object = ddns_get_root_object();
    when_null_trace(ddns_object, exit, ERROR, "DynamicDNS root object is NULL");
    when_null_trace(obj_ddns_client, exit, ERROR, "DynamicDNS.Client object is NULL");

    amxc_var_add_key(uint32_t, mod_args, "ClientInstance", amxd_object_get_index(obj_ddns_client));

    if(host_index > 0) {
        amxc_var_add_key(uint32_t, mod_args, "HostnameInstance", host_index);
    }

    rv = cfgctrlr_ddns_execute_function(ddns_object, "check-ddns", mod_args, mod_ret);

exit:
    amxc_var_delete(&mod_args);
    amxc_var_delete(&mod_ret);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

/**
   @brief
   Stop the DDNS on module

   @param[in] obj_ddns_client Client object.
   @param[in] host_index Hostname index.

   @return
   0 if the ddns is stopped.
 */
int stop_ddns(amxd_object_t* obj_ddns_client, uint32_t host_index) {
    SAH_TRACEZ_IN(ME);
    amxd_object_t* ddns_object = NULL;
    amxc_var_t mod_args;
    amxc_var_t mod_ret;
    int rv = -1;

    amxc_var_init(&mod_args);
    amxc_var_set_type(&mod_args, AMXC_VAR_ID_HTABLE);
    amxc_var_init(&mod_ret);
    amxc_var_set_type(&mod_ret, AMXC_VAR_ID_HTABLE);

    ddns_object = ddns_get_root_object();
    when_null_trace(ddns_object, exit, ERROR, "DynamicDNS root object is NULL");
    when_null_trace(obj_ddns_client, exit, ERROR, "DynamicDNS.Client object is NULL");

    amxc_var_add_key(uint32_t, &mod_args, "ClientInstance", amxd_object_get_index(obj_ddns_client));

    if(host_index > 0) {
        amxc_var_add_key(uint32_t, &mod_args, "HostnameInstance", host_index);
    }

    rv = cfgctrlr_ddns_execute_function(ddns_object, "stop-ddns", &mod_args, &mod_ret);

exit:
    amxc_var_clean(&mod_args);
    amxc_var_clean(&mod_ret);
    SAH_TRACEZ_OUT(ME);
    return rv;
}
